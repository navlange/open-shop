<!--
 * @Author: lokei
 * @Date: 2022-08-13 11:11:32
 * @LastEditors: lokei
 * @LastEditTime: 2022-08-16 10:49:19
 * @Description: 
-->
<p align="center">
	<img alt="logo" width="80" src="https://www.lokei.cn/pico/assets/logo-red.png">
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">奇辰商城</h1>
<p align="center">
	官网文档：<a href="https://www.lokei.cn">https://www.lokei.cn</a>
</p>
<p align="center">
	开放接口：<a href="https://open.lokei.cn:2443/shop-api/documentation">https://open.lokei.cn:2443/shop-api/documentation</a>
</p>

<p align="center">
	演示后台：<a href="https://open.lokei.cn:2443/html/shop/admin">https://https://open.lokei.cn:2443/html/shop/admin</a>
        用户名：admin
        密码：123456
</p>

# 一、系统简介
<!-- [![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://img.shields.io/packagist/dt/laravel/lumen-framework)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://img.shields.io/packagist/v/laravel/lumen-framework)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://img.shields.io/packagist/l/laravel/lumen)](https://packagist.org/packages/laravel/lumen-framework) -->

为软件服务提供一套开源商城服务系统。

# 二、环境部署


## 环境需求

|  |要求 |
|  ----  | ----  |
| 操作系统 | Linux系统 |
| PHP | 8.0版本以上 |
| Web服务 | 推荐Nginx |

## 安装部署

### 1、前置安装

奇辰商城需要先安装奇辰Open-api，请参考：https://gitee.com/navlange/open-api.git

### 2、nginx服务配置
```
root /path_to_root;  #网站根路径

location /shop-api {
    try_files $uri $uri/ /open-shop/shop-api/public/index.php?$query_string;
}
```
'/open-shop/shop-api/public/index.php?$query_string'表示到后端shop-api接口由网站根目录‘/path_to_root’下面的/open-shop/shop-api/public目录内index.php作为请求处理的入口文件。

### 3、部署后端shop-api

* 下载源码

在网站根路径/path_to_root下执行：git clone https://gitee.com/navlange/cms.git ，下载open-api源码。

* 安装第三方库

进入源码目录下shop-api目录，执行composer install命令安装第三方库。

* 初始化

拷贝一份shop-api目录下的.env.example到cms-api目录下面，取名为.env。修改.env数据库配置：

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=******   #设置数据库名称
DB_USERNAME=******   #设置数据库用户名
DB_PASSWORD=******   #设置数据库密码
```

* 导入数据库

建立数据库，数据库配置需要和.env配置文件一致。将源码install目录下origin.sql数据库导入。

### 4、部署前端

将install目录下admin.zip解压至网站根目录/path_to_root下的html/shop/admin目录。

### 5、登入后台

通过 http://域名/html/shop/admin 或 https://域名/html/shop/admin 访问后台。


# 三、业务板块

* 概述
* 商品
* 订单
* 平台
* 系统


# 四、关注了解更多
<img alt="logo" width="200" src="https://www.lokei.cn/pico/assets/lokeinew.jpg">

# 五、奇辰交流群

QQ群：933289062 <a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=DTa6k1uzl7vINweAVjHNblg_I0nrnC1G&jump_from=webapi"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="奇辰Open-API交流" title="奇辰Open-API交流"></a>
<?php
/*
 * @Author: lokei
 * @Date: 2023-07-05 07:52:23
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-18 09:43:22
 * @Description: 
 */

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'shop-api'], function () use ($router) {
    $router->get('/', function () use ($router) {
        return 'shop: ' . $router->app->version();
    });
    
});

<?php
/*
 * @Author: lokei
 * @Date: 2023-07-13 15:13:03
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-16 21:50:12
 * @Description: 
 */
$router->group(['prefix' => env('ROUTER_PREFIX') . 'shop-api'], function () use ($router) {

	// 菜单
	$router->group(['prefix' => 'widget/menu'], function () use ($router) {
		$router->get('list', [
			'as' => 'listMenu', 'uses' => '\App\Http\Controllers\Widget\Menu\ShopController@list'
		]);
		$router->get('displayList', [
			'as' => 'displayListMenu', 'uses' => '\App\Http\Controllers\Widget\Menu\ShopController@displayList'
		]);
		$router->post('reset', [
			'as' => 'resetMenu', 'uses' => '\App\Http\Controllers\Widget\Menu\ShopController@reset'
		]);
	});

	
	// 导航
	$router->group(['prefix' => 'widget/nav'], function () use ($router) {
		$router->get('list', [
			'as' => 'listNav', 'uses' => '\App\Http\Controllers\Widget\Nav\ShopController@list'
		]);
	});
	
	/**首页 */
	$router->group(['prefix' => 'widget/index'], function () use ($router) {
		$router->get('get', [
			'as' => 'getIndexShopTheme', 'uses' => '\App\Http\Controllers\Widget\Index\ShopController@get'
		]);
	});
	$router->group(['prefix' => 'widget/index/head'], function () use ($router) {
		$router->get('get', [
			'as' => 'getIndexHeadTheme', 'uses' => '\App\Http\Controllers\Widget\Index\HeadController@get'
		]);
		$router->post('set', [
			'as' => 'setIndexHeadTheme', 'uses' => '\App\Http\Controllers\Widget\Index\HeadController@set'
		]);
	});
	$router->group(['prefix' => 'widget/index/goods'], function () use ($router) {
		$router->get('get', [
			'as' => 'getIndexGoodsTheme', 'uses' => '\App\Http\Controllers\Widget\Index\GoodsController@get'
		]);
		$router->post('set', [
			'as' => 'setIndexGoodsTheme', 'uses' => '\App\Http\Controllers\Widget\Index\GoodsController@set'
		]);
	});
	
	/**商品 */
	$router->group(['prefix' => 'widget/goods/index'], function () use ($router) {
		$router->get('get', [
			'as' => 'getGoodsIndexTheme', 'uses' => '\App\Http\Controllers\Widget\Goods\IndexController@get'
		]);
		$router->post('set', [
			'as' => 'setGoodsIndexTheme', 'uses' => '\App\Http\Controllers\Widget\Goods\IndexController@set'
		]);
	});
});
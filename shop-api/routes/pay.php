<?php
/*
 * @Author: lokei
 * @Date: 2022-09-28 20:55:13
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-04 21:37:18
 * @Description: 
 */
$router->group(['prefix' => 'shop-api'], function () use ($router) {
	
	$router->group(['prefix' => 'pay', 'middleware' => 'auth'], function () use ($router) {
		$router->get('price', [
			'as' => 'price', 'uses' => '\App\Http\Controllers\Pay\PriceController@get'
		]);
		$router->post('submit', [
			'as' => 'submit', 'uses' => '\App\Http\Controllers\Pay\PayController@submit'
		]);
	});
	
	$router->group(['prefix' => 'pay/order'], function () use ($router) {
		$router->post('success', [
			'as' => 'success', 'uses' => '\App\Http\Controllers\Pay\OrderController@success'
		]);
	});
    
	$router->group(['prefix' => 'price', 'middleware' => 'auth'], function () use ($router) {
		$router->get('ofCartGoods', [
			'as' => 'ofCartGoods', 'uses' => '\App\Http\Controllers\Pay\PriceController@ofCartGoods'
		]);
	});

    $router->group(['prefix' => 'pay/wx'], function () use ($router) {
		// 回调通知
		$router->post('notify/{pid}/{store_id}', [
			'as' => 'wxpayNotify', 'uses' => '\App\Http\Controllers\Pay\Weixin\NotifyController@callback'
		]);
	});
});

<?php
/*
 * @Author: lokei
 * @Date: 2022-09-28 20:55:13
 * @LastEditors: lokei
 * @LastEditTime: 2024-10-08 23:09:32
 * @Description: 
 */
$router->group(['prefix' => 'shop-api'], function () use ($router) {
	
	// 商品
	$router->group(['prefix' => 'goods'], function () use ($router) {
		
		$router->get('get', [
			'as' => 'getGoods', 'uses' => '\App\Http\Controllers\Goods\GoodsController@get'
		]);
		$router->get('list', [
			'as' => 'GoodsList', 'uses' => '\App\Http\Controllers\Goods\GoodsController@list'
		]);
		$router->get('ofIds', [
			'as' => 'ofIdsGoods', 'uses' => '\App\Http\Controllers\Goods\GoodsController@ofIds'
		]);
		$router->get('filter', [
			'as' => 'filterGoods', 'uses' => '\App\Http\Controllers\Goods\GoodsController@filter'
		]);
		$router->post('update', [
			'as' => 'updateGoods', 'uses' => '\App\Http\Controllers\Goods\GoodsController@update'
		]);
		$router->post('create', [
			'as' => 'createGoods', 'uses' => '\App\Http\Controllers\Goods\GoodsController@create'
		]);
		$router->post('delete', [
			'as' => 'deleteGoods', 'uses' => '\App\Http\Controllers\Goods\GoodsController@delete'
		]);
		$router->post('setOrder', [
			'as' => 'setOrderGoods', 'uses' => '\App\Http\Controllers\Goods\GoodsController@setOrder'
		]);
		$router->post('setOn', [
			'as' => 'setOnGoods', 'uses' => '\App\Http\Controllers\Goods\GoodsController@setOn'
		]);
		$router->post('setOff', [
			'as' => 'setOffGoods', 'uses' => '\App\Http\Controllers\Goods\GoodsController@setOff'
		]);
	});
	$router->group(['prefix' => 'goods/recommend'], function () use ($router) {
		
		$router->get('get', [
			'as' => 'getGoodsRecommend', 'uses' => '\App\Http\Controllers\Goods\RecommendController@get'
		]);
		$router->post('set', [
			'as' => 'setGoodsRecommend', 'uses' => '\App\Http\Controllers\Goods\RecommendController@set'
		]);
	});

	// 库存模式
	$router->group(['prefix' => 'goods/stock/mode'], function () use ($router) {
		
		$router->get('get', [
			'as' => 'getGoodsStockMode', 'uses' => '\App\Http\Controllers\Goods\StockController@getMode'
		]);
		$router->post('update', [
			'as' => 'updateGoodsStockMode', 'uses' => '\App\Http\Controllers\Goods\StockController@updateMode'
		]);
	});
	$router->group(['prefix' => 'goods/stock'], function () use ($router) {
		
		$router->get('ofGoods', [
			'as' => 'getStockOfGoods', 'uses' => '\App\Http\Controllers\Goods\StockController@ofGoods'
		]);
	});

	// 规格
	$router->group(['prefix' => 'goods/norm'], function () use ($router) {
		
		$router->get('ofGoods', [
			'as' => 'getNormOfGoods', 'uses' => '\App\Http\Controllers\Goods\NormController@ofGoods'
		]);
		$router->post('create', [
			'as' => 'normCreate', 'uses' => '\App\Http\Controllers\Goods\NormController@create'
		]);
		$router->post('update', [
			'as' => 'updateNorm', 'uses' => '\App\Http\Controllers\Goods\NormController@update'
		]);
		$router->post('delete', [
			'as' => 'normDelete', 'uses' => '\App\Http\Controllers\Goods\NormController@delete'
		]);
		
		$router->post('value/create', [
			'as' => 'normValueCreate', 'uses' => '\App\Http\Controllers\Goods\NormValueController@create'
		]);
		$router->post('value/update', [
			'as' => 'updateNormValue', 'uses' => '\App\Http\Controllers\Goods\NormValueController@update'
		]);
		$router->post('value/delete', [
			'as' => 'deleteNormValue', 'uses' => '\App\Http\Controllers\Goods\NormValueController@delete'
		]);
	});

	
	// 标签
	$router->group(['prefix' => 'goods/tag'], function () use ($router) {
		
		$router->get('ofGoods', [
			'as' => 'ofGoodsTag', 'uses' => '\App\Http\Controllers\Goods\TagController@ofGoods'
		]);
		$router->post('updateGoods', [
			'as' => 'updateGoodsTag', 'uses' => '\App\Http\Controllers\Goods\TagController@updateGoods'
		]);
	});

    // marketing
    $router->group(['prefix' => 'goods/marketing'], function () use ($router) {
		
		$router->get('get', [
			'as' => 'getGoodsMarketing', 'uses' => '\App\Http\Controllers\Goods\MarketingController@get'
		]);
		$router->post('set', [
			'as' => 'setGoodsMarketing', 'uses' => '\App\Http\Controllers\Goods\MarketingController@set'
		]);
	});
	
	// 分销
	$router->group(['prefix' => 'goods/fx'], function () use ($router) {
		
		$router->get('get', [
			'as' => 'getFx', 'uses' => '\App\Http\Controllers\Goods\FxController@get'
		]);
		$router->post('set', [
			'as' => 'setFx', 'uses' => '\App\Http\Controllers\Goods\FxController@set'
		]);
	});
	
	
});

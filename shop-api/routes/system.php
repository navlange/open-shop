<?php
/*
 * @Author: lokei
 * @Date: 2022-09-28 20:55:13
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-04 23:16:29
 * @Description: 
 */
$router->group(['prefix' => 'erp-api'], function () use ($router) {
	
	$router->group(['prefix' => 'conf'], function () use ($router) {
		// Conf
		$router->get('get', [
			'as' => 'getConf', 'uses' => '\App\Http\Controllers\System\ConfController@get'
		]);
		$router->post('update', [
			'as' => 'updateConf', 'uses' => '\App\Http\Controllers\System\ConfController@update'
		]);
	});

});

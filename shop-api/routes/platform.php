<?php
/*
 * @Author: lokei
 * @Date: 2022-09-28 20:55:13
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-18 01:23:03
 * @Description: 
 */
$router->group(['prefix' => env('ROUTER_PREFIX') . 'shop-api'], function () use ($router) {
	
	// 分类
	$router->group(['prefix' => 'platform/type'], function () use ($router) {
		// type
		$router->get('hasGoods', [
			'as' => 'hasGoods', 'uses' => '\App\Http\Controllers\Platform\Type\GoodsController@hasGoods'
		]);
	});


});

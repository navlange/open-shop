<?php
/*
 * @Author: lokei
 * @Date: 2023-11-20 19:47:08
 * @LastEditors: lokei
 * @LastEditTime: 2024-03-18 10:18:55
 * @Description: 
 */
$router->group(['prefix' => env('ROUTER_PREFIX') . 'shop-api'], function () use ($router) {

	// 评价
	$router->group(['prefix' => 'cart', 'middleware' => 'auth'], function () use ($router) {
		// Comment
		$router->get('ofMe', [
			'as' => 'cartOfMe', 'uses' => '\App\Http\Controllers\Cart\CartController@ofMe'
		]);
		$router->post('add', [
			'as' => 'cartadd', 'uses' => '\App\Http\Controllers\Cart\CartController@add'
		]);
		$router->post('update', [
			'as' => 'cartupdate', 'uses' => '\App\Http\Controllers\Cart\CartController@update'
		]);
	});
});

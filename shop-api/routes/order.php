<?php
/*
 * @Author: lokei
 * @Date: 2022-09-28 20:55:13
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-18 05:49:15
 * @Description: 
 */
$router->group(['prefix' => 'shop-api'], function () use ($router) {
	
	$router->group(['prefix' => 'order/conf'], function () use ($router) {
		$router->get('get', [
			'as' => 'getConfOrder', 'uses' => '\App\Http\Controllers\Order\Conf\ConfController@get'
		]);
		$router->post('set', [
			'as' => 'setConfOrder', 'uses' => '\App\Http\Controllers\Order\Conf\ConfController@set'
		]);
	});
	
	$router->group(['prefix' => 'order'], function () use ($router) {
		$router->get('get', [
			'as' => 'orderget', 'uses' => '\App\Http\Controllers\Order\OrderController@get'
		]);
		$router->get('list', [
			'as' => 'orderList', 'uses' => '\App\Http\Controllers\Order\OrderController@list'
		]);
		$router->delete('{id}', [
			'as' => 'orderdelete', 'uses' => '\App\Http\Controllers\Order\OrderController@delete'
		]);
		$router->post('update', [
			'as' => 'orderupdate', 'uses' => '\App\Http\Controllers\Order\OrderController@update'
		]);
		$router->post('cancel', [
			'as' => 'ordercancel', 'uses' => '\App\Http\Controllers\Order\OrderController@cancel'
		]);
	});

	$router->group(['prefix' => 'order', 'middleware' => 'auth'], function () use ($router) {
		$router->get('ofMe', [
			'as' => 'orderOfMe', 'uses' => '\App\Http\Controllers\Order\OrderController@ofMe'
		]);
		$router->post('release', [
			'as' => 'releaseOrder', 'uses' => '\App\Http\Controllers\Order\ReleaseController@submit'
		]);
	});

    // 核销
    $router->group(['prefix' => 'order'], function () use ($router) {
        $router->post('verify/submit', [
            'as' => 'verificationOrder', 'uses' => '\App\Http\Controllers\Order\Verify\VerifyController@submit'
        ]);
    });

    // 订单商品
    $router->group(['prefix' => 'order/goods'], function () use ($router) {
        $router->post('refund', [
            'as' => 'refundOrderGoods', 'uses' => '\App\Http\Controllers\Order\GoodsController@refund'
        ]);
    });
    
    // 订单收货
    $router->group(['prefix' => 'order/receive'], function () use ($router) {
        $router->post('submit', [
            'as' => 'receiveOrderGoods', 'uses' => '\App\Http\Controllers\Order\ReceiveController@submit'
        ]);
    });
});

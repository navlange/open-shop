<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-04 23:18:17
 * @Description: 
 */

namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;

class ConfModel extends Model
{
	protected $table = 'conf';

    protected $primaryKey = null;

    //

    public $timestamps = false;

}

<?php
/*
 * @Author: lokei
 * @Date: 2022-08-21 08:19:20
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-18 07:23:32
 * @Description: 
 */
namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;

class ConsigneeModel extends Model {

    protected $connection = 'smt';
    
    protected $table = 'consignee';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;
}
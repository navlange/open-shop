<?php
/*
 * @Author: lokei
 * @Date: 2022-12-22 13:14:11
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-16 21:36:50
 * @Description: 
 */
namespace App\Models\Order\Conf;

use Illuminate\Database\Eloquent\Model;

class ConfModel extends Model {
    protected $table = 'order_conf';

    protected $primaryKey = null;

    //

    public $timestamps = false;

}
<?php
/*
 * @Author: lokei
 * @Date: 2022-08-21 08:19:20
 * @LastEditors: lokei
 * @LastEditTime: 2024-03-29 10:55:40
 * @Description: 
 */
namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;

class OrderGoodsModel extends Model {
    protected $table = 'order_goods';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;
}
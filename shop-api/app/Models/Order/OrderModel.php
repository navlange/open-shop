<?php
/*
 * @Author: lokei
 * @Date: 2022-12-22 13:14:11
 * @LastEditors: lokei
 * @LastEditTime: 2023-01-29 00:48:08
 * @Description: 
 */
namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;

class OrderModel extends Model {
    protected $table = 'order';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}
<?php
/*
 * @Author: lokei
 * @Date: 2022-12-22 13:14:11
 * @LastEditors: lokei
 * @LastEditTime: 2023-08-10 17:22:59
 * @Description: 
 */
namespace App\Models\ERP\Material;

use Illuminate\Database\Eloquent\Model;

class GoodsModel extends Model {
    protected $connection = 'erp';
    
    protected $table = 'goods';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}
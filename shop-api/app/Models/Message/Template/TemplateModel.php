<?php
/*
 * @Author: lokei
 * @Date: 2022-08-21 08:19:20
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-04 08:53:06
 * @Description: 
 */
namespace App\Models\Message\Template;

use Illuminate\Database\Eloquent\Model;

class TemplateModel extends Model {
    protected $connection = 'origin';
    
    protected $table = 'message_template';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;
}
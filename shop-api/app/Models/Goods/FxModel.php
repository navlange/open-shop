<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-11-11 18:21:38
 * @Description: 
 */

namespace App\Models\Goods;

use Illuminate\Database\Eloquent\Model;

class FxModel extends Model
{
	protected $table = 'goods_fx';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

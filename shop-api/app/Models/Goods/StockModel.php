<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-13 11:43:44
 * @Description: 
 */

namespace App\Models\Goods;

use Illuminate\Database\Eloquent\Model;

class StockModel extends Model
{
	protected $table = 'stock';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-13 12:00:46
 * @Description: 
 */

namespace App\Models\Goods;

use Illuminate\Database\Eloquent\Model;

class NormValueModel extends Model
{
	protected $table = 'norm_value';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

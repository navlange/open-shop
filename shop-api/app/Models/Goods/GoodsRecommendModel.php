<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-13 21:49:13
 * @Description: 
 */

namespace App\Models\Goods;

use Illuminate\Database\Eloquent\Model;

class GoodsRecommendModel extends Model
{
	protected $table = 'goods_recommend';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

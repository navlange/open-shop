<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-15 17:35:53
 * @Description: 
 */

namespace App\Models\Goods;

use Illuminate\Database\Eloquent\Model;

class GoodsMarketingModel extends Model
{
	protected $table = 'goods_marketing';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-13 09:28:47
 * @Description: 
 */

namespace App\Models\Goods;

use Illuminate\Database\Eloquent\Model;

class GoodsModel extends Model
{
	protected $table = 'goods';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

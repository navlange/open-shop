<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-13 14:42:06
 * @Description: 
 */

namespace App\Models\Goods;

use Illuminate\Database\Eloquent\Model;

class NormModel extends Model
{
	protected $table = 'norms';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

    public function values()
    {
        return $this->hasMany('App\Models\Goods\NormValueModel', 'norm_id', 'id');
    }

}

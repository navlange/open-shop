<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-30 18:01:56
 * @Description: 
 */

namespace App\Models\Platform\Print;

use Illuminate\Database\Eloquent\Model;

class PrinterModel extends Model
{
    protected $connection = 'origin';
    
	protected $table = 'printer';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

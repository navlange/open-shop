<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-16 22:18:59
 * @Description: 
 */

namespace App\Models\Platform\Tag;

use Illuminate\Database\Eloquent\Model;

class TagModel extends Model
{
    protected $connection = 'origin';
    
	protected $table = 'tag';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-08-17 23:42:29
 * @Description: 
 */

namespace App\Models\Platform\Type;

use Illuminate\Database\Eloquent\Model;

class TypeModel extends Model
{

    protected $connection = 'origin';
    
	protected $table = 'type';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

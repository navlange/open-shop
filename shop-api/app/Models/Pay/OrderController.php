<?php
/*
 * @Author: lokei
 * @Date: 2024-04-04 22:19:29
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-04 22:22:43
 * @Description: 
 */
namespace App\Http\Controllers\Pay;

use App\Http\Controllers\Controller;
use App\Middle\Pay\PaySuccessMiddle;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function success(Request $request)
    {
        PaySuccessMiddle::success($request->input('id'), $request->input('pay_mode'), $request->input('money'));
    }
}
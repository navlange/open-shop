<?php
/*
 * @Author: lokei
 * @Date: 2022-09-12 11:04:36
 * @LastEditors: lokei
 * @LastEditTime: 2024-03-21 20:59:37
 * @Description: 
 */
namespace App\Models\Pay;

use Illuminate\Database\Eloquent\Model;

class AccountModel extends Model {

    protected $connection = 'smt';

    protected $table = 'pay_account';

	protected $primaryKey = 'uid';
    //

    public $timestamps = false;

    // 初始化账户
    public static function init($uid)
    {
        $account = new AccountModel();
        $account->uid = $uid;
        $account->balance = 0;
        $account->save();
        return $account;
    }
}
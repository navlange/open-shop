<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-18 00:18:00
 * @Description: 
 */

namespace App\Models\Pay\Card;

use Illuminate\Database\Eloquent\Model;

class CardRecordModel extends Model
{

    protected $connection = 'smt';
    
	protected $table = 'pay_card_record';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

	public function card()
    {
        return $this->hasOne('App\Models\Pay\Card\PayCardModel', 'id', 'card_id');
    }

}

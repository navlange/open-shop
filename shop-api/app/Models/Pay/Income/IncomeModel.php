<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-18 03:50:38
 * @Description: 
 */

namespace App\Models\Pay\Income;

use Illuminate\Database\Eloquent\Model;

class IncomeModel extends Model
{
    protected $connection = 'smt';
    
	protected $table = 'income_record';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-04 22:13:36
 * @Description: 
 */

namespace App\Models\Pay\Record;

use Illuminate\Database\Eloquent\Model;

class PayRecordModel extends Model
{
    protected $connection = 'smt';
    
	protected $table = 'pay';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

<?php
/*
 * @Author: lokei
 * @Date: 2022-12-12 16:54:41
 * @LastEditors: lokei
 * @LastEditTime: 2022-12-12 16:59:04
 * @Description: 
 */
namespace App\Models\Common;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class SaasModel extends Model {
	protected static function boot()
    {
        parent::boot();
        if (config('app.proj_type') != null && (config('app.proj_type') == '5-1' || config('app.proj_type') == '7-1')) {
            static::addGlobalScope('avaiable',function (Builder $builder){
				$builder->where('pid', '=', config('app.proj_identity'));
			});
		}

        self::creating(
            function ($model) {
                if (config('app.proj_type') != null && (config('app.proj_type') == '5-1' || config('app.proj_type') == '7-1')) {
                    $model->pid = config('app.proj_identity');
                }
            }
        );
    }
}
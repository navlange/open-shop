<?php
/*
 * @Author: lokei
 * @Date: 2022-12-22 13:14:11
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-03 20:02:18
 * @Description: 
 */
namespace App\Models\OrderSmt;

use Illuminate\Database\Eloquent\Model;

class OrderSmtModel extends Model {

    protected $connection = 'smt';

    protected $table = 'order';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}
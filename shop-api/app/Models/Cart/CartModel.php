<?php
/*
 * @Author: lokei
 * @Date: 2022-08-21 08:19:20
 * @LastEditors: lokei
 * @LastEditTime: 2024-02-18 09:25:29
 * @Description: 
 */
namespace App\Models\Cart;

use Illuminate\Database\Eloquent\Model;

class CartModel extends Model {
    protected $table = 'cart';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;
}
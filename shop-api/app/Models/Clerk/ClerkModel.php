<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-06-29 09:16:00
 * @Description: 
 */

namespace App\Models\Clerk;

use App\Models\Common\SaasModel;

class ClerkModel extends SaasModel
{
    protected $connection = 'origin';
    
	protected $table = 'clerk';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

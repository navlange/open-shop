<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-12 12:01:53
 * @Description: 
 */

namespace App\Models\Widget;

use App\Models\Common\SaasModel;

class NavModel extends SaasModel
{
    protected $connection = 'cms';
    
	protected $table = 'nav';

    protected $primaryKey = 'id';

    //

    public $timestamps = false;

}

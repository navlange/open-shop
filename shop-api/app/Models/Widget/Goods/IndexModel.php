<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-04-06 11:44:44
 * @Description: 
 */

namespace App\Models\Widget\Goods;

use App\Models\Common\SaasModel;

class IndexModel extends SaasModel
{
	protected $table = 'theme_goods_index';

    protected $primaryKey = null;

    //

    public $timestamps = false;

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        self::updating(
            function ($model) {
                if (config('app.proj_type') != null && (config('app.proj_type') == '5-1' || config('app.proj_type') == '7-1')) {
                    $model->primaryKey = 'pid';
                }
            }
        );
    }

}

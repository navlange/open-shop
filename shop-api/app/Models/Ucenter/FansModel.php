<?php
/*
 * @Author: lokei
 * @Date: 2022-07-13 21:50:39
 * @LastEditors: lokei
 * @LastEditTime: 2024-07-26 15:04:51
 * @Description: 
 */

namespace App\Models\Ucenter;

use App\Models\Common\SaasModel;

class FansModel extends SaasModel
{
    protected $connection = 'origin';

	protected $table = 'fans';

	protected $primaryKey = 'fid';
    //

    public $timestamps = false;

}
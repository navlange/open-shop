<?php
/*
 * @Author: lokei
 * @Date: 2022-09-12 11:04:36
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-04 21:38:30
 * @Description: 
 */
namespace App\Models\Store;

use Illuminate\Database\Eloquent\Model;

class StoreModel extends Model {

    protected $connection = 'smt';

    protected $table = 'store';

	protected $primaryKey = 'id';
    //

    public $timestamps = false;
}
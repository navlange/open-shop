<?php
/*
 * @Author: lokei
 * @Date: 2022-10-02 22:34:05
 * @LastEditors: lokei
 * @LastEditTime: 2022-10-03 08:37:48
 * @Description: 
 */
namespace App\Http\Responses;

use Illuminate\Http\JsonResponse;
use OpenApi\Annotations\Items;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Schema;

/**
 * @Schema(
 *     title="通用响应内容",
 *     description="通用响应内容描述"
 * )
 *
 * @package App\Http\Responses
 */
class CommResp extends JsonResponse
{

    /**
     * @Property(
     *     type="integer",
     *     description="errorCode"
     * )
     *
     * @var int
     */
    public $errorCode = 0;

    /**
     * @Property(
     *     type="string",
     *     description="errorMsg"
     * )
     *
     * @var string
     */
    public $errorMsg;

    /**
     * @Property(
     *     type="array",
     *     @Items()
     * )
     *
     * @var array
     */
    public $data = [];
}
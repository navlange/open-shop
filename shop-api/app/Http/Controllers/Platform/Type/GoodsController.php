<?php
/*
 * @Author: lokei
 * @Date: 2022-09-17 16:25:29
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-18 01:24:18
 * @Description: 
 */
namespace App\Http\Controllers\Platform\Type;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Platform\Type\TypeModel;
use App\Models\Goods\GoodsModel;
use Illuminate\Http\Request;

class GoodsController extends Controller {

    public function hasGoods(Request $request) {
        $model = TypeModel::where('scene', '=', 'goods');
        $items = $model->get();
        $new_items = array();
        foreach($items as $item) {
            $goods_mode = GoodsModel::where('type_id', '=', $item->id);
            if($request->input('store_id') != null && intval($request->input('store_id')) > 0) {
                $goods_mode->where('store_id', '=', $request->input('store_id'));
            } else {
                $goods_mode->whereNull('store_id');
            }
            if($goods_mode->count() > 0) {
                array_push($new_items, $item);
            }
        }
        $res = ResultTool::success();
        $res['data']['items'] = $new_items;
        $res['data']['total'] = count($new_items);
        return $res;
    }

}
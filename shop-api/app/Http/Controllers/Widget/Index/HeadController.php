<?php
/*
 * @Author: lokei
 * @Date: 2023-01-02 11:11:11
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-13 17:41:56
 * @Description: 
 */
namespace App\Http\Controllers\Widget\Index;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Widget\Index\ThemeIndexShopModel;
use Illuminate\Http\Request;

class HeadController extends Controller {
    public function get() {
        $theme = ThemeIndexShopModel::first();
        if(is_null($theme)) {
            $theme = new ThemeIndexShopModel();
            $theme->save();
        }
        $res = ResultTool::success();
        $res['data'] = [
            'bg_img' => $theme->bg_img,
            'bg_color' => $theme->bg_color
        ];
        return $res;
    }
    
    public function set(Request $request) {
        $theme = ThemeIndexShopModel::first();
        if(is_null($theme)) {
            $theme = new ThemeIndexShopModel();
        }
        $theme->bg_img = $request->input('bg_img');
        $theme->bg_color = $request->input('bg_color');
        $theme->save();
        return ResultTool::success();
    }
}
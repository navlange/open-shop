<?php
/*
 * @Author: lokei
 * @Date: 2023-01-02 11:11:11
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-13 20:46:59
 * @Description: 
 */
namespace App\Http\Controllers\Widget\Index;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Widget\Index\ThemeIndexShopModel;
use Illuminate\Http\Request;

class GoodsController extends Controller {
    public function get() {
        $theme = ThemeIndexShopModel::first();
        if(is_null($theme)) {
            $theme = new ThemeIndexShopModel();
            $theme->save();
        }
        $res = ResultTool::success();
        $res['data'] = [
            'goods_on' => $theme->goods_on,
            'goods_tag_img' => $theme->goods_tag_img,
            'goods_theme' => $theme->goods_theme,
            'goods_only_img_on' => $theme->goods_only_img_on
        ];
        return $res;
    }
    
    public function set(Request $request) {
        $theme = ThemeIndexShopModel::first();
        if(is_null($theme)) {
            $theme = new ThemeIndexShopModel();
        }
        $theme->goods_on = $request->input('goods_on');
        $theme->goods_tag_img = $request->input('goods_tag_img');
        $theme->goods_theme = $request->input('goods_theme');
        $theme->goods_only_img_on = $request->input('goods_only_img_on');
        $theme->save();
        return ResultTool::success();
    }
}
<?php
/*
 * @Author: lokei
 * @Date: 2023-01-02 11:11:11
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-15 11:10:18
 * @Description: 
 */
namespace App\Http\Controllers\Widget\Index;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Widget\Index\ThemeIndexShopModel;

class ShopController extends Controller {
    public function get() {
        $theme = ThemeIndexShopModel::first();
        if(is_null($theme)) {
            $theme = new ThemeIndexShopModel();
            $theme->save();
        }
        $res = ResultTool::success();
        $res['data'] = $theme;
        return $res;
    }
    
}
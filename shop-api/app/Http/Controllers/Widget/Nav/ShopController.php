<?php
/*
 * @Author: lokei
 * @Date: 2022-12-31 11:30:36
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-13 18:09:25
 * @Description: 
 */
namespace App\Http\Controllers\Widget\Nav;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Middle\NavMiddle;
use App\Models\Widget\NavModel;
use Illuminate\Http\Request;

class ShopController extends Controller {
    public function list(Request $request) {
        $items = NavModel::where('mode', '=', $request->input('mode'))->get();
        if(count($items) == 0) {
            NavMiddle::reset($request->input('mode'));
            $items = NavModel::where('mode', '=', $request->input('mode'))->get();
        }
        $res = ResultTool::success();
        $res['data']['nav_list'] = $items;
        return $res;
    }
}
<?php
/*
 * @Author: lokei
 * @Date: 2023-04-06 11:43:43
 * @LastEditors: lokei
 * @LastEditTime: 2023-04-07 12:52:01
 * @Description: 
 */
namespace App\Http\Controllers\Widget\Goods;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Widget\Goods\IndexModel;
use Illuminate\Http\Request;

class IndexController extends Controller {
    public function get() {
        $theme = IndexModel::first();
        if(is_null($theme)) {
            $theme = new IndexModel();
            $theme->stock_theme = '0';
            $theme->save();
            $theme = IndexModel::first();
        }
        $res = ResultTool::success();
        $res['data'] = $theme;
        return $res;
    }

    public function set(Request $request) {
        $theme = IndexModel::first();
        $theme->base_panel_theme = $request->input('base_panel_theme');
        $theme->stock_theme = $request->input('stock_theme');
        $theme->price_theme = $request->input('price_theme');
        $theme->tag_on = $request->input('tag_on');
        $theme->tag_title = $request->input('tag_title');
        $theme->menu_home_on = $request->input('menu_home_on');
        $theme->menu_contacts_on = $request->input('menu_contacts_on');
        $theme->menu_collect_on = $request->input('menu_collect_on');
        $theme->menu_cart_on = $request->input('menu_cart_on');
        $theme->save();
        return ResultTool::success();
    }
}
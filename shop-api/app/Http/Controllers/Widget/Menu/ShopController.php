<?php
/*
 * @Author: lokei
 * @Date: 2022-12-31 21:21:28
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-13 15:18:14
 * @Description: 
 */

namespace App\Http\Controllers\Widget\Menu;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Middle\MenuMiddle;
use App\Models\Widget\MenuModel;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function list()
    {
        $items = MenuModel::orderBy('id')->get();
        if (count($items) == 0) {
            MenuMiddle::reset();
            $items = MenuModel::orderBy('id')->get();
        }
        $res = ResultTool::success();
        $res['data']['menu_list'] = $items;
        return $res;
    }

    public function displayList(Request $request)
    {
        $items = MenuModel::where('display', '=', '1')->orderBy('id')->get();
        if(count($items) == 0) {
            MenuMiddle::reset();
            $items = MenuModel::where('display', '=', '1')->orderBy('id')->get();
        }
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        return $res;
    }

    public function reset()
    {
        MenuModel::where('id', '>', 0)->delete();
        MenuMiddle::reset();
        $items = MenuModel::orderBy('id')->get();
        $res = ResultTool::success();
        $res['data']['menu_list'] = $items;
        return $res;
    }
}

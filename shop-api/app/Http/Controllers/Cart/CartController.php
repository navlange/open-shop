<?php
/*
 * @Author: lokei
 * @Date: 2024-02-18 09:23:36
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-06 21:37:02
 * @Description: 
 */
namespace App\Http\Controllers\Cart;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Cart\CartModel;
use App\Models\Goods\GoodsModel;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function ofMe(Request $request)
    {
        $user = app('auth')->user();
        $items = CartModel::where('uid', $user->uid)->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        return $res;
    }

    public function add(Request $request) {
        $user = app('auth')->user();
        $cart = CartModel::where('uid', '=', $user->uid)->where('goods_id', '=', $request->input('goods_id'))->first();
        if(!is_null($cart)) {
            if($request->input('amount') != null && $request->input('amount') > 0) {
                $cart->amount += $request->input('amount');
            } else {
                $cart->amount += 1;
            }
            $cart->save();
        } else {
            $cart = new CartModel();
            $cart->uid = $user->uid;
            $cart->goods_id = $request->input('goods_id');
            // 所属门店
            $goods = GoodsModel::where('id', '=', $request->input('goods_id'))->first();
            $cart->store_id = $goods->store_id;
            if($request->input('amount') != null && $request->input('amount') > 0) {
                $cart->amount = $request->input('amount');
            } else {
                $cart->amount = 1;
            }
            $cart->save();
        }
        $res = ResultTool::success();
        $res['data']['id'] = $cart->id;
        return $res;
    }

    public function update(Request $request) {
        $user = app('auth')->user();
        $cart = CartModel::where('uid', '=', $user->uid)->where('goods_id', '=', $request->input('goods_id'))->first();
        if(!is_null($cart) && $request->input('amount') != null) {
            if($request->input('amount') > 0) {
                $cart->amount = $request->input('amount');
                $cart->save();
            } else {
                CartModel::where('uid', '=', $user->uid)->where('goods_id', '=', $request->input('goods_id'))->delete();
            }
        }
        return ResultTool::success();
    }
}
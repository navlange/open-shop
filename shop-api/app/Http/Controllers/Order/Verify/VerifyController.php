<?php
/*
 * @Author: lokei
 * @Date: 2024-04-04 22:58:54
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-04 23:00:22
 * @Description: 
 */
namespace App\Http\Controllers\Order\Verify;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Order\OrderModel;
use App\Models\OrderSmt\OrderSmtModel;
use Illuminate\Http\Request;

class VerifyController extends Controller
{
    public function submit(Request $request)
    {
        $order = OrderModel::where('id', $request->input('id'))->first();
        if (!$order) {
            return ResultTool::fail('订单不存在');
        }
        if ($order->status != 1) {
            return ResultTool::fail('订单状态不正确');
        }
        $order->status = '4';
        $order->save();
        $order_smt = OrderSmtModel::where('id', $order->id)->first();
        $order_smt->status = '4';
        $order_smt->save();
        return ResultTool::success();
    }
}
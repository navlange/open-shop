<?php
/*
 * @Author: lokei
 * @Date: 2024-04-18 02:35:16
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-18 02:39:08
 * @Description: 
 */
namespace App\Http\Controllers\Order;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Middle\Pay\PriceMiddle;
use App\Models\Order\OrderGoodsModel;
use App\Models\Order\OrderModel;
use Illuminate\Http\Request;

class GoodsController extends Controller
{
    public function refund(Request $request)
    {

        $goods_arr = json_decode($request->input('goods_info'), true);
        foreach ($goods_arr as $goods) {
            if ($goods['refund_amount'] > 0) {
                $orderGoods = OrderGoodsModel::query()->where('order_id', $request->input('order_id'))
                    ->where('goods_id', $goods['id'])->first();
                $orderGoods->refund_amount = $goods['refund_amount'];
                $orderGoods->save();
            }
        }
        $order = OrderModel::query()->find($request->input('order_id'));
        $order->goods_info = $request->input('goods_info');
        $price = PriceMiddle::ofOrder($request->input('order_id'));
        $order->price = $price['total_price'];
        $order->save();
        return ResultTool::success();
    }
}
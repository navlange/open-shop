<?php
/*
 * @Author: lokei
 * @Date: 2023-07-16 21:29:52
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-16 21:33:08
 * @Description: 
 */
namespace App\Http\Controllers\Order\Conf;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Order\Conf\ConfModel;
use Illuminate\Http\Request;

class ConfController extends Controller {
    public function get() {
        $conf = ConfModel::first();
        if(is_null($conf)) {
            $conf = new ConfModel();
            $conf->reserve_agreement_on = '0';
            $conf->reserve_contract = '';
            $conf->need_mobile = '0';
            $conf->save();
        }
        $res = ResultTool::success();
        $res['data']['reserve_agreement_on'] = $conf->reserve_agreement_on;
        $res['data']['reserve_contract'] = $conf->reserve_contract;
        $res['data']['need_mobile'] = $conf->need_mobile;
        return $res;
    }

    public function set(Request $request) {
        $conf = ConfModel::first();
        $conf->reserve_agreement_on = $request->input('reserve_agreement_on');
        $conf->reserve_contract = $request->input('reserve_contract');
        $conf->need_mobile = $request->input('need_mobile');
        $conf->save();
        return ResultTool::success();
    }
}
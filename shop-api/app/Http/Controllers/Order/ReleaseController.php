<?php
/*
 * @Author: lokei
 * @Date: 2023-07-17 10:55:24
 * @LastEditors: lokei
 * @LastEditTime: 2024-07-27 22:41:16
 * @Description: 
 */

namespace App\Http\Controllers\Order;

use App\Common\Tools\RandomTool;
use App\Common\Tools\ResultTool;
use App\Common\Tools\Sn;
use App\Http\Controllers\Controller;
use App\Middle\Order\ReleaseSuccessMiddle;
use App\Middle\Pay\PriceMiddle;
use App\Models\Cart\CartModel;
use App\Models\Goods\GoodsModel;
use App\Models\Order\OrderGoodsModel;
use App\Models\Order\OrderModel;
use App\Models\OrderSmt\OrderSmtModel;
use App\Models\Store\StoreModel;
use Illuminate\Http\Request;

class ReleaseController extends Controller
{
    public function submit(Request $request)
    {
        
        $order_smt = new OrderSmtModel();
        $order_smt->app_mode = '0';
        $order_smt->sn = Sn::build_order_no_day5();
        $user = app('auth')->user();
        $order_smt->uid = $user->uid;
        $user_info = [
            'avatar' => $user->avatar,
            'name' => $user->realname ? $user->realname : $user->nickname,
            'mobile' => $user->mobile
        ];
        if ($request->input('name') != null && $request->input('name') != '') {
            $user_info['name'] = $request->input('name');
        }
        if ($request->input('mobile') != null && $request->input('mobile') != '') {
            $user_info['mobile'] = $request->input('mobile');
        }
        $order_smt->user_info = json_encode($user_info);
        $order_smt->store_id = $request->input('store_id');
        $order_smt->status = '0';
        $order_smt->created_at = date('Y-m-d H:i:s', time());
        $order_smt->save();

        $order = new OrderModel();
        $order->id = $order_smt->id;
        $order->sn = $order_smt->sn;
        $user = app('auth')->user();
        $order->uid = $order_smt->uid;
        $order->user_info = $order_smt->user_info;

        $order->consignee_name = $user_info['name'];
        $order->consignee_mobile = $user_info['mobile'];

        $order->store_id = $request->input('store_id');
        $store = StoreModel::where('id', '=', $request->input('store_id'))->first();
        $order->store_info = $store->name;

        $order->consignee_info = $request->input('consignee_info');
        $order->note = $request->input('note');

        if ($request->input('release_mode') != null && $request->input('release_mode') != '' && $request->input('release_mode') == '0') {
            $order->is_multiple = '1';
            $cart_list = CartModel::where('uid', '=', $user->uid)->get();
            $goods_info = [];
            foreach ($cart_list as $cart) {
                $goods = GoodsModel::where('id', '=', $cart->goods_id)->first();
                $cur_goods = [
                    'img_thumb' => $goods->img_thumb,
                    'name' => $goods->name,
                    'price' => $goods->price,
                    'amount' => $cart->amount
                ];
                array_push($goods_info, $cur_goods);
            }
            $order->goods_info = json_encode($goods_info);
        } else if ($request->input('release_mode') != null && $request->input('release_mode') != '' && $request->input('release_mode') == '1') {
            $order->is_multiple = '1';
            $ids = explode(',', $request->input('ids'));
            $carts = CartModel::where('uid', '=', $user->uid)->whereIn('id', $ids)->get();
            $goods_info = [];
            foreach ($carts as $cart) {
                $goods = GoodsModel::where('id', '=', $cart->goods_id)->first();
                $cur_goods = [
                    'img_thumb' => $goods->img_thumb,
                    'name' => $goods->name,
                    'price' => $goods->price,
                    'amount' => $cart->amount
                ];
                array_push($goods_info, $cur_goods);
            }
            $order->goods_info = json_encode($goods_info);
        } else {
            $order->is_multiple = '0';
            $order->goods_id = $request->input('goods_id');
            $goods = GoodsModel::where('id', '=', $request->input('goods_id'))->first();
            $cur_goods = [
                'img_thumb' => $goods->img_thumb,
                'name' => $goods->name,
                'price' => $goods->price,
                'amount' => 1
            ];
            $goods_info = [];
            array_push($goods_info, $cur_goods);
            $order->goods_info = json_encode($goods_info);
        }

        $order->status = '0';
        $order->created_at = $order_smt->created_at;
        $order->save();
        $order_id = $order->id;

        // 添加订单商品
        if ($order->is_multiple == '0') {
            $order_goods = new OrderGoodsModel();
            $order_goods->order_id = $order_id;
            $order_goods->goods_id = $order->goods_id;
            $order_goods->amount = 1;
            $order_goods->save();
        } else if ($order->is_multiple == '1') {
            if ($request->input('release_mode') != null && $request->input('release_mode') != '' && $request->input('release_mode') == '1') {
                $ids = explode(',', $request->input('ids'));
                $carts = CartModel::where('uid', '=', $user->uid)->whereIn('id', $ids)->get();
                foreach ($carts as $cart) {
                    $order_goods = new OrderGoodsModel();
                    $order_goods->order_id = $order_id;
                    $order_goods->goods_id = $cart->goods_id;
                    $order_goods->amount = $cart->amount;
                    $order_goods->save();
                }
            } else {
                $carts = CartModel::where('uid', '=', $user->uid)->get();
                foreach ($carts as $cart) {
                    $order_goods = new OrderGoodsModel();
                    $order_goods->order_id = $order_id;
                    $order_goods->goods_id = $cart->goods_id;
                    $order_goods->amount = $cart->amount;
                    $order_goods->save();
                }
            }
        }

        // 删除购物车
        if ($request->input('release_mode') != null && $request->input('release_mode') != '' && $request->input('release_mode') == '1') {
            $ids = explode(',', $request->input('ids'));
            CartModel::where('uid', '=', $user->uid)->whereIn('id', $ids)->delete();
        } else if ($request->input('release_mode') != null && $request->input('release_mode') != '' && $request->input('release_mode') == '0') {
            CartModel::where('uid', '=', $user->uid)->delete();
        }

        // 计算订单价格
        $price_info = PriceMiddle::ofOrder($order_id);
        $order->price = $price_info['total_price'];
        $order->save();

        ReleaseSuccessMiddle::success($order_id, $request->getHost());

        $res = ResultTool::success();
        $res['data']['order_id'] = $order_id;
        return $res;
    }
}

<?php
/*
 * @Author: lokei
 * @Date: 2023-01-29 01:00:10
 * @LastEditors: lokei
 * @LastEditTime: 2024-07-23 10:20:01
 * @Description: 
 */

namespace App\Http\Controllers\Order;

use App\Common\Permission\RoleGuard;
use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Order\ConsigneeModel;
use App\Models\Order\OrderGoodsModel;
use App\Models\Order\OrderModel;
use App\Models\OrderSmt\OrderSmtModel;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    public function get(Request $request)
    {
        $order = OrderModel::where('id', '=', $request->input('id'))->first();
        $res = ResultTool::success();
        $res['data'] = $order;
        return $res;
    }

    public function list(Request $request)
    {
        $model = RoleGuard::store_guard(null, 'App\Models\Order\OrderModel');
        if ($request->input('start_day') != null && $request->input('start_day') != '') {
            if (!is_null($model)) {
                $model = $model->where('created_at', '>=', $request->input('start_day'));
            } else {
                $model = OrderModel::where('created_at', '>=', $request->input('start_day'));
            }
        }
        if ($request->input('finish_day') != null && $request->input('finish_day') != '') {
            if (!is_null($model)) {
                $model = $model->where('created_at', '<=', $request->input('finish_day'));
            } else {
                $model = OrderModel::where('created_at', '<=', $request->input('finish_day'));
            }
        }
        if ($request->input('store_id') != null && $request->input('store_id') != '') {
			if (!is_null($model)) {
				$model = $model->where('store_id', '=', $request->input('store_id'));
			} else {
				$model = OrderModel::where('store_id', '=', $request->input('store_id'));
			}
		}
		if ($request->input('mobile') != null && $request->input('mobile') != '') {
			if (!is_null($model)) {
				$model = $model->where('consignee_mobile', '=', $request->input('mobile'));
			} else {
				$model = OrderModel::where('consignee_mobile', '=', $request->input('mobile'));
			}
		}
		if ($request->input('goods_name') != null && $request->input('goods_name') != '') {
			if (!is_null($model)) {
				$model = $model->where('goods_info', 'LIKE', '%' . $request->input('goods_name') . '%');
			} else {
				$model = OrderModel::where('goods_info', 'LIKE', '%' . $request->input('goods_name') . '%');
			}
		}
		if ($request->input('sn') != null && $request->input('sn') != '') {
			if (!is_null($model)) {
				$model = $model->where('sn', '=', $request->input('sn'));
			} else {
				$model = OrderModel::where('sn', '=', $request->input('sn'));
			}
		}
		if ($request->input('statuses') != null && $request->input('statuses') != '') {
			$statuses = explode(',', $request->input('statuses'));
			if (!is_null($model)) {
				$model = $model->whereIn('status', $statuses);
			} else {
				$model = OrderModel::whereIn('status', $statuses);
			}
		}
		if ($request->input('limit') != null && $request->input('limit') != '' && $request->input('is_downloading') == null) {
			if (is_null($model)) {
				$items = OrderModel::limit($request->input('limit'))->offset((intval($request->input('page')) - 1) * intval($request->input('limit')))->orderBy('created_at', 'desc')->get();
				$total = OrderModel::count();
			} else {
				$total = $model->count();
				$items = $model->limit($request->input('limit'))->offset((intval($request->input('page')) - 1) * intval($request->input('limit')))->orderBy('created_at', 'desc')->get();
			}
		} else {
			if (is_null($model)) {
				$model = OrderModel::orderBy('created_at', 'desc');
			} else {
				$model = $model->orderBy('created_at', 'desc');
			}
			$items = $model->get();
			$total = count($items);
		}
		$res = ResultTool::success();
		$res['data']['items'] = $items;
		$res['data']['total'] = $total;
        return $res;
    }

    public function ofMe()
    {
        $user = app('auth')->user();
        $items = OrderModel::where('uid', '=', $user->uid)->orderBy('created_at', 'desc')->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        return $res;
    }

    public function delete(Request $request, $id) {
        OrderModel::where('id', '=', $id)->delete();
        OrderGoodsModel::where('order_id', '=', $id)->delete();
        OrderSmtModel::where('id', '=', $id)->delete();
        return ResultTool::success();
    }

    public function cancel(Request $request) {
        $order = OrderModel::where('id', '=', $request->input('id'))->first();
        $order->status = '2';
        $order->save();
        $order_smt = OrderSmtModel::where('id', '=', $request->input('id'))->first();
        $order_smt->status = '2';
        $order_smt->save();
        return ResultTool::success();
    }

    public function update(Request $request) {
        $order = OrderModel::where('id', '=', $request->input('id'))->first();
        $order->need_packing = $request->input('need_packing');
        $order->need_delivery = $request->input('need_delivery');

        $order->delivery_mode = $request->input('delivery_mode');
        if($request->input('consignee_id') != null && intval($request->input('consignee_id')) > 0) {
            $order->consignee_id = $request->input('consignee_id');
            $consignee = ConsigneeModel::where('id', '=', $request->input('consignee_id'))->first();
            $order->consignee_name = $consignee->name;
            $order->consignee_mobile = $consignee->mobile;
            $order->consignee_prov = $consignee->prov;
            $order->consignee_city = $consignee->city;
            $order->consignee_district = $consignee->district;
            $order->consignee_address = $consignee->address;
        }

        $order->save();
        return ResultTool::success();
    }

}

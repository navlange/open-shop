<?php
/*
 * @Author: lokei
 * @Date: 2024-04-18 05:49:48
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-18 05:51:29
 * @Description: 
 */
namespace App\Http\Controllers\Order;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Order\OrderModel;
use App\Models\OrderSmt\OrderSmtModel;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function submit(Request $request)
    {
        $order = OrderModel::find($request->input('id'));
        $order->status = '4';
        $order->save();
        $order_smt = OrderSmtModel::where('id', $request->input('id'))->first();
        $order_smt->status = '4';
        $order_smt->save();
        return ResultTool::success();
    }
}
<?php
/*
 * @Author: lokei
 * @Date: 2022-10-02 22:18:03
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-05 15:04:42
 * @Description: 
 */
namespace App\Http\Controllers;

use OpenApi\Annotations\Contact;
use OpenApi\Annotations\Info;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Schema;
use OpenApi\Annotations\Server;

/**
 *
 * @Info(
 *     version="1.0.0",
 *     title="奇辰ERP-API",
 *     description="构建软件服务SaaS的开源生态，提供前后端分离ERP服务。",
 *     @Contact(
 *         email="15727013996@163.com",
 *         name="奇辰"
 *     )
 * )
 *
 * @Server(
 *     url=SWAGGER_LUME_CONST_HOST,
 *     description="Erp-api接口",
 * )
 *
 * @Schema(
 *     schema="ApiResponse",
 *     type="object",
 *     description="响应实体，响应结果统一使用该结构",
 *     title="响应实体",
 *     @Property(
 *         property="code",
 *         type="string",
 *         description="响应代码"
 *     ),
 *     @Property(property="message", type="string", description="响应结果提示")
 * )
 *
 *
 * @package App\Http\Controllers
 */
class SwaggerController
{}
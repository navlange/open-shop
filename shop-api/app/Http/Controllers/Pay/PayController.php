<?php
/*
 * @Author: lokei
 * @Date: 2023-07-17 15:28:07
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-18 07:10:12
 * @Description: 
 */
namespace App\Http\Controllers\Pay;

use App\Common\Enums\PayMode;
use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Middle\Pay\PayBalanceMiddle;
use App\Middle\Pay\PayCardMiddle;
use App\Middle\Pay\PaySuccessMiddle;
use App\Middle\Pay\PriceMiddle;
use App\Models\Order\OrderModel;
use Illuminate\Http\Request;

class PayController extends Controller {
    public function submit(Request $request) {
        $price = PriceMiddle::ofOrder($request->input('order_id'));
        // 余额支付
        if ($request->input('pay_mode') == PayMode::BALANCE['value']) {
            // TODO: 余额支付
            $order = OrderModel::where('id', $request->input('order_id'))->first();
            $result = PayBalanceMiddle::pay(app('auth')->user()->uid, $price['discount_price'], $request->input('order_id'), '订单编号：' . $order->sn);
            if ($result['code'] != 0) {
                return ResultTool::fail($result['msg']);
            } else {
                // TODO: 支付成功
                PaySuccessMiddle::success($request->input('order_id'), PayMode::BALANCE['value'], $price['discount_price']);
                return ResultTool::success();
            }
        } else if ($request->input('pay_mode') == PayMode::WECHAT['value']) {
            $res = ResultTool::success();
            $res['data']['to_pay'] = $price['discount_price'];
            $res['data']['pay_target_id'] = $request->input('order_id');
            return $res;
        } else if ($request->input('pay_mode') == PayMode::ALIPAY['value']) {
            $res = ResultTool::success();
            $res['data']['to_pay'] = $price['discount_price'];
            $res['data']['pay_target_id'] = $request->input('order_id');
            return $res;
        } else if ($request->input('pay_mode') == PayMode::PAYCARD['value']) {
            $order = OrderModel::where('id', $request->input('order_id'))->first();
            $result = PayCardMiddle::pay($request->input('record_id'), $price['discount_price'], $request->input('order_id'), '订单编号：' . $order->sn);
            if ($result['code'] != 0) {
                return ResultTool::fail($result['msg']);
            } else {
                PaySuccessMiddle::success($request->input('order_id'), PayMode::PAYCARD['value'], $price['discount_price']);
                return ResultTool::success();
            }
        } else if ($request->input('pay_mode') == PayMode::OFFLINE['value']) {
            $order = OrderModel::where('id', $request->input('order_id'))->first();
            $order->clerk_discount = $request->input('clerk_discount');
            $order->pay_note = $request->input('note');
            $order->save();
            $result = PaySuccessMiddle::success($request->input('order_id'), PayMode::OFFLINE['value'], $request->input('money'));
            return ResultTool::success();
        }
        // 未知支付方式
        return ResultTool::fail('未知支付方式');
    }
}
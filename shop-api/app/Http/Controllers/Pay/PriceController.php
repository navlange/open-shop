<?php
/*
 * @Author: lokei
 * @Date: 2023-07-17 14:45:01
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-16 01:58:37
 * @Description: 
 */
namespace App\Http\Controllers\Pay;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Middle\Pay\PriceMiddle;
use App\Models\Cart\CartModel;
use App\Models\Goods\GoodsModel;
use Illuminate\Http\Request;

class PriceController extends Controller {
    public function get(Request $request) {
        $price = PriceMiddle::ofOrder($request->input('order_id'));
        $res = ResultTool::success();
        $res['data'] = $price;
        return $res;
    }

    public function ofCartGoods(Request $request) {
        $user = app('auth')->user();
        $ids = explode(',', $request->input('ids'));
        $carts = CartModel::where('uid', '=', $user->uid)->whereIn('id', $ids)->get();
        $total_price = 0;
        $discount_price = 0;
        foreach($carts as $cart) {
            $goods = GoodsModel::where('id', '=', $cart->goods_id)->first();
            $total_price += $goods->price * $cart->amount;
            $discount_price += $goods->price * $cart->amount;
        }
        $res = ResultTool::success();
        $res['data']['total_price'] = $total_price;
        $res['data']['discount_price'] = $discount_price;
        return $res;
    }
}
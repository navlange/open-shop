<?php
/*
 * @Author: lokei
 * @Date: 2023-07-17 16:56:49
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-16 08:12:09
 * @Description: 
 */
namespace App\Http\Controllers\Pay;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Middle\Pay\PaySuccessMiddle;
use Illuminate\Http\Request;

class OrderController extends Controller {
    public function success(Request $request) {
        PaySuccessMiddle::success($request->input('id'), $request->input('pay_mode'), $request->input('money'));
        return ResultTool::success();
    }
}
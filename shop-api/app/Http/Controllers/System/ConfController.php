<?php
/*
 * @Author: lokei
 * @Date: 2023-07-04 23:17:23
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-04 23:25:22
 * @Description: 
 */
namespace App\Http\Controllers\System;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\System\ConfModel;
use Illuminate\Http\Request;

class ConfController extends Controller {
    public function get() {
        $conf = ConfModel::first();
        if(is_null($conf)) {
            $conf = new ConfModel();
            $conf->goods_api = '';
            $conf->save();
            $conf = ConfModel::first();
        }
        $res = ResultTool::success();
        $res['data'] = $conf;
        return $res;
    }

    public function update(Request $request) {
        $conf = ConfModel::first();
        if(is_null($conf)) {
            $conf = new ConfModel();
            $conf->goods_api = '';
            $conf->save();
            $conf = ConfModel::first();
        }
        $conf->goods_api = $request->input('goods_api');
        $conf->save();
        return ResultTool::success();
    }
}
<?php
/*
 * @Author: lokei
 * @Date: 2023-07-04 14:57:12
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-13 11:43:09
 * @Description: 
 */
namespace App\Http\Controllers\Goods;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Goods\GoodsModel;
use App\Models\Goods\StockModel;
use Illuminate\Http\Request;

class StockController extends Controller {
    public function getMode(Request $request) {
        $goods = GoodsModel::where('id', '=', $request->input('goods_id'))->first();
        $res = ResultTool::success();
        $res['data'] = [
            'stock_mode' => $goods->stock_mode
        ];
        return $res;
    }

    public function updateMode(Request $request) {
        $goods = GoodsModel::where('id', '=', $request->input('goods_id'))->first();
        $goods->stock_mode = $request->input('stock_mode');
        $goods->save();
        return ResultTool::success();
    }

    public function ofGoods(Request $request) {
        $items = StockModel::where('goods_id', '=', $request->input('goods_id'))->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }
}
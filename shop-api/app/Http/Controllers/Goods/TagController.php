<?php
/*
 * @Author: lokei
 * @Date: 2023-07-16 22:12:58
 * @LastEditors: lokei
 * @LastEditTime: 2024-06-18 00:53:00
 * @Description: 
 */
namespace App\Http\Controllers\Goods;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Goods\GoodsModel;
use App\Models\Platform\Tag\TagModel;
use Illuminate\Http\Request;

class TagController extends Controller {

    public function ofGoods(Request $request) {
        $goods = GoodsModel::where('id', '=', $request->input('id'))->first();
        $tag_ids = explode('|', $goods->tag_ids);
        $items = [];
        foreach($tag_ids as $tag_id) {
            $tag = TagModel::where('id', '=', $tag_id)->first();
            if($tag != null) {
                array_push($items, $tag);
            }
        }
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        return $res;
    }
    
    public function updateGoods(Request $request) {
        $goods = GoodsModel::where('id', '=', $request->input('id'))->first();
        $goods->tag_ids = $request->input('tag_ids');
        if($request->input('tag_ids') == '') {
            $goods->tags = '';
            $goods->save();
            return ResultTool::success();
        }
        $tag_ids = explode('|', $request->input('tag_ids'));
        $tag_str = '';
        foreach($tag_ids as $tag_id) {
            $tag = TagModel::where('id', '=', $tag_id)->first();
            if($tag_str != '') {
                $tag_str .= '|';
            }
            $tag_str .= $tag->name;
        }
        $goods->tags = $tag_str;
        $goods->save();
        return ResultTool::success();
    }
}
<?php
/*
 * @Author: lokei
 * @Date: 2023-02-09 17:11:49
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-15 17:39:18
 * @Description: 
 */
namespace App\Http\Controllers\Goods;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Goods\GoodsMarketingModel;
use Illuminate\Http\Request;

class MarketingController extends Controller {
    public function get(Request $request) {
        $goods = GoodsMarketingModel::where('id', '=', $request->input('id'))->first();
        if(is_null($goods)) {
            $goods = new GoodsMarketingModel();
            $goods->id = $request->input('id');
            $goods->member_mode = '0';
            $goods->save();
        }
        $res = ResultTool::success();
        $res['data']['member_mode'] = $goods->member_mode;
        $res['data']['member_discount'] = $goods->member_discount;
        // $res['data']['member_on'] = $goods->member_on;
        // $res['data']['all_member_level_on'] = $goods->all_member_level_on;
        // $res['data']['member_level_on_list'] = $goods->member_level_on_list;
        return $res;
    }

    public function set(Request $request) {
        $goods = GoodsMarketingModel::where('id', '=', $request->input('id'))->first();
        $goods->member_mode = $request->input('member_mode');
        $goods->member_discount = $request->input('member_discount');
        // $goods->member_on = $request->input('member_on');
        // $goods->all_member_level_on = $request->input('all_member_level_on');
        // $goods->member_level_on_list = $request->input('member_level_on_list');
        $goods->save();
        return ResultTool::success();
    }
}
<?php
/*
 * @Author: lokei
 * @Date: 2023-11-11 18:15:39
 * @LastEditors: lokei
 * @LastEditTime: 2023-11-15 17:14:34
 * @Description: 
 */
namespace App\Http\Controllers\Goods;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Goods\FxModel;
use Illuminate\Http\Request;

class FxController extends Controller {
    public function get(Request $request) {
        $fx = FxModel::where('goods_id', '=', $request->input('goods_id'))->first();
        $res = ResultTool::success();
        $res['data'] = $fx;
        return $res;
    }

    public function set(Request $request) {
        $fx = FxModel::where('goods_id', '=', $request->input('goods_id'))->first();
        if(is_null($fx)) {
            $fx = new FxModel();
            $fx->goods_id = $request->input('goods_id');
        }
        $fx->fx_custom_on = $request->input('fx_custom_on');
        $fx->self_deduct = $request->input('self_deduct');
        $fx->deduct_mode = $request->input('deduct_mode');
        $fx->deduct_money_1 = $request->input('deduct_money_1');
        $fx->deduct_ratio_1 = $request->input('deduct_ratio_1');
        $fx->self_deduct_mode = $request->input('self_deduct_mode');
        $fx->self_deduct_money_1 = $request->input('self_deduct_money_1');
        $fx->self_deduct_ratio_1 = $request->input('self_deduct_ratio_1');
        $fx->save();
        return ResultTool::success();
    }
}
<?php
/*
 * @Author: lokei
 * @Date: 2023-07-13 09:26:24
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-13 14:43:58
 * @Description: 
 */
namespace App\Http\Controllers\Goods;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Middle\StockMiddle;
use App\Models\Goods\NormModel;
use App\Models\Goods\NormValueModel;
use Illuminate\Http\Request;

class NormController extends Controller {
    public function ofGoods(Request $request) {
        $items = NormModel::where('goods_id', '=', $request->input('id'))->with('values')->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function create(Request $request) {
        $norm = new NormModel();
        $norm->goods_id = $request->input('goods_id');
        $norm->name = $request->input('name');
        $norm->save();
        StockMiddle::reset($request->input('goods_id'));
        return ResultTool::success();
    }

    public function update(Request $request) {
        $norm = NormModel::where('id', '=', $request->input('id'))->first();
        $norm->name = $request->input('name');
        $norm->save();
        return ResultTool::success();
    }

    public function delete(Request $request) {
        $norm = NormModel::where('id', '=', $request->input('id'))->first();
        $goods_id = $norm->goods_id;
        NormValueModel::where('norm_id', '=', $request->input('id'))->delete();
        NormModel::where('id', '=', $request->input('id'))->delete();
        StockMiddle::reset($goods_id);
        return ResultTool::success();
    }
}
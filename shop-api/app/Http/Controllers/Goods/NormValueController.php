<?php
/*
 * @Author: lokei
 * @Date: 2023-07-13 13:45:10
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-13 14:47:17
 * @Description: 
 */
namespace App\Http\Controllers\Goods;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Middle\StockMiddle;
use App\Models\Goods\NormModel;
use App\Models\Goods\NormValueModel;
use Illuminate\Http\Request;

class NormValueController extends Controller {
    public function create(Request $request) {
        $norm_value = new NormValueModel();
        $norm_value->norm_id = $request->input('norm_id');
        $norm_value->value = $request->input('value');
        $norm_value->save();
        $norm = NormModel::where('id', '=', $request->input('norm_id'))->first();
        StockMiddle::reset($norm->goods_id);
        return ResultTool::success();
    }

    public function update(Request $request) {
        $norm_value = NormValueModel::where('id', '=', $request->input('id'))->first();
        $norm_value->value = $request->input('value');
        $norm_value->save();
        return ResultTool::success();
    }

    public function delete(Request $request) {
        $norm_value = NormValueModel::where('id', '=', $request->input('id'))->first();
        $norm = NormModel::where('id', '=', $norm_value->norm_id)->first();
        NormValueModel::where('id', '=', $request->input('id'))->delete();
        StockMiddle::reset($norm->goods_id);
        return ResultTool::success();
    }
}
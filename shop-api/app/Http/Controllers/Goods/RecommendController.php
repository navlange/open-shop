<?php
/*
 * @Author: lokei
 * @Date: 2023-05-28 22:55:44
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-13 21:48:13
 * @Description: 
 */
namespace App\Http\Controllers\Goods;

use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Goods\GoodsRecommendModel;
use Illuminate\Http\Request;

class RecommendController extends Controller {
    public function get(Request $request) {
        $recommend = GoodsRecommendModel::where('scene', '=', $request->input('scene'))->first();
        if(is_null($recommend)) {
            $recommend = new GoodsRecommendModel();
            $recommend->scene = $request->input('scene');
            $recommend->save();
        }
        $res = ResultTool::success();
        $res['data']['goods_list'] = $recommend->goods_list;
        return $res;
    }

    public function set(Request $request) {
        $recommend = GoodsRecommendModel::where('scene', '=', $request->input('scene'))->first();
        $recommend->goods_list = $request->input('goods_list');
        $recommend->save();
        return ResultTool::success();
    }
}
<?php
/*
 * @Author: lokei
 * @Date: 2023-02-08 00:00:48
 * @LastEditors: lokei
 * @LastEditTime: 2024-10-09 19:00:04
 * @Description: 
 */

namespace App\Http\Controllers\Goods;

use App\Common\Permission\RoleGuard;
use App\Common\Tools\ResultTool;
use App\Http\Controllers\Controller;
use App\Models\Goods\GoodsModel;
use App\Models\Goods\GoodsRecommendModel;
use Illuminate\Http\Request;
use App\Models\ERP\Material\GoodsModel as MaterialGoodsModel;
use App\Models\Smt\Conf\ShopModel;

class GoodsController extends Controller
{

    public function get(Request $request)
    {
        $goods = GoodsModel::where('id', '=', $request->input('id'))->first();
        $res = ResultTool::success();
        $res['data'] = $goods;
        return $res;
    }

    public function list(Request $request)
    {
        $model = RoleGuard::store_guard(null, 'App\Models\Goods\GoodsModel');

        // if($request->input('app_mode') != null && $request->input('app_mode') != '') {
        //     if($model != null) {
        //         $model = $model->where('app_mode', '=', $request->input('app_mode'));
        //     } else {
        //         $model = GoodsModel::where('app_mode', '=', $request->input('app_mode'));
        //     }
        // }
        if ($request->input('name') != null && $request->input('name') != '') {
            if ($model != null) {
                $model->where('name', 'LIKE', "%" . $request->input('name') . "%");
            } else {
                $model = GoodsModel::where('name', 'LIKE', "%" . $request->input('name') . "%");
            }
        }
        if ($request->input('store_id') != null && $request->input('store_id') != '') {
            if ($model != null) {
                $model = $model->where('store_id', '=', $request->input('store_id'));
            } else {
                $model = GoodsModel::where('store_id', '=', $request->input('store_id'));
            }
        }

        if ($request->input('limit') != null && $request->input('limit') != '' && $request->input('is_downloading') == null) {
            if (is_null($model)) {
                $total = GoodsModel::count();
                $items = GoodsModel::limit($request->input('limit'))->offset((intval($request->input('page')) - 1) * intval($request->input('limit')))->get();
            } else {
                $total = $model->count();
                $items = $model->limit($request->input('limit'))->offset((intval($request->input('page')) - 1) * intval($request->input('limit')))->get();
            }
        } else {
            if (is_null($model)) {
                $items = GoodsModel::get();
                $total = count($items);
            } else {
                $model = $model;
                $items = $model->get();
                $total = count($items);
            }
        }

        // $conf_shop = CateringModel::first();
        // if (!is_null($conf_shop) && $conf_shop->stock_mode == '1') {
        //     foreach ($items as $item) {
        //         $stock_list = MaterialGoodsModel::where('goods_id', '=', $item->id)->get();
        //         $stock_amount = 0;
        //         $stock_sales = 0;
        //         foreach ($stock_list as $stock) {
        //             $stock_amount += $stock->amount;
        //             $stock_sales += $stock->sales;
        //         }
        //         $item->amount = $stock_amount;
        //         $item->sales = $stock_sales;
        //     }
        // }

        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = $total;
        return $res;
    }

    public function filter(Request $request)
    {
        $model = GoodsModel::where('status', '=', '1');
        $recommend_scene = $request->input("recommend_scene");
        if ($recommend_scene != null && $recommend_scene != '') {
            $recommend = GoodsRecommendModel::where('scene', '=', $recommend_scene)->first();
            if (!is_null($recommend)) {
                $ids = explode(';', $recommend->goods_list);
                $items = $model->whereIn('id', $ids)->get();
            } else {
                $items = null;
            }
        } else {
            if ($request->input('tag_id') != null && $request->input('tag_id') != '' && intval($request->input('tag_id')) > 0) {
                $model->where('tag_id', '=', $request->input('tag_id'));
            }
            if ($request->input('type_id') != null && $request->input('type_id') != '' && intval($request->input('type_id')) > 0) {
                $model->where('type_id', '=', $request->input('type_id'));
            }
            if ($request->input('store_id') != null && $request->input('store_id') != '' && intval($request->input('store_id')) > 0) {
                $model->where('store_id', '=', $request->input('store_id'));
            }
            if ($request->input('search_key') != null && $request->input('search_key') != '') {
                $model->where('name', 'LIKE', "%" . $request->input('search_key') . "%");
            }
            $items = $model->orderBy('order_index', 'desc')->get();
            // if ($request->input('app_mode') != null && $request->input('app_mode') == '10') { // 2024-10-09 18:54:48圣元宝取消注释
                $conf_shop = ShopModel::first();
                if (!is_null($conf_shop) && $conf_shop->stock_mode == '1') {
                    foreach ($items as $item) {
                        $stock_list = MaterialGoodsModel::where('goods_id', '=', $item->id)->get();
                        $stock_amount = 0;
                        $stock_sales = 0;
                        foreach ($stock_list as $stock) {
                            $stock_amount += $stock->amount;
                            $stock_sales += $stock->sales;
                        }
                        $item->amount = $stock_amount;
                        $item->sales = $stock_sales;
                    }
                }
            // }
        }
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function create(Request $request)
    {
        $goods = new GoodsModel();
        $goods->app_mode = $request->input('app_mode');
        $goods->name = $request->input('name');
        if ($request->input('store_id') != null) {
            $goods->store_id = $request->input('store_id');
        }
        if ($request->input('type_id') != null) {
            $goods->type_id = $request->input('type_id');
        }
        if ($request->input('price') != null) {
            $goods->price = $request->input('price');
        }
        if ($request->input('purchase_price') != null) {
            $goods->purchase_price = $request->input('purchase_price');
        }
        if ($request->input('unit') != null) {
            $goods->unit = $request->input('unit');
        }
        if ($request->input('integral') != null) {
            $goods->integral = $request->input('integral');
        }
        if ($request->input('amount') != null) {
            $goods->amount = $request->input('amount');
        }
        if ($request->input('sales') != null) {
            $goods->sales = $request->input('sales');
        }
        if ($request->input('status') != null) {
            $goods->status = $request->input('status');
        }
        $goods->intro = $request->input('intro');
        $goods->detail = $request->input('detail');
        $goods->img_thumb = $request->input('img_thumb');
        $goods->img_thumb_only = $request->input('img_thumb_only');
        $goods->img_display = $request->input('img_display');
        if ($request->input('gallery') != null) {
            $goods->gallery = $request->input('gallery');
        }

        $goods->save();
        return ResultTool::success();
    }

    public function update(Request $request)
    {
        $goods = GoodsModel::where('id', '=', $request->input('id'))->first();
        $goods->name = $request->input('name');
        if ($request->input('store_id') != null) {
            $goods->store_id = $request->input('store_id');
        }
        if ($request->input('type_id') != null) {
            $goods->type_id = $request->input('type_id');
        }
        if ($request->input('price') != null) {
            $goods->price = $request->input('price');
        }
        if ($request->input('purchase_price') != null) {
            $goods->purchase_price = $request->input('purchase_price');
        }
        if ($request->input('unit') != null) {
            $goods->unit = $request->input('unit');
        }
        if ($request->input('integral') != null) {
            $goods->integral = $request->input('integral');
        }
        if ($request->input('amount') != null) {
            $goods->amount = $request->input('amount');
        }
        if ($request->input('sales') != null) {
            $goods->sales = $request->input('sales');
        }
        if ($request->input('status') != null) {
            $goods->status = $request->input('status');
        }
        $goods->intro = $request->input('intro');
        $goods->detail = $request->input('detail');
        $goods->img_thumb = $request->input('img_thumb');
        $goods->img_thumb_only = $request->input('img_thumb_only');
        $goods->img_display = $request->input('img_display');
        if ($request->input('gallery') != null) {
            $goods->gallery = $request->input('gallery');
        }

        $goods->save();
        return ResultTool::success();
    }

    public function setOrder(Request $request)
    {
        $goods = GoodsModel::where('id', '=', $request->input('goods_id'))->first();
        $goods->order_index = $request->input('order_index');
        $goods->save();
        return ResultTool::success();
    }

    public function ofIds(Request $request)
    {
        $ids = explode('|', $request->input('ids'));
        $items = GoodsModel::whereIn('id', $ids)->get();
        $res = ResultTool::success();
        $res['data']['items'] = $items;
        $res['data']['total'] = count($items);
        return $res;
    }

    public function setOn(Request $request)
    {
        $goods = GoodsModel::where('id', '=', $request->input('id'))->first();
        $goods->status = '1';
        $goods->save();
        return ResultTool::success();
    }

    public function setOff(Request $request)
    {
        $goods = GoodsModel::where('id', '=', $request->input('id'))->first();
        $goods->status = '0';
        $goods->save();
        return ResultTool::success();
    }
}

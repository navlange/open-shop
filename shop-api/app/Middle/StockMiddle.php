<?php
/*
 * @Author: lokei
 * @Date: 2023-07-13 11:56:01
 * @LastEditors: lokei
 * @LastEditTime: 2023-07-13 12:01:00
 * @Description: 
 */
namespace App\Middle;

use App\Models\Goods\GoodsModel;
use App\Models\Goods\NormModel;
use App\Models\Goods\NormValueModel;
use App\Models\Goods\StockModel;

class StockMiddle {
    public static function reset($goods_id) {
		StockModel::where('goods_id', '=', $goods_id)->delete();
		$norms = NormModel::where('goods_id', '=', $goods_id)->orderBy('id', 'desc')->get();
		foreach ($norms as &$norm) {
			$norm_values = NormValueModel::where('norm_id', '=', $norm->id)->get();
			$norm->values = $norm_values;
		}
		$content = array();
		$all_stock = array();
		StockMiddle::getsulie($norms, $content, 0, $all_stock);
		if(count($all_stock) > 0) {
			$goods = GoodsModel::where('id', '=', $goods_id)->first();
			foreach($all_stock as $stock) {
				$new_stock = new StockModel();
				$new_stock->goods_id = $goods_id;
				$new_stock->norms = json_encode($stock);
				$new_stock->price = $goods->price;
				$new_stock->amount = $goods->amount;
				$new_stock->sales =0;
				$new_stock->save();
			}
		}
	}

	public static function getsulie($list, $content, $deep, &$all_stock)
	{
		$i = 0;
		if ($deep > count($list)) {
			return;
		}
		foreach ($list as $norm) {
			if ($i == $deep) {
				foreach ($norm->values as $value) {
					
					// $vv = $content . $vv;
					// $content[$v->id] = $vv->id;
					$n_v = array();
					$n_v[$norm->id] = $value->id;
					foreach($content as $n => $v) {
						$n_v[$n] = $v;
					}
					if ($deep == count($list) - 1) {
						array_push($all_stock, $n_v);
					} else {
						StockMiddle::getsulie($list, $n_v, $deep + 1, $all_stock);
					}
				}
				break;
			}
			$i++;
		}
		return;
	}
}
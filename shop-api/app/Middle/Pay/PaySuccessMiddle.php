<?php
/*
 * @Author: lokei
 * @Date: 2024-03-21 20:48:40
 * @LastEditors: lokei
 * @LastEditTime: 2024-07-26 15:51:00
 * @Description: 
 */

namespace App\Middle\Pay;

use App\Common\Enums\PayMode;
use App\Models\ERP\Material\GoodsModel;
use App\Models\Order\OrderGoodsModel;
use App\Models\Order\OrderModel;
use App\Models\OrderSmt\OrderSmtModel;
use App\Models\Pay\Income\IncomeModel;
use App\Models\Platform\Print\PrinterModel;
use App\Models\Smt\Conf\ShopModel;
use App\Models\Store\StoreModel;
use Illuminate\Support\Facades\Redis;
use ReflectionClass;

class PaySuccessMiddle
{
    public static function success($order_id, $pay_mode, $money)
    {
        $order = OrderModel::find($order_id);
        if ($order->status == '0') {
            $order->status = 1;
            $order->pay_mode = $pay_mode;
            $order->payed_money = $money;
            // 计算排队号
            $queue_number = 0;
            if (Redis::exists('queue_number')) {
                $queue_number = Redis::incr('queue_number');
            } else {
                $queue_number = 1;
                Redis::set('queue_number', $queue_number);
                // 设置过期时间为凌晨
                $expire_time = strtotime(date('Y-m-d', strtotime('+1 day'))) - time();
                Redis::expire('queue_number', $expire_time);
            }
            // queue_number 转为3位字符串
            $queue_number_str = str_pad($queue_number, 3, '0', STR_PAD_LEFT);
            $order->queue_number = $queue_number_str;
            $order->save();

            $order_smt = OrderSmtModel::where('id', $order_id)->first();
            $order_smt->status = 1;
            $order_smt->save();

            // 库存
            $goods_list = OrderGoodsModel::where('order_id', '=', $order_id)->get();
            $conf_shop = ShopModel::first();
            for ($i = 0; $i < count($goods_list); $i++) {
                if ($conf_shop != null && $conf_shop->stock_mode == '1') {
                    $material_goods_list = GoodsModel::where('goods_id', '=', $goods_list[$i]->goods_id)->get();
                    $left_amount = $goods_list[$i]->amount;
                    for ($j = 0; $j < count($material_goods_list); $j++) {
                        if ($material_goods_list[$j]->amount > $material_goods_list[$j]->sales) {
                            if ($material_goods_list[$j]->amount - $material_goods_list[$j]->sales >= $left_amount) {
                                $material_goods_list[$j]->sales += $left_amount;
                                $material_goods_list[$j]->save();
                                $left_amount = 0;
                                break;
                            } else {
                                $left_amount -= $material_goods_list[$j]->amount - $material_goods_list[$j]->sales;
                                $material_goods_list[$j]->sales = $material_goods_list[$j]->amount;
                                $material_goods_list[$j]->save();
                            }
                        }
                    }
                    if ($left_amount > 0 && count($material_goods_list) > 0) {
                        $material_goods_list[0]->sales += $left_amount;
                        $material_goods_list[0]->save();
                    }
                }
            }

            // 收入管理
            if ($order->store_id > 0 && $money > 0) {
                $store = StoreModel::where('id', '=', $order->store_id)->first();
                if ($store) {
                    // 商家收入
                    $store_income = $money;
                    if ($store->store_income_mode == 1) {
                        if ($money > $store->store_income_money_by_platform) {
                            $store_income = $money - $store->store_income_money_by_platform;
                        }
                    } else if ($store->store_income_mode == 2) {
                        $store_income = $money * (100 - $store->store_income_ratio_by_platform) / 100;
                    }
                    $description = '订单编号：' . $order->sn;
                    if ($order->pay_note) {
                        $description .= '；备注：' . $order->pay_note;
                    }
                    $status = 1;
                    $withdrawable = 0;
                    if ($pay_mode == PayMode::WECHAT['value']) {
                        $status = 0;
                        $withdrawable = 1;
                    }
                    // $store = StoreModel::where('id', '=', $order->store_id)->first();
                    if ($order->app_mode == 2 && $pay_mode == 1) {
                        $withdrawable = 1;
                        if ($store->income) {
                            $store->income += $store_income;
                        } else {
                            $store->income = $store_income;
                        }
                        $store->save();
                    }
                    // $income = new IncomeModel();
                    // $income->incIncome('store', 0, $store->id, $order->pay_mode, $money, $store_income, $description, $order->app_mode, $order->id, $store->name, $store->mobile, $order->user_info, $order->goods_info, $status, $withdrawable);
                    $record = new IncomeModel();
                    $record->type = 'store';
                    $record->store_id = $store->id;
                    $record->store_name = $store->name;
                    $record->Store_mobile = $store->mobile;
                    $record->pay_mode = $pay_mode;
                    $record->money = $money;
                    $record->income = $store_income;
                    $record->deduct = $store_income;
                    $record->description = $description;
                    $record->app_mode = $order->app_mode;
                    $record->order_id = $order_id;
                    $record->user_info = $order->user_info;
                    $record->goods_info = $order->goods_info;
                    $record->createtime = date('Y-m-d H:i:s');
                    $record->status = $status;
                    $record->withdrawable = $withdrawable;
                    $record->save();
                }
            }

            // 打印小票
            if ($pay_mode == PayMode::OFFLINE['value']) {
                if ($order->store_id != null && intval($order->store_id) > 0) {
                    $fields = array();
                    $content = '<CB>订单打印</CB><BR>';
                    $goods_info = json_decode($order->goods_info);
                    $content .= '名称　　　　　 数量  单价  小计<BR>';
                    $content .= '--------------------------------<BR>';
                    $total_price = 0;
                    foreach ($goods_info as $goods) {
                        $content .= $goods->name;
                        $name_len = mb_strlen($goods->name, 'utf-8');
                        if ($name_len < 9) {
                            for ($i = 0; $i < (9 - $name_len); $i++) {
                                $content .= '　';
                            }
                        }
                        $content .= $goods->amount . '  ' . $goods->price . '  ' . number_format((intval($goods->amount) * floatval($goods->price)), 2) . '<BR>';
                        $content .= '--------------------------------<BR>';
                        $total_price += intval($goods->amount) * floatval($goods->price);
                    }
                    $content .= '合计：' . number_format($total_price, 2) . '元<BR>';
                    $content .= '支付：' . number_format($order->payed_money, 2) . '元<BR>';
                    // $content .= '送货地点：广州市南沙区xx路xx号<BR>';
                    $content .= '店名：' . $order->store_info . '<BR>';
                    $content .= '单号：' . $order->sn . '<BR>';
                    $content .= '联系电话：' . $order->consignee_mobile . '<BR>';
                    $content .= '下单时间：' . $order->created_at . '<BR>';

                    $reflection = new ReflectionClass(PayMode::class);
                    $pay_mode_str = '';
                    foreach ($reflection->getConstants() as $key => $value) {
                        if ($value['value'] == $order->pay_mode) {
                            $pay_mode_str = $value['name'];
                            break;
                        }
                    }

                    $content .= '支付方式：' . $pay_mode_str . '<BR>';
                    $content .= '点单信息：' . $order->consignee_info ? $order->consignee_info : '' . '<BR>';
                    $content .= '备注信息：' . ($order->note ? $order->note : '') . ($order->pay_note ? (' ' . $order->pay_note) : '') . '<BR>';
                    // if ($request->input('custom_content') != null) {
                    //     $content .= $request->input('custom_content');
                    // }
                    // $content .= '订餐时间：2016-08-08 08:08:08<BR>';
                    // $content .= '<QR>http://www.dzist.com</QR>';
                    $fields['content'] = $content;
                    $printer_list = PrinterModel::where('store_id', '=', $order->store_id)->get();
                    foreach ($printer_list as $printer) {
                        $print_count = 1;
                        if ($printer->print_count != null && $printer->print_count > 1) {
                            $print_count = $printer->print_count;
                        }
                        $fields['printer'] = json_encode([
                            'type' => $printer->type,
                            'user' => $printer->user,
                            'ukey' => $printer->ukey,
                            'sn' => $printer->sn,
                            'print_number' => $print_count
                        ]);
                        Redis::xAdd('smt_print', '*', $fields);
                    }
                }
            }
        }
    }
}

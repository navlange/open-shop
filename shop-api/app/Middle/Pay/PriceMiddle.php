<?php
/*
 * @Author: lokei
 * @Date: 2024-03-29 10:40:56
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-18 04:12:04
 * @Description: 
 */
namespace App\Middle\Pay;

use App\Models\Goods\GoodsModel;
use App\Models\Order\OrderGoodsModel;
use App\Models\Order\OrderModel;

class PriceMiddle {
    public static function ofOrder($order_id) {
        $price_info = [];
        $order = OrderModel::where('id', '=', $order_id)->first();
        if($order->is_multiple == '0') {
            $goods = GoodsModel::where('id', '=', $order->goods_id)->first();
            $price = $goods->price * $order->amount;
            $price_info['total_price'] = number_format($price, 2, '.', '');
            $price_info['discount_price'] = number_format($price, 2, '.', '');
        } else if ($order->is_multiple == '1') {
            $order_goods_list = OrderGoodsModel::where('order_id', '=', $order_id)->get();
            $total_price = 0;
            $discount_price = 0;
            foreach($order_goods_list as $order_goods) {
                $goods = GoodsModel::where('id', '=', $order_goods->goods_id)->first();
                $legal_amount = $order_goods->amount;
                if($order_goods->refund_amount > 0) {
                    $legal_amount -= $order_goods->refund_amount;
                }
                $total_price += $goods->price * $legal_amount;
                $discount_price += $goods->price * $legal_amount;
            }
            $price_info['total_price'] = number_format($total_price, 2, '.', '');
            $price_info['discount_price'] = number_format($discount_price, 2, '.', '');
        }
        return $price_info;
    }
}
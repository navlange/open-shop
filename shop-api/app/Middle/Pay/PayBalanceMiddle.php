<?php
/*
 * @Author: lokei
 * @Date: 2024-03-21 20:30:04
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-18 07:02:21
 * @Description: 
 */
namespace App\Middle\Pay;

use App\Common\Enums\PayMode;
use App\Common\Tools\Sn;
use App\Models\Pay\AccountModel;
use App\Models\Pay\Record\PayRecordModel;

class PayBalanceMiddle
{
    public static function pay($uid, $to_pay, $order_id, $description = '')
    {
        $account = AccountModel::where('uid', $uid)->first();
        if(is_null($account))
        {
            $account = AccountModel::init($uid);
        }
        if($account->balance < $to_pay)
        {
            return [
                'code' => 1,
                'msg' => '余额不足'
            ];
        }
        $account->balance -= $to_pay;
        $account->save();

        $pay = new PayRecordModel();
        $pay->uid = $uid;
        $pay->mode = '0';
        $pay->pay_mode = PayMode::BALANCE['value'];
        $pay->money = $to_pay;
        $pay->createtime = date('Y-m-d H:i:s', time());
        $pay->tid = Sn::build_order_no();
        $pay->status = '1';
        $pay->order_id = $order_id;
        $pay->description = $description;
        $pay->save();
        
        return [
            'code' => 0,
            'msg' => '支付成功'
        ];
    }
}
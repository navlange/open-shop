<?php
/*
 * @Author: lokei
 * @Date: 2024-03-21 20:30:04
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-18 07:02:03
 * @Description: 
 */
namespace App\Middle\Pay;

use App\Common\Enums\PayMode;
use App\Common\Tools\Sn;
use App\Models\Pay\Card\CardRecordModel;
use App\Models\Pay\Record\PayRecordModel;

class PayCardMiddle
{
    public static function pay($record_id, $to_pay, $order_id, $description = '')
    {
        $card_record = CardRecordModel::where('id', $record_id)->first();
        if(is_null($card_record))
        {
            return [
                'code' => 2,
                'msg' => '储值卡不存在'
            ];
        }
        if($card_record->money < $to_pay)
        {
            return [
                'code' => 1,
                'msg' => '余额不足'
            ];
        }
        $card_record->money -= $to_pay;
        $card_record->save();

        $pay = new PayRecordModel();
        $pay->uid = $card_record->uid;
        $pay->mode = '0';
        $pay->pay_mode = PayMode::PAYCARD['value'];
        $pay->money = $to_pay;
        $pay->createtime = date('Y-m-d H:i:s', time());
        $pay->tid = Sn::build_order_no();
        $pay->status = '1';
        $pay->pay_card_record_id = $record_id;
        $pay->order_id = $order_id;
        $pay->store_id = $card_record->store_id;
        $pay->description = $description;
        $pay->save();

        return [
            'code' => 0,
            'msg' => '支付成功'
        ];
    }
}
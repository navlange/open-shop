<?php
/*
 * @Author: lokei
 * @Date: 2024-04-17 13:58:07
 * @LastEditors: lokei
 * @LastEditTime: 2024-07-28 00:22:03
 * @Description: 
 */

namespace App\Middle\Order;

use App\Models\Message\Template\TemplateModel;
use App\Models\Order\OrderModel;
use Illuminate\Support\Facades\Redis;
use App\Models\Platform\Print\PrinterModel;
use App\Models\Store\StoreModel;
use App\Models\Ucenter\FansModel;

class ReleaseSuccessMiddle
{
    public static function success($order_id, $host)
    {
        $order = OrderModel::where('id', '=', $order_id)->first();
        if ($order->store_id != null && intval($order->store_id) > 0) {
            $fields = array();
            $content = '<CB>订单打印</CB><BR>';
            // $content .= '名称　　　　　 单价  数量 金额<BR>';
            // $content .= '--------------------------------<BR>';
            // $content .= '饭　　　　　　 1.0    1   1.0<BR>';
            // $content .= '炒饭　　　　　 10.0   10  10.0<BR>';
            // $content .= '蛋炒饭　　　　 10.0   10  100.0<BR>';
            // $content .= '鸡蛋炒饭　　　 100.0  1   100.0<BR>';
            // $content .= '番茄蛋炒饭　　 1000.0 1   100.0<BR>';
            // $content .= '西红柿蛋炒饭　 1000.0 1   100.0<BR>';
            // $content .= '西红柿鸡蛋炒饭 100.0  10  100.0<BR>';
            // $content .= '备注：加辣<BR>';
            $goods_info = json_decode($order->goods_info);
            $content .= '名称　　　　　 数量  单价  小计<BR>';
            $content .= '--------------------------------<BR>';
            $total_price = 0;
            foreach ($goods_info as $goods) {
                $content .= $goods->name;
                $name_len = mb_strlen($goods->name, 'utf-8');
                if ($name_len < 9) {
                    for ($i = 0; $i < (9 - $name_len); $i++) {
                        $content .= '　';
                    }
                }
                $content .= $goods->amount . '  ' . $goods->price . '  ' . number_format((intval($goods->amount) * floatval($goods->price)), 2) . '<BR>';
                $content .= '--------------------------------<BR>';
                $total_price += intval($goods->amount) * floatval($goods->price);
            }
            $content .= '合计：' . number_format($total_price, 2) . '元<BR>';
            // $content .= '送货地点：广州市南沙区xx路xx号<BR>';
            $content .= '店名：' . $order->store_info . '<BR>';
            $content .= '单号：' . $order->sn . '<BR>';
            $content .= '联系电话：' . $order->consignee_mobile . '<BR>';
            $content .= '下单时间：' . $order->created_at . '<BR>';
            $content .= '支付方式：' . $order->pay_mode . '<BR>';
            $content .= '点单信息：' . $order->consignee_info ? $order->consignee_info : '' . '<BR>';
            $content .= '备注信息：' . ($order->note ? $order->note : '') . '<BR>';
            // if ($request->input('custom_content') != null) {
            //     $content .= $request->input('custom_content');
            // }
            // $content .= '订餐时间：2016-08-08 08:08:08<BR>';
            // $content .= '<QR>http://www.dzist.com</QR>';
            $fields['content'] = $content;
            $printer_list = PrinterModel::where('store_id', '=', $order->store_id)->get();
            foreach ($printer_list as $printer) {
                $print_count = 1;
                if ($printer->print_count != null && $printer->print_count > 1) {
                    $print_count = $printer->print_count;
                }
                $fields['printer'] = json_encode([
                    'type' => $printer->type,
                    'user' => $printer->user,
                    'ukey' => $printer->ukey,
                    'sn' => $printer->sn,
                    'print_number' => $print_count
                ]);
                Redis::xAdd('smt_print', '*', $fields);
            }

            // 发送消息
            $template = TemplateModel::where('identity', '=', 'new_order_2_store')->first();
            if (!is_null($template)) {
                $store = StoreModel::where('id', '=', $order->store_id)->first();
                if ($store->uid > 0) {
                    $fans = FansModel::where('uid', '=', $store->uid)->where('platform', '=', 'mp')->first();
                    if (!is_null($fans)) {
                        $fields = array();
                        $fields['type'] = 'mp';
                        $fields['pid'] = config('app.proj_identity');
                        $fields['touser'] = $fans->openid;
                        $fields['template_id'] = $template->template_id;
                        $fields['url'] ='https://' . $host . '/html/shop/mobile' . ((config('app.proj_identity') != null && config('app.proj_identity') != '') ? ('?pid=' . config('app.proj_identity')) : '') . '#/order/pages/info/goods?id=' . $order->id;
                        $fields['keyword_amount'] = 4;
                        $fields['first'] = '您收到了一条新的订单。';

                        $keyword1 = [
                            'name' => 'character_string1',
                            'value' => $order->sn
                        ];
                        $fields['keyword1'] = json_encode($keyword1);

                        $user_info = json_decode($order->user_info);
                        $keyword2 = [
                            'name' => 'thing19',
                            'value' => $user_info->name . ' ' . $user_info->mobile
                        ];
                        $fields['keyword2'] = json_encode($keyword2);

                        $goods_info = json_decode($order->goods_info);
                        $goods_str = '';
                        foreach ($goods_info as $goods) {
                            $goods_str .= $goods->name . '*' . $goods->amount . '\n';
                        }
                        $keyword3 = [
                            'name' => 'thing8',
                            'value' => $goods_str
                        ];
                        $fields['keyword3'] = json_encode($keyword3);

                        $keyword4 = [
                            'name' => 'time2',
                            'value' => date('Y年m月d日 H:i', strtotime($order->created_at))
                        ];
                        $fields['keyword4'] = json_encode($keyword4);

                        $fields['remark'] = '请及时处理。';
                        Redis::xAdd('send_message', '*', $fields);
                    }
                }
            }
        }
    }
}

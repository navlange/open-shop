<?php
/*
 * @Author: lokei
 * @Date: 2022-12-31 11:37:18
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-17 12:02:41
 * @Description: 
 */

namespace App\Middle;

use App\Models\Widget\NavModel;

class NavMiddle
{
	public static function reset($mode)
	{
		NavModel::where('mode', '=', 'index')->delete();
		$nav = new NavModel();
		$nav->mode = $mode;
		$nav->name = "门店";
		$nav->wxapp_url = '"/store/pages/store/list"';
		$nav->display = "1";
		$nav->img = '""';
		$nav->save();
		$nav = new NavModel();
		$nav->mode = $mode;
		$nav->name = "商城";
		$nav->wxapp_url = '/goods/pages/goods/list';
		$nav->display = "1";
		$nav->img = "";
		$nav->save();
	}
}

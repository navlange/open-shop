<?php
/*
 * @Author: lokei
 * @Date: 2022-08-18 17:09:40
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-16 07:04:04
 * @Description: 
 */

namespace App\Common\Tools;

use Illuminate\Support\Facades\Redis;

class Sn
{
    public static function build_order_no()
    {
        list($t1, $t2) = explode(' ', microtime());
        return date('YmdHis', $t2) . $t1 * 1000000 . substr(implode(array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 6);
    }

    public static function build_order_no_day5() {
        // redis 当天递增
        $key = 'order_no_day5_' . date('Ymd');
        if(!Redis::exists($key)) {
            // 第二天自动清零
            Redis::setex($key, 86400, 0);
        }
        Redis::incr($key);
        $num = Redis::get($key);
        if (intval($num) < 10000) {
            $num = str_pad($num, 5, '0', STR_PAD_LEFT);
        }
        return date('Ymd') . $num;
    }
}

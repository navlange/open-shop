<?php
/*
 * @Author: lokei
 * @Date: 2022-04-26 08:10:40
 * @LastEditors: lokei
 * @LastEditTime: 2024-03-29 11:48:34
 * @Description: 
 */
namespace App\Common\Tools;

use App\Common\Enums\ResultCode;

class ResultTool {
	public static function success($data = []) {
		return [
			'errorCode' => (ResultCode::SUCCESS)['code'],
			'errorMsg' => (ResultCode::SUCCESS)['msg'],
			'data' => $data
		];
	}

	public static function fail($msg = '') {
		return [
			'errorCode' => (ResultCode::FAIL)['code'],
			'errorMsg' => $msg ? $msg : (ResultCode::FAIL)['msg'],
			'data' => []
		];
	}
}
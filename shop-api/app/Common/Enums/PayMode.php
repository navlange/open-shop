<?php
/*
 * @Author: lokei
 * @Date: 2024-03-21 20:20:51
 * @LastEditors: lokei
 * @LastEditTime: 2024-03-21 20:26:32
 * @Description: 
 */
// 支付方式枚举
namespace App\Common\Enums;

use MyCLabs\Enum\Enum;

class PayMode extends Enum
{
    const BALANCE = [
        'value' => '0',
        'name' => '余额支付'
    ];
    const WECHAT = [
        'value' => '1',
        'name' => '微信支付'
    ];
    const WEIXINTHIRD = [
        'value' => '1-5',
        'name' => '微信第三方支付'
    ];
    const MEMBER = [
        'value' => '2',
        'name' => '会员支付'
    ];
    const INTEGRAL = [
        'value' => '3',
        'name' => '积分兑换'
    ];
    const EXCHANGE = [
        'value' => '3-1',
        'name' => '兑换码兑换'
    ];
    const DYCOUPON = [
        'value' => '3-2',
        'name' => '抖音团购券兑换'
    ];
    const PAYCARD = [
        'value' => '4',
        'name' => '储值卡支付'
    ];
    const CLERK = [
        'value' => '6',
        'name' => '店员支付'
    ];
    const DOUYINDANBAO = [
        'value' => '7',
        'name' => '抖音担保支付'
    ];
    const ALIPAY = [
        'value' => '8',
        'name' => '支付宝支付'
    ];
    const OFFLINE = [
        'value' => '9',
        'name' => '线下支付'
    ];
    const NONEED = [
        'value' => '10',
        'name' => '无需支付'
    ];
}
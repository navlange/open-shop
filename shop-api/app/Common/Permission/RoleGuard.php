<?php
/*
 * @Author: lokei
 * @Date: 2023-07-25 15:06:06
 * @LastEditors: lokei
 * @LastEditTime: 2024-04-15 18:17:10
 * @Description: 
 */
namespace App\Common\Permission;

use App\Models\Clerk\ClerkModel;

class RoleGuard {
    public static function store_guard($model = null, $model_name, $id_name = 'store_id') {
        if (config('app.token.cid') != null && intval(config('app.token.cid')) > 0) {
            $clerk = ClerkModel::where('id', '=', intval(config('app.token.cid')))->first();
            if (!is_null($clerk) && ($clerk->role == 'store_admin' || $clerk->role == 'store_clerk')) {
                if($model != null) {
                    $model = $model->where($id_name, '=', $clerk->store_id);
                } else {
                    $model = $model_name::where($id_name, '=', $clerk->store_id);
                }
            }
        }
        return $model;
    }
}
import { request } from '@/common/js/request'

export function fetch(query: any) {
	return request('/smt/store/get', query, 'GET')
}

export function ofMe(query: any) {
	return request('/smt/store/ofMe', query, 'GET')
}

export function roleOfMe(query: any) {
	return request('/smt/store/roleOfMe', query, 'GET')
}

export function ofUid(query: any) {
	return request('/smt/store/ofUid', query, 'GET')
}

export function fetchDefault(query: any) {
	return request('/smt/store/getDefault', query, 'GET')
}

export function queryList(query: any) {
	if(query!= null || (query != null && !query.hasOwnProperty("key"))) {
		var params = Object.assign({status: '1'}, query)
	} else {
		var params = query
	}
	return request('/smt/store/list', params, 'GET')
}

export function queryOfIds(query: any) {
	return request('/smt/store/ofIds', query, 'GET')
}

export function register(query: any) {
	return request('/smt/store/register', query, 'POST')
}

export function update(query: any) {
	return request('/smt/store/update', query, 'POST')
}

export function getContactsMobile(query: any) {
	return request('/smt/store/contacts/getMobile', query, 'GET')
}

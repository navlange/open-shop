import { request } from '@/common/js/request'

export function get(query: any) {
	return request('/cms-api/widget/nav/pic/get', query, 'GET')
}

export function getHot(query: any) {
	return request('/cms-api/widget/nav/pic/hot/get', query, 'GET')
}

import { request } from '@/common/js/request'
import { appinfo } from '@/app.conf'

export function queryList(query: any) {
	if (appinfo.type === 'dz') {
		return new Promise((resolve, reject) => {
			let menu_list = []
			menu_list.push({
				name: '首页',
				customer_name: '首页',
				name_en: 'index',
				display: '1',
				icon_name: 'shouye',
				wxapp_url: '/platform/pages/index/index'
			})
			menu_list.push({
				name: '门店',
				customer_name: '门店',
				name_en: 'store_list',
				display: '1',
				icon_name: 'mendianliebiao',
				wxapp_url: '/shop/pages/store/list'
			})
			menu_list.push({
				name: '点餐',
				customer_name: '点餐',
				name_en: 'goods_list',
				display: '1',
				icon_name: 'diancan',
				wxapp_url: '/things/pages/list/index'
			})
			menu_list.push({
				name: '我的',
				customer_name: '我的',
				name_en: 'person',
				display: '1',
				icon_name: 'gerenzhongxinxia',
				wxapp_url: '/ucenter/pages/person/index'
			})
			resolve({ data: { items: menu_list } })
		})
	} else {
		return request('/smt/widget/menu/displayList', query, 'GET')
	}
}

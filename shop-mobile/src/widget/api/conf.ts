import { request } from '@/common/js/request'

export function get(query: any) {
  return request('cms-api/widget/conf/get', query, 'GET')
}
import { request } from '@/common/js/request'

export function get(query: any) {
	return request('/cms-api/widget/person/get', query, 'GET')
}

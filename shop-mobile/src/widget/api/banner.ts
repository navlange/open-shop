import { request } from '@/common/js/request'

export function queryList(query: any) {
  return request('/cms-api/widget/banner/list', query, 'GET')
}
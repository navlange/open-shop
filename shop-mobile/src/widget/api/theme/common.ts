import { request } from '@/common/js/request';
import { appinfo } from '@/app.conf';

export function queryTheme(query: any) {
	if (appinfo.type === 'dz') {
		return new Promise((resolve, reject) => {
			const theme = {
				color: appinfo.color,
			};
			resolve({ data: theme });
		});
	} else {
		return new Promise((resolve, reject) => {
			request('cms-api/widget/theme/get', query, 'GET')
				.then((res: any) => {
					uni.setStorageSync('theme', res.data.theme);
					resolve(res);
				})
				.catch((e) => {
					reject(e);
				});
		});
	}
}

import { request } from '@/common/js/request'

export function hasGoods(query: any) {
	return request('/smt/platform/type/hasGoods', query, 'GET')
}

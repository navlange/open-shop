import { request } from '@/common/js/request'

export function fetchList(query: any) {
	return request('/platform/type/list', query, 'GET')
}

export function ofMode(query: any) {
	return request('/api/type/ofMode', query, 'GET')
}

export function ofScene(query: any) {
	return request('/api/type/ofScene', query, 'GET')
}

export function ofSceneRoot(query: any) {
	return request('/api/type/ofSceneRoot', query, 'GET')
}

export function ofModeRoot(query: any) {
	return request('/api/type/ofModeRoot', query, 'GET')
}

export function ofPre(query: any) {
	return request('/api/type/ofPre', query, 'GET')
}

export function ofIndexArticleRecommend(query: any) {
	return request('/api/type/ofIndexArticleRecommend', query, 'GET')
}

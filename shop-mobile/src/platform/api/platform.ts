import { request } from '@/common/js/request'

export function we7uniaccount(query: any) {
  return request('/platform/we7uniaccount', query, 'GET')
}

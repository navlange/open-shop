import { request } from '@/common/js/request'
import { APPMODE } from '@/common/js/def'

export function checkAuth(query: any) {
	return request('/platform/smtapp/auth/check', query, 'GET')
}

export function ofProj(query: any) {
	return request('/smartore/smtapp/auth/ofProj', query, 'GET')
}

export function checkAuthOfAppMode(app_mode: string) {
	let auth_list = uni.getStorageSync('auth_list')
	for(var i=0; i<auth_list.length; i++) {
		if(auth_list[i] == app_mode) {
			return true
		}
	}
	return false
}

export function isMainAppMode(app_mode: string) {
	let auth_list = uni.getStorageSync('auth_list')
	let main_exist = false
	for(var i=0; i<auth_list.length; i++) {
		if(auth_list[i] == app_mode) {
			console.log(app_mode)
			main_exist = true
		}
	}
	if(main_exist) {
		return true
	} else {
		return false
	}
}

export function getMainAppMode() {
	var auth_list = uni.getStorageSync('auth_list')
	var main_app_mode = ''
	for(var i=0; i<auth_list.length; i++) {
		if(auth_list[i] == APPMODE.SHOP) {
			if(main_app_mode == '') {
				main_app_mode = APPMODE.SHOP
			} else {
				main_app_mode = ''
				break
			}
		} else if (auth_list[i] == APPMODE.TICKET) {
			if(main_app_mode == '') {
				main_app_mode = APPMODE.TICKET
			} else {
				main_app_mode = ''
				break
			}
		} else if (auth_list[i] == APPMODE.SEAT) {
			if(main_app_mode == '') {
				main_app_mode = APPMODE.SEAT
			} else {
				main_app_mode = ''
				break
			}
		} else if (auth_list[i] == APPMODE.SCENIC) {
			if(main_app_mode == '') {
				main_app_mode = APPMODE.SCENIC
			} else {
				main_app_mode = ''
				break
			}
		} else if (auth_list[i] == APPMODE.LINE) {
			if(main_app_mode == '') {
				main_app_mode = APPMODE.LINE
			} else {
				main_app_mode = ''
				break
			}
		} else if (auth_list[i] == APPMODE.ZL) {
			if(main_app_mode == '') {
				main_app_mode = APPMODE.ZL
			} else {
				main_app_mode = ''
				break
			}
		} else if (auth_list[i] == APPMODE.ZLMOBILE) {
			if(main_app_mode == '') {
				main_app_mode = APPMODE.ZLMOBILE
			} else {
				main_app_mode = ''
				break
			}
		} else if (auth_list[i] == APPMODE.ZLCAR) {
			if(main_app_mode == '') {
				main_app_mode = APPMODE.ZLCAR
			} else {
				main_app_mode = ''
				break
			}
		} else if (auth_list[i] == APPMODE.MASTER) {
			if(main_app_mode == '') {
				main_app_mode = APPMODE.MASTER
			} else {
				main_app_mode = ''
				break
			}
		} else if (auth_list[i] == APPMODE.SMTSERVICE) {
			if(main_app_mode == '') {
				main_app_mode = APPMODE.SMTSERVICE
			} else {
				main_app_mode = ''
				break
			}
		} else if (auth_list[i] == APPMODE.PLACE) {
			if(main_app_mode == '') {
				main_app_mode = APPMODE.PLACE
			} else {
				main_app_mode = ''
				break
			}
		} else if (auth_list[i] == APPMODE.CWASH) {
			if(main_app_mode == '') {
				main_app_mode = APPMODE.CWASH
			} else {
				main_app_mode = ''
				break
			}
		} else if (auth_list[i] == APPMODE.ROOM) {
			if(main_app_mode == '') {
				main_app_mode = APPMODE.ROOM
			} else {
				main_app_mode = ''
				break
			}
		} else if (auth_list[i] == APPMODE.CATERING) {
			if(main_app_mode == '') {
				main_app_mode = APPMODE.CATERING
			} else {
				main_app_mode = ''
				break
			}
		} else if (auth_list[i] == APPMODE.CHUXING) {
			if(main_app_mode == '') {
				main_app_mode = APPMODE.CHUXING
			} else {
				main_app_mode = ''
				break
			}
		}
	}
	return main_app_mode
}

import { request } from '@/common/js/request'

export function get(query) {
  return request('/api/conf/init/get', query, 'GET')
}

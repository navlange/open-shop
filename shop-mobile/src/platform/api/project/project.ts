import { request } from "@/common/js/request";
import { getUrlCode } from "@/common/js/util";

export function getMpAppid(query: any) {
    return request("/smartore/platform/project/getMpAppid", query, "GET");
}

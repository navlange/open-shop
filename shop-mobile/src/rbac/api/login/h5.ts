import { isWechat } from '@/common/js/platform';
import { request } from '@/common/js/request';

export function nav_login(back: any, pid = '') {
    uni.setStorageSync("login_back", back);
    if(isWechat()){
        uni.navigateTo({
            url: '/rbac/pages/login/mp?' + (pid !== '' ? ('pid=' + pid + '&') : '') + 'callback=' + encodeURIComponent(back)
        });
    } else {
        // uni.navigateTo({
        //     url: "/rbac/pages/login/password"
        // });
        request(
            "/api/rabc/conf/login/get",
            {},
            "GET"
        ).then((res: any) => {
            if (res.data.login_mode == "1") {
                let url =
                    "/rbac/pages/login/mobile";
                uni.navigateTo({
                    url: url,
                });
            } else {
                let url =
                    "/rbac/pages/login/password";
                uni.navigateTo({
                    url: url,
                });
            }
        });
    }
}

import { request } from '@/common/js/request'

export function ofMe(query: any) {
	return request('/api/rbac/username/ofMe', query, 'GET')
}

export function updateOfMe(query: any) {
	return request('/api/rbac/username/updateOfMe', query, 'POST')
}

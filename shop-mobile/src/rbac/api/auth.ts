import { request } from '@/common/js/request';
import { ofMe as userInfo } from '@/ucenter/api/user/info'

export function login(query: any) {
    return new Promise((resolve, reject) => {
        request(
            'api/auth/login', query,
            'POST'
        )
            .then((res: any) => {
                //成功回调
                if (res.data.hasOwnProperty('token')) {
                    uni.setStorageSync('token', res.data.token);
                    userInfo(null).then((res: any) => {
                        let user_info = uni.getStorageSync('user_info')
                        if (user_info !== null && user_info !== '' && user_info !== undefined) {
                            user_info = Object.assign(user_info, res.data)
                        } else {
                            user_info = res.data
                        }
                        uni.setStorageSync('user_info', user_info)
                        resolve(res.data.token);
                    })
                } else {
                    reject(res.errorMsg);
                }
            })
            .catch((err) => {
                //请求失败
                reject(err);
            });
    })
}
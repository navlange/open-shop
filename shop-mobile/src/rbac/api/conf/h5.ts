import { request } from '@/common/js/request'

export function get(query: any) {
	return request('/api/rabc/conf/h5/get', query, 'GET')
}

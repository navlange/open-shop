import { getUrlCode, getAppid } from "@/common/js/util";
import { url } from "@/common/js/url";
import { nav_login } from "@/rbac/api/login/h5";

export function request(
    path: string,
    query: any,
    method: any,
    contentType = null
) {
    return new Promise((resolve, reject) => {
        let types = "";
        if (method == "POST" && !contentType) {
            types = "application/x-www-form-urlencoded";
        } else if (method == "POST" && contentType) {
            types = contentType;
        } else {
            types = "application/json";
        }
        const token = uni.getStorageSync("token");
        var header;
        if (token !== null && token !== "" && token !== undefined) {
            header = {
                "X-Requested-With": "XMLHttpRequest", // 触发ajax请求
                "Content-Type": types,
                Accept: "application/json",
                Authorization: "bearer" + token,
            };
            header = Object.assign(header, { "Smt-Token": token });
        } else {
            header = {
                "X-Requested-With": "XMLHttpRequest", // 触发ajax请求
                "Content-Type": types,
                Accept: "application/json",
            };
        }

        var appid = getAppid();
        if (appid !== "" && appid !== null && appid !== undefined) {
            header = Object.assign(header, { appid: appid });
        }

        var open_mode = uni.getStorageSync("open_mode");
        if (open_mode !== null && open_mode !== "" && open_mode !== undefined) {
            header = Object.assign(header, {
                "Open-Mode": open_mode,
            });
        }

        // #ifdef H5
        const url_params = getUrlCode();
        if (
            url_params.pid !== null &&
            url_params.pid !== "" &&
            url_params.pid !== undefined
        ) {
            header = Object.assign(header, { pid: url_params.pid });
        }
        // #endif

        uni.request({
            url: url(path), //仅为示例，并非真实接口地址。
            data: query,
            method: method,
            header: header,
            success: (res) => {
                if (res.statusCode === 200) {
                    resolve(res.data);
                } else if (res.statusCode === 401 || res.statusCode === 701) {
                    uni.removeStorageSync("token");
                    uni.showModal({
                        content: "尚未登入！",
                        confirmText: "立即登入",
                        success: (res) => {
                            if (res.confirm) {
                                // #ifdef H5
                                let pid = "";
                                const url_params = getUrlCode();
                                if (
                                    url_params.pid !== null &&
                                    url_params.pid !== "" &&
                                    url_params.pid !== undefined
                                ) {
                                    pid = url_params.pid;
                                }
                                nav_login(window.location.href, pid);
                                // #endif
                                // #ifndef H5
                                uni.navigateTo({
                                    url: "/ucenter/pages/person/index",
                                });
                                // #endif
                            }
                        },
                    });
                } else if (res.statusCode === 1001) {
                    uni.removeStorageSync("token");
                    uni.removeStorageSync("userInfo");
                    resolve({
                        errorCode: 1001,
                        errorMsg: "登入失败",
                    });
                } else {
                    reject(res);
                }
            },
            fail: (err: any) => {
                if (err.statusCode === 701) {
                    uni.removeStorageSync("token");
                    uni.showModal({
                        content: "尚未登入！",
                        confirmText: "立即登入",
                        success: (res) => {
                            if (res.confirm) {
                                uni.navigateTo({
                                    url: "/ucenter/pages/person/index",
                                });
                            }
                        },
                    });
                } else {
                    reject(err);
                }
            },
        });
    });
}

import { appinfo } from '@/app.conf'
import { getUrlCode } from "@/common/js/util"

export const url = (path: string) => {
	if (isUrl(path)) {
		return path;
	}
	//#ifdef MP-WEIXIN
	if (import.meta.env.MODE === 'development') {
		// if (path.substr(0, 3) === 'api' || path.substr(0, 4) === '/api' || path.substr(0, 3) === 'hdb' || path.substr(0, 4) === '/hdb' || path.substr(0, 3) === 'smt' || path.substr(0, 4) === '/smt' || path.substr(0, 7) === 'cms-api' || path.substr(0, 8) === '/cms-api' || path.substr(0, 7) === 'erp-api' || path.substr(0, 8) === '/erp-api') {
		// 	return appinfo.siteroot + ((appinfo.port !== null && appinfo.port !== '' && appinfo.port !== undefined) ? (':' + appinfo.port) : '') + '/' + (path.charAt(0) === '/' ? path.slice(1) : path)
		// }
		// return appinfo.siteroot + ((appinfo.port !== null && appinfo.port !== '' && appinfo.port !== undefined) ? (':' + appinfo.port) : '') + '/dev-api/' + (path.charAt(0) === '/' ? path.slice(1) : path)
		return 'http://localhost/' + (path.charAt(0) === '/' ? path.slice(1) : path)
	} else if (import.meta.env.MODE === 'production') {
		// return appinfo.siteroot + '/' + (path.charAt(0) === '/' ? path.slice(1) : path)
		return appinfo.siteroot + ((appinfo.port !== null && appinfo.port !== '' && appinfo.port !== undefined) ? (':' + appinfo.port) : '') + '/' + (path.charAt(0) === '/' ? path.slice(1) : path)
	}
	//#endif

	//#ifdef MP-TOUTIAO
	if (import.meta.env.MODE === 'development') {
		if (path.substr(0, 3) === 'api' || path.substr(0, 4) === '/api' || path.substr(0, 3) === 'hdb' || path.substr(0, 4) === '/hdb' || path.substr(0, 3) === 'smt' || path.substr(0, 4) === '/smt' || path.substr(0, 7) === 'cms-api' || path.substr(0, 8) === '/cms-api' || path.substr(0, 7) === 'erp-api' || path.substr(0, 8) === '/erp-api') {
			return appinfo.siteroot + ((appinfo.port !== null && appinfo.port !== '' && appinfo.port !== undefined) ? (':' + appinfo.port) : '') + '/' + (path.charAt(0) === '/' ? path.slice(1) : path)
		}
		return appinfo.siteroot + ((appinfo.port !== null && appinfo.port !== '' && appinfo.port !== undefined) ? (':' + appinfo.port) : '') + '/dev-api/' + (path.charAt(0) === '/' ? path.slice(1) : path)
	} else if (import.meta.env.MODE === 'production') {
		return appinfo.siteroot + ((appinfo.port !== null && appinfo.port !== '' && appinfo.port !== undefined) ? (':' + appinfo.port) : '') + '/' + (path.charAt(0) === '/' ? path.slice(1) : path)
	}
	//#endif

	//#ifdef MP-ALIPAY
	if (import.meta.env.MODE === 'development') {
		// if (path.substr(0, 3) === 'api' || path.substr(0, 4) === '/api' || path.substr(0, 3) === 'hdb' || path.substr(0, 4) === '/hdb' || path.substr(0, 3) === 'smt' || path.substr(0, 4) === '/smt' || path.substr(0, 7) === 'cms-api' || path.substr(0, 8) === '/cms-api' || path.substr(0, 7) === 'erp-api' || path.substr(0, 8) === '/erp-api') {
		// 	return appinfo.siteroot + ((appinfo.port !== null && appinfo.port !== '' && appinfo.port !== undefined) ? (':' + appinfo.port) : '') + '/' + (path.charAt(0) === '/' ? path.slice(1) : path)
		// }
		return appinfo.siteroot + ((appinfo.port !== null && appinfo.port !== '' && appinfo.port !== undefined) ? (':' + appinfo.port) : '') + '/' + (path.charAt(0) === '/' ? path.slice(1) : path)
	} else if (import.meta.env.MODE === 'production') {
		let aliExtConfig = my.getExtConfigSync ? my.getExtConfigSync() : {}
		var url = 'https://' + aliExtConfig.host;
		if (aliExtConfig.port !== null && aliExtConfig.port !== '' && aliExtConfig.port !== undefined) {
			url += ':' + aliExtConfig.port
		}
		url += '/' + (path.charAt(0) === '/' ? path.slice(1) : path)

		return url
	}
	//#endif

	//#ifdef H5
	let href = window.location.href
	return (new URL(href)).origin + '/' + (path.charAt(0) === '/' ? path.slice(1) : path)
	//#endif
}

export const asset = (url: string) => {
	if (url !== undefined && url !== null) {
		if (isUrl(url)) {
			return url;
		} else {
			//#ifdef MP-WEIXIN
			if (import.meta.env.MODE === 'development') {
				return 'http://localhost/' + (url.charAt(0) === '/' ? url.slice(1) : url)
			} else {
				return appinfo.siteroot + '/' + (url.charAt(0) === '/' ? url.slice(1) : url)
			}
			//#endif

			//#ifdef MP-TOUTIAO
			return appinfo.siteroot + ((appinfo.port !== null && appinfo.port !== '' && appinfo.port !== undefined) ? (':' + appinfo.port) : '') + '/' + (url.charAt(0) === '/' ? url.slice(1) : url)
			//#endif

			//#ifdef MP-ALIPAY
			let aliExtConfig = my.getExtConfigSync ? my.getExtConfigSync() : {}
			var new_url = 'https://' + aliExtConfig.host;
			if (aliExtConfig.port !== null && aliExtConfig.port !== '' && aliExtConfig.port !== undefined) {
				new_url += ':' + aliExtConfig.port
			}
			return new_url + '/' + (url.charAt(0) === '/' ? url.slice(1) : url)
			//#endif

			//#ifdef H5
			let href = window.location.href
			return (new URL(href)).origin + '/' + url
			//#endif
		}
	} else {
		return ''
	}
}

export const asset_static = (path: string) => {
    if (isUrl(path)) {
        return path;
    }
    let href = window.location.href;
    return new URL(href).origin + "/html/" + appinfo.identity + "/mobile/" + (path.charAt(0) === "/" ? path.slice(1) : path);
};

export const isUrl = (url: string) => {
	const strRegex = '^((https|http|ftp)://)?'//(https或http或ftp):// 可有可无
		+ '(([\\w_!~*\'()\\.&=+$%-]+: )?[\\w_!~*\'()\\.&=+$%-]+@)?' //ftp的user@ 可有可无
		+ '(([0-9]{1,3}\\.){3}[0-9]{1,3}' // IP形式的URL- 3位数字.3位数字.3位数字.3位数字
		+ '|' // 允许IP和DOMAIN（域名）
		+ '(localhost)|'    //匹配localhost
		+ '([\\w_!~*\'()-]+\\.)*' // 域名- 至少一个[英文或数字_!~*\'()-]加上.
		+ '\\w+\\.' // 一级域名 -英文或数字 加上.
		+ '[a-zA-Z]{1,6})' // 顶级域名- 1-6位英文
		+ '(:[0-9]{1,5})?' // 端口- :80 ,1-5位数字
		+ '((/?)|' // url无参数结尾 - 斜杆或这没有
		+ '(/[\\w_!~*\'()\\.;?:@&=+$,%#-]+)+/?)$';//请求参数结尾- 英文或数字和[]内的各种字符
	const re = new RegExp(strRegex, 'i'); // 大小写不敏感
	if (re.test(encodeURI(url))) {
		return true;
	}
	return false;

	// var strRegex = '^http[s]?:\\/\\/([\\w-]+\\.)+[\\w-]+([\\w-./?%&=]*)?$';
	// var re = new RegExp(strRegex);
	// return re.test(url);
}

export const replaceImg = (rich_text: string) => {
	var b = /<img [^>]*src=['"]([^'"]+)[^>]*>/g;// img 标签取src里面内容的正则
	var s = rich_text.match(b);// 取到所有img标签 放到数组 s里面
	if (s !== null && s !== undefined && s.length > 0) {
		for (var i = 0; i < s.length; i++) {
			var srcImg = s[i].replace(b, '$1');//取src面的内容
			if (srcImg.slice(0, 4) == 'http' || srcImg.slice(0, 5) == 'https') {
				//若src前4位置或者前5位是http、https则不做任何修改
				// console.log('不做任何修改');
			} else {
				//修改富文本字符串内容 img标签src 相对路径改为绝对路径
				rich_text = rich_text.replace(new RegExp(srcImg, 'g'), appinfo.siteroot + '/' + (srcImg.charAt(0) === '/' ? srcImg.slice(1) : srcImg)); //'http://www.dlzjzx.tj.cn'自己替换的内容
			}
		}
		rich_text = rich_text.replace(/\<img/gi, '<img style="max-width:100%;height:auto" ')
	}
	return rich_text
}

export const getQueryString = (name: string) => {
	const reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i')
	const search = window.location.search.split('?')[1] || ''
	const r = search.match(reg) || []
	return r[2]
}

/**
 * 删除URL中指定search参数,会将参数值一起删除
 * @param {string} url 地址字符串
 * @param {array} aParam 要删除的参数key数组，如['name','age']
 * @return {string} 返回新URL字符串
 */
function ridUrlParam(url: string, params: any) {
	for (var index = 0; index < params.length; index++) {
		var item = params[index];
		var fromIndex = url.indexOf(item + "="); //必须加=号，避免参数值中包含item字符串
		if (fromIndex !== -1) {
			// 通过url特殊符号，计算出=号后面的的字符数，用于生成replace正则
			var startIndex = url.indexOf("=", fromIndex);
			var endIndex = url.indexOf("&", fromIndex);
			var hashIndex = url.indexOf("#", fromIndex);

			var reg;
			if (endIndex !== -1) {
				// 后面还有search参数的情况
				var num = endIndex - startIndex;
				reg = new RegExp(item + "=.{" + num + "}");
				url = url.replace(reg, "");
			} else if (hashIndex !== -1) {
				// 有hash参数的情况
				var num = hashIndex - startIndex - 1;
				reg = new RegExp("&?" + item + "=.{" + num + "}");
				url = url.replace(reg, "");
			} else {
				// search参数在最后或只有一个参数的情况
				reg = new RegExp("&?" + item + "=.+");
				url = url.replace(reg, "");
			}
		}
	}
	var noSearchParam = url.indexOf("=");
	if (noSearchParam === -1) {
		url = url.replace(/\?/, ""); // 如果已经没有参数，删除？号
	}
	return url;
}

export const build_h5_base_url = () => {
	var loca = window.location;
	const url_params = getUrlCode()
	var base_url = loca.origin + loca.pathname
	if (url_params.appid !== null && url_params.appid !== '' && url_params.appid !== undefined) {
		base_url += '?appid=' + url_params.appid
	}
	return base_url
}

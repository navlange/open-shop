export function validate(val: any, type: any, rule: any) {
    let result = {
        status: true,
        msg: ''
    }
    switch (type) {
        case 'String':
            switch (rule) {
                case 'required':
                    if (val === '' || val === 'undefined' || val === null) {
                        result.status = false
                    }
                    break
                default:
                    break
            }
            break
        case 'Array':
            switch (rule) {
                case 'required':
                    if (val === null || val.length <= 0) {
                        result.status = false
                    }
                    break
                default:
                    break
            }
            break
        case 'Mobile':
            if (!(/^1[3456789]\d{9}$/.test(val))) {
                result.status = false
                result.msg = '请输入正确格式手机号！'
            }
            break
        case 'Money':
            let priceReg = /(^[1-9]\d*(\.\d{1,2})?$)|(^0(\.\d{1,2})?$)/
            if (!priceReg.test(val)) {
                result.status = false
                result.msg = '请输入正确格式金额！'
            }
            break
        case 'IdNumber':
            if (val === '' || val === 'undefined' || val === null) {
                result.status = false
                result.msg = '请输入正确格式身份证号！'
            } else {
                //身份证正则表达式(18位)
                var isIdCard2 = /^[1-9]\d{5}(19\d{2}|[2-9]\d{3})((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])(\d{4}|\d{3}X)$/i;
                var stard = "10X98765432"; //最后一位身份证的号码
                var first = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2]; //1-17系数
                var sum = 0;
                if (!isIdCard2.test(val)) {
                    result.status = false
                    result.msg = '请输入正确格式身份证号！'
                }
                var year = val.substr(6, 4);
                var month = val.substr(10, 2);
                var day = val.substr(12, 2);
                var birthday = val.substr(6, 8);

                var id_number_birthday = ''
                var date = new Date(year + '/' + month + '/' + day)
                if (date instanceof Date) {
                    var year: any = date.getFullYear();
                    var month: any = date.getMonth() + 1;
                    month = month < 10 ? '0' + month : month;
                    var day: any = date.getDate();
                    day = day < 10 ? '0' + day : day;
                    id_number_birthday = year + '' + month + '' + day;
                } else {
                    result.status = false
                    result.msg = '请输入正确格式身份证号！'
                }

                if (birthday != id_number_birthday) { //校验日期是否合法
                    result.status = false
                    result.msg = '请输入正确格式身份证号！'
                }
                for (var i = 0; i < val.length - 1; i++) {
                    sum += val[i] * first[i];
                }
                var cur_result = sum % 11;
                var last = stard[cur_result]; //计算出来的最后一位身份证号码
                if (val !== '' && val !== 'undefined' && val !== null && val[val.length - 1].toUpperCase() != last) {
                    result.status = false
                    result.msg = '请输入正确格式身份证号！'
                }
            }
        default:
            break
    }
    return result
}

export function getUrlCode() {
	// 截取url中的code方法
	var url = location.href; //获取打开的公众号的路径
	let winUrl = url;
	var theRequest: any = new Object();
	var parts = url.split('#') // vue兼容微擎
	parts.forEach(element => {
		if (element.indexOf('?') != -1) {
			var str = element.substr(element.indexOf('?') + 1);
			var strs = str.split('&');
			for (var i = 0; i < strs.length; i++) {
				var items = strs[i].split('=');
				theRequest[strs[i].split('=')[0]] = strs[i].split('=')[1];
			}
		}
	});
	return theRequest;
}

export function object_key_exist(obj: any, key: string) {
	if (obj instanceof Object) {
		const keys = Object.keys(obj)
		for (var i = 0; i < keys.length; i++) {
			if (keys[i] == key) {
				return true
			}
		}
	}
	return false
}

export function isJSON(str: string) {
	try {
		let obj = JSON.parse(str);
		return !!obj && typeof obj === 'object';
	} catch (e) { }
	return false;
}

export function getAppid() {
	var appid = ''
	// #ifdef MP-WEIXIN
	const accountInfo = uni.getAccountInfoSync();
	appid = accountInfo.miniProgram.appId
	// #endif
	// #ifdef MP-ALIPAY
	// var res = my.getExtConfigSync();
	const appIdRes = my.getAppIdSync();
	appid = appIdRes.appId
	// #endif
	// #ifdef MP-TOUTIAO
	// const extConfig = uni.getExtConfigSync ? uni.getExtConfigSync() : {}
	appid = 'tte22d94907d1ecb3201'
	// #endif

	// #ifdef H5
	var open_mode = uni.getStorageSync('open_mode')
	if(open_mode == '2-1') {
		appid = uni.getStorageSync('corpid')
	} else {
		const url_params: any = getUrlCode()
		appid = url_params.appid
	}
	// #endif
	return appid
}

import { request } from '@/common/js/request'

export function roleOfOrder(query: any) {
  return request('smt/order/operate/roleOfOrder', query, 'GET')
}

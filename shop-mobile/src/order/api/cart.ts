import { request } from '@/common/js/request'

export function ofMe(query: any) {
  return request('/shop-api/cart/ofMe', query, 'GET')
}

export function add(query: any) {
  return request('/shop-api/cart/add', query, 'POST')
}

export function update(query: any) {
  return request('/shop-api/cart/update', query, 'POST')
}

export function clear(query: any) {
  return request('/shop-api/cart/clear', query, 'POST')
}

export function deleteCart(query: any) {
  return request('/shop-api/cart/delete', query, 'POST')
}
import { request } from '@/common/js/request'

export function getConf(query: any) {
	return request('shop-api/order/conf/get', query, 'GET')
}

export function queryList(query: any) {
	return request('shop-api/order/list', query, 'GET')
}

export function ofMe(query: any) {
	return request('shop-api/order/ofMe', query, 'GET')
}

export function fetch(query: any) {
	return request('shop-api/order/get', query, 'GET')
}

export function latestOfMe(query: any) {
	return request('shop-api/order/latestOfMe', query, 'GET')
}

export function releaseCheck(query: any) {
	return request('shop-api/order/release/check', query, 'POST')
}

export function release(query: any) {
	return request('shop-api/order/release', query, 'POST')
}

export function releaseGoods(query: any) {
	if(!(query != null && query != '' && query != undefined && query.order_from !== null && query.order_from !== '' && query.order_from !== undefined)) {
		// #ifdef MP-WEIXIN
		var params = Object.assign({order_from: '1'}, query)
		// #endif
		// #ifdef MP-ALIPAY
		var params = Object.assign({order_from: '2'}, query)
		// #endif
		// #ifdef MP-ALIPAY
		var params = Object.assign({order_from: '0'}, query)
		// #endif
	} else {
		var params = query
	}
	return request('shop-api/order/release/goods/v2', params, 'POST')
}

export function update(query: any) {
	return request('shop-api/order/update', query, 'POST')
}

export function cancel(query: any) {
	return request('shop-api/order/cancel', query, 'POST')
}

export function receive(query: any) {
	return request('shop-api/order/delivery/receive', query, 'POST')
}

export function verify(query: any) {
	return request('shop-api/order/verify/submit', query, 'POST')
}

export function close(query: any) {
	return request('shop-api/order/close', query, 'POST')
}

export function trans_status(status: string, sub_status = '0') {
	if (status == '0') {
		if (sub_status === '6') {
			return '内部预订'
		}
		return '待支付';
	} else if (status == '1') {
		if (sub_status === '1') {
			return '已发货'
		} else if (sub_status === '3') {
			return '已收货'
		} else {
			return '已付款';
		}
	} else if (status == '2') {
		return '已取消';
	} else if (status == '3') {
		return '进行中';
	} else if (status == '4') {
		return '已完成';
	} else if (status == '5') {
		if (sub_status == '0') {
			return '退款/审核中';
		} else if (sub_status == '1') {
			return '退款/已完成'
		}
	} else if (status == '7') {
		return '待结算';
	} else if (status == '8') {
		return '已结算';
	} else if (status == '9') {
		return '已完成';
	} else if (status == '10') {
		return '已关闭';
	}
}
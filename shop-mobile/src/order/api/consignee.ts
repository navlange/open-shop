import { request } from '@/common/js/request'

export function fetch(query: any) {
  return request('/smt/order/consignee/get', query, 'GET')
}

export function queryDefault(query: any) {
  return request('/smt/order/consignee/getDefault', query, 'GET')
}

export function ofMe(query: any) {
  return request('/smt/order/consignee/ofMe', query, 'GET')
}

export function add(query: any) {
  return request('/smt/order/consignee/add', query, 'POST')
}

export function update(query: any) {
  return request('/smt/order/consignee/update', query, 'POST')
}

export function deleteConsignee(query: any) {
  return request('/smt/order/consignee/delete', query, 'POST')
}
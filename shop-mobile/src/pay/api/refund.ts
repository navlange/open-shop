import { request } from '@/common/js/request'

export function submit(query: any) {
  return request('smartore/refund/submit', query, 'POST')
}

export function member(query: any) {
  return request('smartore/refund/member', query, 'POST')
}

import { request } from '@/common/js/request'

export function success(query: any) {
    return request('/shop-api/pay/order/success', query, 'POST')
}

export function fail(query: any) {
    return request('/shop-api/pay/order/fail', query, 'POST')
}
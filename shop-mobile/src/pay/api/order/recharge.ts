import { request } from '@/common/js/request'

export function success(query: any) {
    return request('/smartore/pay/recharge/success', query, 'POST')
}

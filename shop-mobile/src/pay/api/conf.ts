import { request } from '@/common/js/request'

export function fetch(query: any) {
	return request('smt/pay/conf/get', query, 'GET')
}
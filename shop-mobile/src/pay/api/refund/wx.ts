import { request } from '@/common/js/request'

export function refund(query: any) {
	return request('/api/pay/wx/refund', query, 'POST')
}
import { request } from "@/common/js/request";

export function priceOfCartGoods(query: any) {
    return request("/shop-api/price/ofCartGoods", query, "GET");
}

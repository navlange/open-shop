import { request } from '@/common/js/request'

export function pay(query: any) {
	return request('/smt/pay/wx/pay', query, 'POST')
}
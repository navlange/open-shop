import { request } from '@/common/js/request'

export function price(query: any) {
	return request('/shop-api/pay/price', query, 'GET')
}

export function pay(query: any) {
	return request('/shop-api/pay/submit', query, 'POST')
}

export function trans_pay_mode(mode: any) {
	if(mode == '0') {
		return '余额'
	} else if (mode == '1') {
		return '微信'
	} else if (mode == '2') {
		return '次卡'
	} else if (mode == '3') {
		return '积分兑换'
	} else if (mode == '4') {
		return '储值卡'
	} else if (mode == '6') {
		return '营销支付'
	} else if (mode == '8') {
		return '支付宝'
	} else if (mode == '9') {
		return '线下'
	} else if (mode == '10') {
		return '无需支付'
	}
}
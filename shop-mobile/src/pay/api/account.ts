import { request } from '@/common/js/request'

export function ofMe(query: any) {
	return request('/smt/pay/account/ofMe', query, 'GET')
}

export function getBalance(query: any) {
	return request('/smt/pay/account/getBalance', query, 'GET')
}

export function getDeposit(query: any) {
	return request('/smt/pay/account/getDeposit', query, 'GET')
}

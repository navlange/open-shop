import { request } from '@/common/js/request'

export function info(query: any) {
  return request('/api/user/info/ofAlipay', query, 'GET')
}

import { request } from '@/common/js/request'

export function info(query: any) {
  return request('/api/user/info/ofMiniapp', query, 'GET')
}

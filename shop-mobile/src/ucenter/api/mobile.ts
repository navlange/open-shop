import { request } from '@/common/js/request'

export function refreshLocal(mobile: string) {
  let user_info = uni.getStorageSync('user_info');
  if (user_info === null || user_info === '' || user_info === undefined) {
    user_info = {
      mobile: mobile,
    };
  } else {
    user_info.mobile = mobile;
  }
  uni.setStorageSync('user_info', user_info);
}

export function getWxBindMobile(query: any) {
  return request('/api/mobile/getWxBind', query, 'POST')
}

export function getAliBindMobile(query: any) {
  return request('/api/mobile/getAliBind', query, 'POST')
}

export function updateOfMe(query: any) {
  return request('/api/mobile/updateOfMe', query, 'POST')
}

import { request } from '@/common/js/request'

export function register(query: any) {
	return request('/api/user/register', query, 'POST')
}

export function ofMe(query: any) {
	return request('/api/user/ofMe', query, 'GET')
}

export function updateOfMe(query: any) {
	return request('/api/user/updateOfMe', query, 'POST')
}

export function fetch(query: any) {
	return request('/api/user/get', query, 'GET')
}

export function updateName(query: any) {
	return request('/api/name/update', query, 'POST')
}

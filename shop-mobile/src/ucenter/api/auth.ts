import { request } from '@/common/js/request'
import { getUrlCode } from '@/common/js/util';

export const register = () => {
	return new Promise((resolve, reject) => {
		uni.getUserProfile({
			lang: 'zh_CN',
			desc: '登录',
			success: (res: any) => {
				uni.login({
					provider: "weixin",
					success: (loginRes) => {
						request('ucenter/register', {
							code: loginRes.code,
							nickname: res.userInfo.nickName,
							gender: res.userInfo.gender,
							city: res.userInfo.city,
							prov: res.userInfo.province,
							country: res.userInfo.country,
							avatar: res.userInfo.avatarUrl
						}, 'POST').then(res => {
							//成功回调
							resolve(res)
						}).catch(err => {
							//请求失败
							console.log(err)
							reject(err)
						})
					},
					fail(err) {
						uni.showModal({
							showCancel: false,
							content: "登入失败"
						})
						reject(err)
					}
				})
			},
			fail: (res) => {
				uni.showModal({
					showCancel: false,
					content: "登入失败"
				})
				reject('err')
			}
		});
	})
}

export const checkLogin = () => {
	const token = uni.getStorageSync('token');
	if (token !== null && token !== '' && token !== undefined) {
		return true;
	} else {
		return false;
	}
}

export const mobileNotLogin = () => {
	uni.showModal({
		content: "尚未登入",
		confirmText: "立即登入",
		success: res => {
			if (res.confirm) {
				uni.navigateTo({
					url: '/ucenter/pages/person/index'
				})
			}
		}
	})
}

export const login = (query: any) => {
	return request('/ucenter/auth/login', query, 'POST')
}

export const logout = () => {
	let token = uni.getStorageSync('token')
	uni.removeStorageSync('token')
	uni.removeStorageSync('user_info')
	return request('api/auth/logout', {
		token: token
	}, 'POST')
}

export const build_login_callback = (page: string) => {
	var url = page
	if (import.meta.env.MODE === 'development') {
		url += '?is_dev=1'
	}
	var callback_page = encodeURIComponent(url); // 当前H5页面的url
	var loca = window.location;
	const url_params = getUrlCode()
	var base_url = loca.origin + loca.pathname
	if(url_params.appid !== null && url_params.appid !== '' && url_params.appid !== undefined) {
		base_url += '?appid=' + url_params.appid
	}
	return base_url + '#/ucenter/pages/login/index?callback=' + callback_page;
}

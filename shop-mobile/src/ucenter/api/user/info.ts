import { request } from '@/common/js/request'

export function ofMe(query: any) {
  return request('/api/user/info/ofMe', query, 'GET')
}

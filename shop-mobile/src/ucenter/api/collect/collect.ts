import { request } from '@/common/js/request'

export function get(query: any) {
    return request('/api/collect/get', query, 'GET')
}

export function add(query: any) {
    return request('/api/collect/add', query, 'POST')
}

export function del(query: any) {
    return request('/api/collect/del', query, 'POST')
}

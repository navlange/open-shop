import { appinfo } from "@/app.conf";
import { request } from "@/common/js/request";
import { getMpAppid } from "@/platform/api/project/project";

export function auth_code(mode: any, callback_page: any, pid: any) {
    getMpAppid(null).then((res: any) => {
        if (import.meta.env.MODE === "development") {
            var url =
                "http://localhost/html/" + appinfo.identity + "/mobile/#/rbac/pages/login/code?callback_page=" +
                callback_page;
            url += "&is_dev=1";
        } else {
            var url =
                new URL(window.location.href).origin +
                "/html/" + appinfo.identity + "/mobile/" + (pid ? ("?pid=" + pid) : "") + "#/rbac/pages/login/code?callback_page=" +
                callback_page;
        }
        var local = encodeURIComponent(url); // 当前H5页面的url

        var wxUrl =
            "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" +
            res.data.appid +
            "&redirect_uri=" +
            local +
            "&response_type=code" +
            "&scope=snsapi_" +
            (mode === "userinfo" ? "userinfo" : "base") +
            "&state=STATE#wechat_redirect";
        window.location.href = wxUrl;
    });
}

export function userFromCode(code: any) {
    return new Promise((resolve, reject) => {
        request(
            "/api/auth/mp/code",
            {
                mode: "base",
                code: code,
            },
            "POST"
        )
            .then((res: any) => {
                if (res.errorCode == 200) {
                    if (res.data.hasOwnProperty("userInfo")) {
                        let userInfo = uni.getStorageSync("userInfo");
                        if (
                            userInfo !== null &&
                            userInfo !== "" &&
                            userInfo !== undefined
                        ) {
                            userInfo = Object.assign(userInfo, res.userInfo);
                        } else {
                            userInfo = res.userInfo;
                        }
                        uni.setStorageSync("userInfo", userInfo);
                    }
                    if (res.data.hasOwnProperty("token")) {
                        uni.setStorageSync("token", res.data.token);
                        resolve(res.token);
                    } else {
                        reject("non login!");
                    }
                } else {
                    uni.showToast({
                        title: res.errorMsg,
                        icon: "none",
                    });
                    reject(res.errorMsg);
                }
            })
            .catch((e) => {
                reject("code session error");
            });
    });
}

export function ofUser(query: any) {
    return request("/api/fans/ofUser", query, "GET");
}

export function bindUser(query: any) {
    return request("/api/fans/bindUser", query, "POST");
}

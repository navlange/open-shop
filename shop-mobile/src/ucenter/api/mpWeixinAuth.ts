import { request } from "@/common/js/request";
import { info as userInfo } from "@/ucenter/api/miniapp/user";

export function login() {
    return new Promise((resolve, reject) => {
        uni.login({
            provider: "weixin",
            success: (loginRes) => {
                let pre_uid = uni.getStorageSync("pre_uid");
                let data = {
                    code: loginRes.code,
                };
                if (
                    pre_uid !== null &&
                    pre_uid !== "" &&
                    pre_uid !== undefined &&
                    parseInt(pre_uid) > 0
                ) {
                    data = Object.assign(data, { pre_uid: pre_uid });
                }
                request("api/auth/miniapp/login", data, "POST")
                    .then((res: any) => {
                        //成功回调
                        if (res.data.hasOwnProperty("token")) {
                            uni.setStorageSync("token", res.data.token);
                            userInfo(null).then((res: any) => {
                                let user_info = uni.getStorageSync("user_info");
                                if (
                                    user_info !== null &&
                                    user_info !== "" &&
                                    user_info !== undefined
                                ) {
                                    user_info = Object.assign(
                                        user_info,
                                        res.data
                                    );
                                } else {
                                    user_info = res.data;
                                }
                                uni.setStorageSync("user_info", user_info);
                                resolve(res.data.token);
                            });
                        } else {
                            reject("non login!");
                        }
                    })
                    .catch((err) => {
                        //请求失败
                        reject(err);
                    });
            },
        });
    });
}

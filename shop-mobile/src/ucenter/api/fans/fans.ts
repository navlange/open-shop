import { request } from '@/common/js/request'

export function rebindUser(query: any) {
  return request('/api/fans/rebindUser', query, 'POST')
}

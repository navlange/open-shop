import { request } from '@/common/js/request'

export function submit(query: any) {
    return request('/cms-api/comment/submit', query, 'POST')
}

export function ofOrder(query: any) {
    return request('/cms-api/comment/ofOrder', query, 'GET')
}

export function ofGoods(query: any) {
    return request('/cms-api/comment/ofGoods', query, 'GET')
}

export function ofStore(query: any) {
    return request('/cms-api/comment/ofStore', query, 'GET')
}

import { request } from '@/common/js/request'

export function fetch(query: any) {
  return request('/shop-api/goods/get', query, 'GET')
}

export function fetchStock(query: any) {
  return request('/shop-api/stock/ofGoods', query, 'GET')
}

export function queryList(query: any) {
  return request('/shop-api/goods/list', query, 'GET')
}

export function ofMode(query: any) {
  return request('/shop-api/goods/ofMode', query, 'GET')
}

export function ofStore(query: any) {
  return request('/shop-api/goods/list', query, 'GET')
}

export function queryIds(query: any) {
  return request('/shop-api/goods/ofIds', query, 'GET')
}

export function filterQuery(query: any) {
  return request('/shop-api/goods/filter', query, 'GET')
}



export function ofMe(query: any) {
  return request('/shop-api/goods/ofMe', query, 'GET')
}

export function createOfMe(query: any) {
  return request('/shop-api/goods/createOfMe', query, 'POST')
}

export function update(query: any) {
  return request('/shop-api/goods/update', query, 'POST')
}

export function setOn(query: any) {
  return request('/shop-api/goods/setOn', query, 'POST')
}

export function setOff(query: any) {
  return request('/shop-api/goods/setOff', query, 'POST')
}

export function createOfStore(query: any) {
  return request('/shop-api/goods/createOfStore', query, 'POST')
}

import { request } from '@/common/js/request'

export function ofGoods(query: any) {
	return request('smartore/things/norm/ofGoods', query, 'GET')
}

export function create(query: any) {
	return request('smartore/things/norm/create', query, 'POST')
}

export function update(query: any) {
	return request('smartore/things/norm/update', query, 'POST')
}

export function deleteNorm(query: any) {
	return request('smartore/things/norm/delete', query, 'POST')
}

export function createNormValue(query: any) {
	return request('smartore/things/norm/value/create', query, 'POST')
}

export function deleteValue(query: any) {
	return request('smartore/things/norm/value/delete', query, 'POST')
}

export function updateValue(query: any) {
	return request('smartore/things/norm/value/update', query, 'POST')
}

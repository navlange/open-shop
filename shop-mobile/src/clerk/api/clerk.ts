import { request } from '@/common/js/request'

export function fetch(query: any) {
  return request('/api/clerk/get', query, 'GET')
}

export function queryList(query: any) {
  return request('/api/clerk/list', query, 'GET')
}

export function ofMe(query: any) {
  return request('/api/clerk/ofMe', query, 'GET')
}

export function qrcode(query: any) {
  return request('/api/clerk/qrcode', query, 'GET')
}

import { createSSRApp } from "vue";
import App from "./App.vue";
import { get as getInitConf } from "@/platform/api/conf/init";
import { get as getWidgetConf } from "@/widget/api/conf";
import { get as getPersonThemeCommon } from "@/widget/api/person/common";
import { queryTheme } from '@/widget/api/theme/common';

export function createApp() {
    const app = createSSRApp(App);

    app.provide(
        "onLaunched",
        new Promise((resolve) => {
            let to_init: any[] = [];
            to_init.push(
                new Promise((resolve, reject) =>
                    getInitConf(null)
                        .then((res: any) => {
                            uni.removeStorageSync("platform_conf");
                            uni.setStorageSync(
                                "platform_conf",
                                res.data.platform_conf
                            );
                            resolve(res);
                        })
                        .catch((e: any) => {
                            reject(e);
                        })
                )
            );
            to_init.push(
                new Promise((resolve, reject) => {
                    getWidgetConf(null)
                        .then((res: any) => {
                            uni.removeStorageSync("widget_conf");
                            uni.setStorageSync("widget_conf", res.data);
                            resolve(res);
                        })
                        .catch((e: any) => {
                            reject(e);
                        });
                })
            );
            to_init.push(
                new Promise((resolve, reject) => {
                    getPersonThemeCommon(null)
                        .then((res: any) => {
                            uni.removeStorageSync("theme_person_head");
                            uni.setStorageSync(
                                "theme_person_head",
                                res.data.theme_head
                            );
                            uni.removeStorageSync("theme_person_nav");
                            uni.setStorageSync(
                                "theme_person_nav",
                                res.data.theme_nav
                            );
                            resolve(res);
                        })
                        .catch((e: any) => {
                            reject(e);
                        });
                })
            );
            to_init.push(
                new Promise((resolve, reject) => {
                    queryTheme(null)
                        .then((res: any) => {
                            uni.removeStorageSync("theme");
                            uni.setStorageSync("theme", res.data.theme);
                            resolve(res);
                        })
                        .catch((e: any) => {
                            reject(e);
                        });
                })
            );
            uni.showLoading();
            Promise.all(to_init).then((res) => {
                // console.log("onLaunched", res);
                uni.hideLoading();
                resolve("onLaunched");
            });
        })
    );

    return {
        app,
    };
}

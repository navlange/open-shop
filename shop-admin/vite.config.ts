import { fileURLToPath, URL } from 'node:url'

import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import path from 'path'
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'
import { viteMockServe } from 'vite-plugin-mock'

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
    const root = process.cwd()
    const env = loadEnv(mode, root)
    return {
        plugins: [
            vue(),
            vueJsx(),
            createSvgIconsPlugin({
                iconDirs: [
                    path.resolve(process.cwd(), 'src/icons/svg'),
                    path.resolve(process.cwd(), '../icons/svg')
                ], // 在src下新建文件夹icons
                symbolId: 'icon-[dir]-[name]'
            }),
            // mock服务
            viteMockServe({
                supportTs: false,
                logger: false,
                mockPath: './src/mock/'
            })
        ],
        resolve: {
            alias: {
                '@': fileURLToPath(new URL('./src', import.meta.url)),
                '@module': fileURLToPath(
                    new URL(
                        env.VITE_APP_MODULE_PATH ? env.VITE_APP_MODULE_PATH : './src/modules',
                        import.meta.url
                    )
                )
            }
        },
        server: {
            hmr: {
                overlay: false
            },
            host: '127.0.0.1',
            port: 5173, // 端口
            proxy: {
                '/api': {
                    // 请求接口中要替换的标识
                    target: 'http://localhost', // 代理地址
                    changeOrigin: true, // 是否允许跨域
                    secure: true,
                    rewrite: (path) => path // api标志替换为''
                },
                '/shop-api': {
                    // 请求接口中要替换的标识
                    target: 'http://localhost', // 代理地址
                    changeOrigin: true, // 是否允许跨域
                    secure: true,
                    rewrite: (path) => path // api标志替换为''
                },
                '/cms-api': {
                    // 请求接口中要替换的标识
                    target: 'http://localhost', // 代理地址
                    changeOrigin: true, // 是否允许跨域
                    secure: true,
                    rewrite: (path) => path // api标志替换为''
                },
                '/smt': {
                    // 请求接口中要替换的标识
                    target: 'http://localhost', // 代理地址
                    changeOrigin: true, // 是否允许跨域
                    secure: true,
                    rewrite: (path) => path // api标志替换为''
                },
                '/smartore': {
                    // 请求接口中要替换的标识
                    target: 'http://localhost', // 代理地址
                    changeOrigin: true, // 是否允许跨域
                    secure: true,
                    rewrite: (path) => path // api标志替换为''
                },
                '/images': {
                    target: 'http://localhost',
                    changeOrigin: true,
                    rewrite: (path) => path
                }
            }
        },
        base: env.VITE_APP_BASE_PATH
    }
})

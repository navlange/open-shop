import { request } from '@/common/js/request'
import { openapi_url } from '@/common/js/url'

export function batches(query: any) {
	return request(openapi_url('/pay/wx/transfer/batches'), query, 'POST')
}

import { request } from '@/common/js/request'
import { smt_url } from '@/common/js/url'

export function fetch(query: any) {
	return request(smt_url('/pay/wx/conf/get'), query, 'GET')
}

export function set(query: any) {
	return request(smt_url('/pay/wx/conf/set'), query, 'POST')
}

export function setProfitsharing(query: any) {
	return request(smt_url('/pay/wx/conf/setProfitsharing'), query, 'POST')
}

export function gainPlatformCerts(query: any) {
	return request(smt_url('/pay/wx/conf/gainPlatformCerts'), query, 'GET')
}

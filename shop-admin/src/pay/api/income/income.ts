import { request } from '@/common/js/request'
import { smt_url } from '@/common/js/url'

export function fetchList(query: any) {
    return request(smt_url('/pay/income/list'), query, 'GET')
}

export function deleteIncome(query: any) {
    return request(smt_url('/pay/income/delete'), query, 'POST')
}

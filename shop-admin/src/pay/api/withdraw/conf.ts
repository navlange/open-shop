import { request } from "@/common/js/request";
import { smt_url } from "@/common/js/url";

export function fetch(query: any) {
    return request(smt_url("/pay/withdraw/conf/get"), query, "GET");
}

export function update(query: any) {
    return request(smt_url("/pay/withdraw/conf/update"), query, "POST");
}

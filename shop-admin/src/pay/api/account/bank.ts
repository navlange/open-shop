import { request } from '@/common/js/request'
import { smt_url } from '@/common/js/url'

export function ofUser(query: any) {
	return request(smt_url('/pay/account/bank/ofUser'), query, 'GET')
}

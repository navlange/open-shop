import { request } from '@/common/js/request'
import { smt_url } from '@/common/js/url'

export function get(query: any) {
	return request(smt_url('/pay/account/balance/get'), query, 'GET')
}

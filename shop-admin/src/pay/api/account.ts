import { request } from "@/common/js/request";
import { smt_url } from "@/common/js/url";

export function fetchList(query: any) {
    return request(smt_url("/pay/account/list"), query, "GET");
}

export function deleteAccount(query: any) {
    return request(smt_url("/pay/account/delete"), query, "POST");
}

export function operate(query: any) {
    return request(smt_url("/pay/account/operate"), query, "POST");
}

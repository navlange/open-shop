import { request } from '@/common/js/request'
import { smt_url } from '@/common/js/url'

export function bindAccount(query: any) {
	return request(smt_url('/pay/withdraw/bindAccount'), query, 'POST')
}

export function fetchList(query: any) {
	return request(smt_url('/pay/withdraw/list'), query, 'GET')
}

export function acceptWithdraw(query: any) {
	return request(smt_url('/pay/withdraw/accept'), query, 'POST')
}

export function acceptWithdrawWx(query: any) {
	return request(smt_url('/pay/withdraw/acceptWx'), query, 'POST')
}

export function wxTransferSuccess(query: any) {
	return request(smt_url('/pay/withdraw/wxTransferSuccess'), query, 'POST')
}

export function deleteWithdraw(query: any) {
	return request(smt_url('/pay/withdraw/delete'), query, 'POST')
}

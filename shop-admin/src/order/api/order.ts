import { request } from "@/common/js/request";
import { shop_url } from "@/common/js/url";

export function release(query: any) {
    return request(shop_url("/order/release"), query, "POST");
}

export function releaseSeatHelp(query: any) {
    return request(shop_url("/order/release/seat/help"), query, "POST")
}

export function trans_status(status: any, sub_status = "0") {
    if (status === "0") {
        return "待确认";
    } else if (status === "1") {
        if (sub_status === "1") {
            return "已发货";
        } else if (sub_status === "3") {
            return "已收货";
        } else {
            return "已支付";
        }
    } else if (status === "2") {
        return "已取消";
    } else if (status === "3") {
        return "进行中";
    } else if (status === "4") {
        return "已完成";
    } else if (status === "5") {
        if (sub_status === "0") {
            return "退款/审核中";
        } else if (sub_status === "1") {
            return "退款/已完成";
        }
    } else if (status === "6") {
        return "进行中";
    } else if (status === "7") {
        return "待结算";
    } else if (status === "8") {
        return "已结算";
    } else if (status === "9") {
        return "已完成";
    } else if (status === "10") {
        return "已关闭";
    }
}

export function fetch(query: any) {
    return request(shop_url("/order/get"), query, "GET");
}

export function fetchList(query: any) {
    return request(shop_url("/order/list"), query, "GET");
}

export function createOrder(data: any) {
    return request(shop_url("/order/create"), data, "POST");
}

export function updateOrder(data: any) {
    return request(shop_url("/order/update"), data, "POST");
}

export function refundAccept(query: any) {
    return request(shop_url("/order/refund/accept"), query, "POST");
}

export function deleteOrder(id: any) {
    return request(shop_url("/order/" + id), {}, "DELETE");
}

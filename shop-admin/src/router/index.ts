import { createRouter, createWebHashHistory } from 'vue-router'
// import HomeView from '../views/HomeView.vue'
import Layout from '../layout/index.vue'

let module_constant_routes: any[] = []
const modules: any = import.meta.glob('./modules/*.ts', { eager: true })
const modules_arr = []
for (const key in modules) {
    modules_arr.push(modules[key])
}
modules_arr.sort((a: any, b: any) => {
    return b['order_index'] - a['order_index']
})
modules_arr.forEach((item: any) => {
    module_constant_routes = module_constant_routes.concat(item['constantRoutes'])
})

let has_home_router = false
for(let i = 0; i < module_constant_routes.length; i++) {
    if(module_constant_routes[i].path === '/') {
      has_home_router = true
        break
    }
}
let has_login_router = false
for(let i = 0; i < module_constant_routes.length; i++) {
    if(module_constant_routes[i].path === '/login') {
      has_login_router = true
        break
    }
}

const constant_routers = []
if(!has_login_router) {
  constant_routers.push({
    path: '/login',
    component: () => import('../views/login/index.vue'),
    hidden: true
  })
}
if(!has_home_router) {
  constant_routers.push({
    path: '/',
    name: 'home',
    component: Layout,
    children: [
      {
        path: '',
        component: () => import('@/views/dashboard/index.vue'),
        name: 'Dashboard',
        meta: { title: 'dashboard', icon: 'dashboard', affix: true }
      }
    ]
  })
}
/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = constant_routers.concat(module_constant_routes)

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [
    // 404 page must be placed at the end !!!
    { path: '/:pathMatch(.*)', redirect: '/404', hidden: true }
]

const router = createRouter({
    history: createWebHashHistory(import.meta.env.BASE_URL),
    routes: constantRoutes.concat(module_constant_routes)
})

export function resetRouter() {
    // const newRouter = createRouter()
    // router.matcher = newRouter.matcher // reset router
}

export default router

/** When your routing table is too long, you can split it into small modules **/
import Layout from '@/layout/index.vue'

export const constantRoutes = [
    {
        path: '/platform',
        component: Layout,
        name: 'platform',
        meta: {
            title: '平台',
            icon: 'platform'
        },
        children: [
            {
                path: 'pay',
                component: () => import('@/pay/views/conf/index.vue'),
                name: 'payConf',
                meta: { title: '支付', icon: 'pay', affix: true }
            },
            {
                path: 'template',
                component: () => import('@/message/views/template/index.vue'),
                name: 'mpTemplate',
                meta: { title: '模板消息', icon: 'wechat', affix: true }
            },
            {
                path: 'template/create',
                component: () => import('@/message/views/template/create.vue'),
                name: 'createTemplate',
                meta: { title: '添加模板', icon: 'message', affix: true },
                hidden: true
            },
            {
                path: 'template/edit',
                component: () => import('@/message/views/template/edit.vue'),
                name: 'editmessage',
                meta: { title: '编辑模板', icon: 'message', affix: true },
                hidden: true
            }
        ]
    }
]

export const constantTailRoutes = []

export const order_index = 10
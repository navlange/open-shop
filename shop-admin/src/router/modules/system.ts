/** When your routing table is too long, you can split it into small modules **/
import Layout from '@/layout/index.vue'

export const constantRoutes = [
    {
        path: '/finance',
        component: Layout,
        name: 'finance',
        meta: {
            title: '财务',
            icon: 'pay'
        },
        children: [
            {
                path: 'income',
                component: () => import('@/pay/views/income/index.vue'),
                name: 'income',
                meta: { title: '收入', icon: 'income', affix: true }
            },
            {
                path: 'withdraw',
                component: () => import('@/pay/views/withdraw/index.vue'),
                name: 'withdraw',
                meta: { title: '提现', icon: 'withdraw', affix: true }
            }
        ]
    },
    {
        path: '/conf',
        component: Layout,
        name: 'conf',
        meta: {
            title: '系统',
            icon: 'conf'
        },
        children: [
            {
                path: 'index',
                component: () => import('@/system/views/conf/index.vue'),
                name: 'confIndex',
                meta: { title: 'confBase', icon: 'conf-base', affix: true }
            },
            // {
            //     path: 'permission',
            //     component: () => import('@/rabc/views/conf/index'),
            //     name: 'confPermission',
            //     meta: { title: 'confPermission', icon: 'permission', affix: true }
            // },
            // {
            //     path: 'business',
            //     component: () => import('@/smt/views/conf/index'),
            //     name: 'confBusiness',
            //     meta: { title: '业务设置', icon: 'order', affix: true }
            // },
            {
                path: 'dictionary',
                component: () => import('@/platform/views/dictionary/index.vue'),
                name: 'dictionary',
                meta: { title: '数据字典', icon: 'type', affix: true }
            },
            {
                path: 'theme',
                component: () => import('@/widget/views/page/index.vue'),
                name: 'pageIndex',
                meta: { title: '模式样式', icon: 'page-diy', affix: true }
            },
            // {
            //     path: 'myAccount',
            //     component: () => import('@/views/user/edit'),
            //     name: 'myAccount',
            //     meta: { title: '我的账户', icon: 'alipay', affix: true },
            //     hidden: true
            // },
            // {
            //     path: 'log',
            //     component: () => import('@/platform/views/log/index'),
            //     name: 'log',
            //     meta: { title: '日志记录', icon: 'log', affix: true }
            // },
            {
                path: 'system',
                component: () => import('@/system/views/system/index.vue'),
                name: 'system',
                meta: { title: '系统', icon: 'conf', affix: true }
            }
        ]
    }
]

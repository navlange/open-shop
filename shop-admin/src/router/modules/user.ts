/** When your routing table is too long, you can split it into small modules **/
import Layout from '@/layout/index.vue'

export const constantRoutes = [{
    path: '/ucenter',
    component: Layout,
    name: 'ucenter',
    meta: {
        title: '用户',
        icon: 'crm'
    },
    children: [
        {
            path: 'user/index',
            component: () => import('@/ucenter/views/user/index.vue'),
            name: 'user',
            meta: { title: 'user', icon: 'user', affix: true }
        },
        {
            path: 'user/add',
            component: () => import('@/ucenter/views/user/add.vue'),
            name: 'addUser',
            meta: { title: 'user', icon: 'user', affix: true },
            hidden: true
        },
        {
            path: 'user/edit',
            component: () => import('@/ucenter/views/user/edit.vue'),
            name: 'editUser',
            meta: { title: 'user', icon: 'user', affix: true },
            hidden: true
        },
        {
            path: 'fans/index',
            component: () => import('@/ucenter/views/fans/index.vue'),
            name: 'fans',
            meta: { title: 'fans', icon: 'fans', affix: true }
        },
        {
            path: 'clerk',
            component: () => import('@/clerk/views/clerk/index.vue'),
            name: 'clerk',
            meta: { title: 'clerk', icon: 'clerk', affix: true }
        },
        {
            path: 'clerk/add',
            component: () => import('@/clerk/views/clerk/add.vue'),
            name: 'addClerk',
            meta: { title: 'addClerk', icon: 'clerk', affix: true },
            hidden: true
        },
        {
            path: 'clerk/edit',
            component: () => import('@/clerk/views/clerk/edit.vue'),
            name: 'editClerk',
            meta: { title: 'editClerk', icon: 'clerk', affix: true },
            hidden: true
        }
    ]
}]

export const asyncRoutes = []

export const constantTailRoutes = []

export const order_index = 15
/** When your routing table is too long, you can split it into small modules **/
import Layout from '@/layout/index.vue'

export const constantRoutes = [
    {
        path: '/order',
        component: Layout,
        name: 'order',
        meta: {
            title: '订单',
            icon: 'component'
        },
        children: [
            {
                path: 'index',
                component: () => import('@/order/views/order/index.vue'),
                name: 'carIndex',
                meta: { title: '订单', icon: 'list', affix: true }
            }
        ]
    },
    {
        path: '/goods',
        component: Layout,
        name: 'goods',
        meta: {
            title: '商品',
            icon: 'things'
        },
        children: [
            {
                path: 'index',
                component: () => import('@/goods/views/goods/index.vue'),
                name: 'goodsCommon',
                meta: { title: '商品', icon: 'things', affix: true }
            },
            {
                path: 'create',
                component: () => import('@/goods/views/goods/create.vue'),
                name: 'createGoods',
                meta: { title: '创建商品', icon: 'edit', affix: true },
                hidden: true
            },
            {
                path: 'edit',
                component: () => import('@/goods/views/goods/edit.vue'),
                name: 'editDoods',
                meta: { title: '编辑商品', icon: 'edit', affix: true },
                hidden: true
            }
        ]
    },
    {
        path: '/store',
        component: Layout,
        name: 'store',
        meta: {
            title: '商家',
            icon: 'store'
        },
        children: [
            {
                path: 'index',
                component: () => import('@/shop/views/store/index.vue'),
                name: 'storeIndex',
                meta: { title: '商家', icon: 'store', affix: true }
            },
            {
                path: 'add',
                component: () => import('@/shop/views/store/add.vue'),
                name: 'addStore',
                meta: { title: '添加商家', icon: 'store', affix: true },
                hidden: true
            },
            {
                path: 'edit',
                component: () => import('@/shop/views/store/edit.vue'),
                name: 'editStore',
                meta: { title: '编辑商家', icon: 'store', affix: true },
                hidden: true
            }
        ]
    },
    // {
    //     path: '/system',
    //     component: Layout,
    //     name: 'system',
    //     meta: {
    //         title: '系统',
    //         icon: 'conf'
    //     },
    //     children: [
    //         // {
    //         // 	path: 'conf',
    //         // 	component: () => import('@/system/views/conf/index'),
    //         // 	name: 'systemConf',
    //         // 	meta: { title: '设置', icon: 'conf-base', affix: true }
    //         // },
    //         {
    //             path: 'dictionary',
    //             component: () => import('@/platform/views/dictionary/index'),
    //             name: 'dictionary',
    //             meta: { title: '数据字典', icon: 'type', affix: true }
    //         },
    //         {
    //             path: 'business',
    //             component: () => import('@/eshop/views/conf/index'),
    //             name: 'confBusiness',
    //             meta: { title: '业务设置', icon: 'order', affix: true }
    //         },
    //         {
    //             path: 'theme',
    //             component: () => import('@/widget/views/page/index'),
    //             name: 'pageIndex',
    //             meta: { title: '模式样式', icon: 'page-diy', affix: true }
    //         },
    //         {
    //             path: 'index',
    //             component: () => import('@/system/views/system/index'),
    //             name: 'platformConf',
    //             meta: { title: '系统', icon: 'conf', affix: true }
    //         }
    //     ]
    // },
    // {
    //     path: '/tool',
    //     component: Layout,
    //     name: 'tool',
    //     children: [
    //         {
    //             path: 'index',
    //             component: () => import('@/tool/views/tool/index'),
    //             name: 'index',
    //             meta: { title: 'toolBase', icon: 'tool-base', affix: true },
    //             hidden: true
    //         }
    //     ]
    // }
]

export const order_index = 20
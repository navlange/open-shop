// import i18n from '@/lang'
const title = import.meta.env.VITE_APP_TITLE ? import.meta.env.VITE_APP_TITLE : 'vue-element-admin3'

export default function getPageTitle(key) {
  // const hasKey = i18n.te(`route.${key}`)
  // if (hasKey) {
  //   const pageName = i18n.t(`route.${key}`)
  //   return `${pageName} - ${title}`
  // }
  return `${title}`
}

export function getQueryString(name: string) {
	const reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i')
	const search = window.location.search.split('?')[1] || ''
	const r = search.match(reg) || []
	return r[2]
}

export function url(path: string) {
	if (isUrl(path)) {
		return path
	}
	if (import.meta.env.NODE_ENV === 'production') {
		const href = new URL(window.location.href)
		return href.origin + '/' + 'product/' + (path.charAt(0) === '/' ? path.slice(1) : path)
	} else {
		return 'product/' + (path.charAt(0) === '/' ? path.slice(1) : path)
	}
}

export function openapi_url(path: string) {
	return '/api/' + (path.charAt(0) === '/' ? path.slice(1) : path)
}

export function cms_url(path: string) {
	return '/cms-api/' + (path.charAt(0) === '/' ? path.slice(1) : path)
}

const isUrl = (str_url: string) => {
	const strRegex = '^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|localhost|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$' // eslint-disable-line
	const re = new RegExp(strRegex)
	return re.test(str_url)
}

export function asset(path: string) {
	if (isUrl(path)) {
		return path
	} else {
		const href = new URL(window.location.href)
		return href.origin + '/' + path
	}
}

import { request } from '@/common/js/request'

export function fetchList(query: any) {
	return request('/shop-api/goods/list', query, 'GET')
}

export function fetchGoods(query: any) {
	return request('/shop-api/goods/get', query, 'GET')
}

export function createGoods(query: any) {
	return request('/shop-api/goods/create', query, 'POST')
}

export function updateGoods(query: any) {
	return request('/shop-api/goods/update', query, 'POST')
}

export function deleteGoods(query: any) {
	return request('/shop-api/goods/delete', query, 'POST')
}

export function setOrder(query: any) {
	return request('/shop-api/goods/setOrder', query, 'POST')
}

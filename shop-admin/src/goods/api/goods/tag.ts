import { request } from '@/common/js/request'

export function updateGoods(query: any) {
	return request('/shop-api/goods/tag/updateGoods', query, 'POST')
}

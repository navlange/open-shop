import { request } from '@/common/js/request'

export function get(query: any) {
    return request('/shop-api/goods/recommend/get', query, 'GET')
}

export function set(query: any) {
    return request('/shop-api/goods/recommend/set', query, 'POST')
}

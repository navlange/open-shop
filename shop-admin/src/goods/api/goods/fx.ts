import { request } from '@/common/js/request'
import { shop_url } from '@/common/js/url'

export function get(query: any) {
    return request(shop_url('/goods/fx/get'), query, 'GET')
}

export function set(query: any) {
    return request(shop_url('/goods/fx/set'), query, 'POST')
}

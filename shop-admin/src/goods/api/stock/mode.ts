import { request } from '@/common/js/request'

export function get(query: any) {
    return request('/shop-api/goods/stock/mode/get', query, 'GET')
}

export function update(query: any) {
    return request('/shop-api/goods/stock/mode/update', query, 'POST')
}

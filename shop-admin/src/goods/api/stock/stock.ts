import { isJSON } from '@/common/js/util'
import { request } from '@/common/js/request'

export function trans_norms(json_value: any, norms: any) {
	let norms_str = ''
	if (isJSON(json_value)) {
		const object_value = JSON.parse(json_value)
		const keys = Object.keys(object_value)
		keys.forEach(element => {
			norms.forEach((n: any) => {
				if (parseInt(n.id) === parseInt(element)) {
					n.values.forEach((v: any) => {
						if (parseInt(v.id) === parseInt(object_value[element])) {
							if (norms_str !== '') {
								norms_str += ', '
							}
							norms_str += v.value
						}
					})
				}
			})
		})
	}
	return norms_str
}

export function ofGoods(query: any) {
	return request('/shop-api/goods/stock/ofGoods', query, 'GET')
}

export function update(query: any) {
	return request('/shop-api/goods/stock/update', query, 'POST')
}

export function reset(query: any) {
	return request('/shop-api/goods/stock/reset', query, 'POST')
}

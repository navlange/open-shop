import Cookies from 'js-cookie'
import { request } from '@/common/js/request'
import { smartore_url, getQueryString } from '@/common/js/url'

const TokenKey = 'Admin-Token'

export function getToken() {
    return Cookies.get(TokenKey)
}

export function setToken(token: string) {
    return Cookies.set(TokenKey, token)
}

export function removeToken() {
    return Cookies.remove(TokenKey)
}

export function isLogin() {
    const token = getToken()
    if (token) {
        return true
    } else {
        return false
    }
}

export function login(data: any) {
if ((window as any).microApp && (window as any).microApp.getData) {
		return new Promise((resolve, reject) => {
			(window as any).microApp.getData().login().then((res: any) => {
				request('https://saas.lokei.cn/saas/we7/login', {
						jwt_secret: process.env.VUE_APP_JWT_SECRET,
						code: res.code,
						open_mode: '1'
					}, 'POST').then(res => {
					Cookies.set('W7_HOST', res.data.host, {
						expires: 365,
						path: '/',
						SameSite: 'None',
						Secure: true
					})
					resolve(res)
				}).catch(e => {
					reject(e)
				})
			}).catch((e: any) => {
                console.log(e)
				return new Promise((resolve, reject) => {
					reject('微擎登入错误')
				})
			})
		})
	} else if (getQueryString('auth_code') !== '' && getQueryString('auth_code') !== null && getQueryString('auth_code') !== undefined) {
		return request(smartore_url('/auth/qyapi/login'), Object.assign({
				auth_code: getQueryString('auth_code')
			}, data), 'POST')
	} else {
		return request(smartore_url('/auth/project/login?username=' + data.username + '&password=' + data.password + (data.pid ? '&pid=' + data.pid : '')), data, 'POST')
	}
}

export function getInfo(query: any) {
	return request(smartore_url('/auth/project/info'), query, 'GET')
}

export function logout() {
	return request(smartore_url('/auth/project/logout'), {}, 'POST')
}

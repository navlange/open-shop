import { request } from '@/common/js/request'
import { smt_url } from '@/common/js/url'

export function get(query) {
	return request(smt_url('/conf/custom/get'), query, 'GET')
}

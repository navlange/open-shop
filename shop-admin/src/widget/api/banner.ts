import { request } from '@/common/js/request'

export function fetchList(query: any) {
	return request('/cms-api/widget/banner/list', query, 'GET')
}

export function addBanner(query: any) {
	return request('/cms-api/widget/banner/add', query, 'POST')
}

export function deleteBanner(query: any) {
	return request('/cms-api/widget/banner/delete', query, 'POST')
}

export function updateBanner(query: any) {
	return request('/cms-api/widget/banner/update', query, 'POST')
}


import { request } from '@/common/js/request'

export function fetchList(query: any) {
	return request('/shop-api/widget/menu/list', query, 'GET')
}

export function reset(query: any) {
	return request('/shop-api/widget/menu/reset', query, 'POST')
}

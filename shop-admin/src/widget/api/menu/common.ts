import { request } from '@/common/js/request'

export function addmenu() {
	return request('/cms-api/widget/menu/add', {}, 'POST')
}

export function deleteMenu(query: any) {
	return request('/cms-api/widget/menu/delete', query, 'POST')
}

export function updateMenu(query: any) {
	return request('/cms-api/widget/menu/update', query, 'POST')
}

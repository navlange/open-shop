import { request } from '@/common/js/request'

export function get(query: any) {
	return request('/cms-api/widget/theme/menu/get', query, 'GET')
}

export function set(query: any) {
	return request('/cms-api/widget/theme/menu/set', query, 'POST')
}

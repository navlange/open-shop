import { request } from '@/common/js/request'

export function fetch(query: any) {
	return request('/cms-api/widget/theme/get', query, 'GET')
}

export function update(query: any) {
	return request('/cms-api/widget/theme/update', query, 'POST')
}

import { request } from '@/common/js/request'

export function get(query: any) {
    return request('/cms-api/widget/index/banner/get', query, 'GET')
}

export function set(query: any) {
    return request('/cms-api/widget/index/banner/set', query, 'POST')
}

import { request } from '@/common/js/request'
import { cms_url } from '@/common/js/url'

export function get(query: any) {
    return request(cms_url('/widget/index/store/get'), query, 'GET')
}

export function set(query: any) {
    return request(cms_url('/widget/index/store/set'), query, 'POST')
}

export function turnOn(query: any) {
    return request(cms_url('/widget/index/store/turnOn'), query, 'POST')
}

export function turnOff(query: any) {
    return request(cms_url('/widget/index/store/turnOff'), query, 'POST')
}

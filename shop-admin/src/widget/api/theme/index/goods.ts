import { request } from '@/common/js/request'

export function get(query: any) {
    return request('/shop-api/widget/index/goods/get', query, 'GET')
}

export function set(query: any) {
    return request('/shop-api/widget/index/goods/set', query, 'POST')
}

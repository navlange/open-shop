import { request } from '@/common/js/request'
import { cms_url } from '@/common/js/url'

export function fetch(query: any) {
	return request(cms_url('/widget/nav/pic/get'), query, 'GET')
}

export function update(query: any) {
	return request(cms_url('/widget/nav/pic/update'), query, 'POST')
}

export function getHot(query: any) {
	return request(cms_url('/widget/nav/pic/hot/get'), query, 'GET')
}

export function addHot(query: any) {
	return request(cms_url('/widget/nav/pic/hot/add'), query, 'POST')
}

export function updateHot(query: any) {
	return request(cms_url('/widget/nav/pic/hot/update'), query, 'POST')
}

export function deleteHot(query: any) {
	return request(cms_url('/widget/nav/pic/hot/delete'), query, 'POST')
}

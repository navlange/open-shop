import type { MockMethod } from 'vite-plugin-mock'
const tokens = {
    admin: {
        token: 'admin-token'
    },
    editor: {
        token: 'editor-token'
    }
}

const users: any = {
    'admin-token': {
        roles: ['admin'],
        introduction: 'I am a super administrator',
        avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
        name: 'Super Admin'
    },
    'editor-token': {
        roles: ['editor'],
        introduction: 'I am an editor',
        avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
        name: 'Normal Editor'
    }
}

const mock: Array<MockMethod> = [
    {
        // 接口路径
        url: '/vue-element-admin3/auth/admin/login',

        // 接口方法
        method: 'post',

        // 返回数据
        response: () => {
            return {
                errorCode: 200,
                code: 20000,
                data: tokens['admin']
            }
        }
    },
    {
        // 接口路径
        url: '/vue-element-admin3/auth/admin/role',

        // 接口方法
        method: 'get',

        // 返回数据
        response: (config) => {
            const { token } = config.query
            const info = users[token]
            return {
                errorCode: 200,
                code: 20000,
                data: info
            }
        }
    },
    {
        // 接口路径
        url: '/vue-element-admin3/auth/logout',

        // 接口方法
        method: 'post',

        // 返回数据
        response: () => {
            return {
                errorCode: 200,
                code: 20000,
                data: 'success'
            }
        }
    }
]

export default mock

const globalPlugin = {
    compile_env: typeof import.meta.env !== "undefined" ? 'vite' : 'vue-cli-service',
    is_we7: ((window as any).microApp && (window as any).microApp.getData) ? true : false
}

export default globalPlugin
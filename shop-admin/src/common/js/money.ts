export function parseMoney(x: any) {
    let f_x = parseFloat(x)
    if (isNaN(f_x)) {
        return ''
    }
    f_x = Math.round(f_x * 100) / 100
    let s_x = f_x.toString()
    let pos_decimal = s_x.indexOf('.')
    if (pos_decimal < 0) {
        pos_decimal = s_x.length
        s_x += '.'
    }
    while (s_x.length <= pos_decimal + 2) {
        s_x += '0'
    }
    return s_x
}

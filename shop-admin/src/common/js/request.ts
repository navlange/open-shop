import axios, { type AxiosRequestConfig, type AxiosResponse, type Method } from "axios";
import store from '@/store'
import { ElMessage } from "element-plus";

import global from "@/global";

const service = axios.create({
    baseURL: (global.compile_env === 'vue-cli-service') ? process.env.VUE_APP_BASE_API : import.meta.env.VITE_APP_BASE_API,
    timeout: 5000,
});

// 请求拦截器，一般用来设置请求头信息
service.interceptors.request.use((config) => {
	if(store.getters.token) {
		config.headers.Authorization = `Bearer ${store.getters.token}`;
        config.headers['Proj-Token'] = store.getters.token
	}
    return config;
});

// 响应拦截器，处理后台返回的数据
service.interceptors.response.use((resp) => {
    return resp.data;
},
(error) => {
    console.error('err' + error);
    ElMessage.error(error.message);
    return Promise.reject(error);
}
);

/** 接口的返回值类型 */
type Result<T> = {
    code: 200 | 404 | 500;
    msg?: string;
    data?: T;
};

// 因为TS有类型约束，所以外面再包一层函数，用来做类型约束，这样在使用的时候就有类型推断了
export const request = <T = any>(
    url: string,
    submitData: object,
    method: Method,
    config?: AxiosRequestConfig
) => {
    return service<T, Result<T>>({
        url,
        method,
        // get请求用params接收，其他请求用data
        [method.toLowerCase() === "get" ? "params" : "data"]: submitData,
        // 其他配置项
        ...config,
    });
};

export const get = async (url: string, params?: any) => {
    try {
        const response: AxiosResponse = await service.get(url, { params });
        return response;
    } catch (error) {
        console.error(error);
    }
};

export const post = async (url: string, params?: any) => {
    try {
        const response: AxiosResponse = await service.post(url, params);
        return response;
    } catch (error) {
        console.error(error);
    }
};

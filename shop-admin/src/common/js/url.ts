export function isUrl(str_url: any) {
    // var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
    //     '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' + // domain name
    //     '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    //     '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    //     '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    //     '(\\#[-a-z\\d_]*)?$', 'i') // fragment locator
    // return pattern.test(str_url)
    const strRegex = '^http[s]?:\\/\\/([\\w-]+\\.)+[\\w-]+([\\w-./?%&=]*)?$'
    const re = new RegExp(strRegex)
    return re.test(str_url)
}

export function asset(path: any) {
    const strRegex = '^http[s]?:\\/\\/.*$'
    const re = new RegExp(strRegex)
    if (re.test(path)) {
        return path
    } else if (path !== null && path !== undefined && path !== '') {
        const href = window.location.href
        return new URL(href).origin + '/' + path
    } else {
        return path
    }
}

export function getQueryString(name: any) {
    const reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i')
    const search = window.location.search.split('?')[1] || ''
    const r = search.match(reg) || []
    return r[2]
}

export function getUrlCode() {
	// 截取url中的code方法
	const url = location.href
	const theRequest = <any>{}
	const parts = url.split('#') // vue兼容微擎
	parts.forEach(element => {
		if (element.indexOf('?') !== -1) {
			const str = element.substr(element.indexOf('?') + 1)
			const strs = str.split('&')
			for (let i = 0; i < strs.length; i++) {
				theRequest[strs[i].split('=')[0]] = strs[i].split('=')[1]
			}
		}
	})
	return theRequest
}

export function openapi_url(path: string) {
    return '/api/' + (path.charAt(0) === '/' ? path.slice(1) : path)
}

export function cms_url(path: string) {
    return '/cms-api/' + (path.charAt(0) === '/' ? path.slice(1) : path)
}

export function smt_url(path: string) {
    return '/smt/' + (path.charAt(0) === '/' ? path.slice(1) : path)
}

export function shop_url(path: string) {
    return '/shop-api/' + (path.charAt(0) === '/' ? path.slice(1) : path)
}

export function smartore_url(path: string) {
    return '/smartore/' + (path.charAt(0) === '/' ? path.slice(1) : path)
}

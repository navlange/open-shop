import { PAYMODE } from '@/common/js/def'

export function isObj(object: any) {
	return object && typeof (object) === 'object' && Object.prototype.toString.call(object).toLowerCase() === '[object object]'
}

export function isJSON(str: any) {
	try {
		const obj = JSON.parse(str)
		return !!obj && typeof obj === 'object'
	} catch (e) {
		// console.log(e)
	}
	return false
}

export function trans_pay_mode(pay_mode: any) {
	if (pay_mode === PAYMODE.BALANCE) {
		return '余额支付'
	} else if (pay_mode === PAYMODE.WECHAT) {
		return '微信支付'
	} else if (pay_mode === PAYMODE.ALIPAY) {
		return '支付宝支付'
	} else if (pay_mode === PAYMODE.INTEGRAL) {
		return '积分兑换'
	} else if (pay_mode === PAYMODE.DYCOUPON) {
		return '抖券兑换'
	} else if (pay_mode === PAYMODE.PAYCARD) {
		return '储值卡支付'
	} else if (pay_mode === PAYMODE.OFFLINE) {
		return '线下支付'
	} else if (pay_mode === PAYMODE.NONEED) {
		return '无需支付'
	}
	return '--'
}

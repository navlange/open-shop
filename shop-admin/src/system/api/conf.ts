import { request } from '@/common/js/request'

export function fetch(query: any) {
    return request('/api/conf/get', query, 'GET')
}

export function update(query: any) {
    return request('/api/conf/update', query, 'POST')
}

import { request } from '@/common/js/request'

export function get(query: any) {
	return request('/api/platform/agreement/get', query, 'GET')
}

export function update(query: any, new_data: any) {
	return request('/api/platform/agreement/update', query, 'POST')
}

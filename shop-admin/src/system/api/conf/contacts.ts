import { request } from '@/common/js/request'

export function fetch(query: any) {
	return request('/api/platform/contacts/get', query, 'GET')
}

export function update(query: any) {
	return request('/api/platform/contacts/update', query, 'POST')
}

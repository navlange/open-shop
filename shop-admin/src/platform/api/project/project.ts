import { request } from '@/common/js/request'
import { smartore_url } from '@/common/js/url'

export function getPid(query: any) {
    if(import.meta.env.DEV) {
        return new Promise((resolve) => {
            resolve({
                code: 0,
                data: {
                    pid: null
                }
            })
        })
    }
    return request(smartore_url('/platform/project/getPid'), query, 'GET')
}

import { request } from '@/common/js/request'

export function fetchList(query: any) {
    return request('/tag/list', query, 'GET')
}

export function ofScene(query: any) {
    return request('/api/tag/ofScene', query, 'GET')
}

export function create(query: any) {
    return request('/tag/create', query, 'POST')
}

export function update(query: any) {
    return request('/tag/update', query, 'POST')
}

export function deleteTag(query: any) {
    return request('/tag/delete', query, 'POST')
}

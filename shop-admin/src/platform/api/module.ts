import Cookies from 'js-cookie'

import { request } from '@/common/js/request'
import { smartore_url } from '@/common/js/url'
import { isJSON } from '@/common/js/util'

const CurSmtappKey = 'Cur-App-Key'

const SmtAuthListKey = 'AmtAuthList-Key'

const SaasOpenModeKey = 'Saas-Open-Mode-Key'

export function setSaasOpenMode(open_mode: any) {
	return Cookies.set(SaasOpenModeKey, open_mode, {
		expires: 365,
		path: '/',
		SameSite: 'None',
		Secure: true
	})
}

export function getSaasOpenMode() {
	return Cookies.get(SaasOpenModeKey)
}

export function getCurSmtAppMode() {
	return Cookies.get(CurSmtappKey)
}

export function setCurSmtAppMode(mode: any) {
	return Cookies.set(CurSmtappKey, mode, {
		expires: 365,
		path: '/',
		SameSite: 'None',
		Secure: true
	})
}

export function removeCurSmtAppMode() {
	return Cookies.remove(CurSmtappKey)
}

export function getSmtAuthList() {
    if (isJSON(Cookies.get(SmtAuthListKey))) {
        return JSON.parse(Cookies.get(SmtAuthListKey) ?? "")
    } else {
        return []
    }
}

export function setSmtAuthList(auth_list: any) {
	return Cookies.set(SmtAuthListKey, JSON.stringify(auth_list), {
		expires: 365,
		path: '/',
		SameSite: 'None',
		Secure: true
	})
}

export function removeSmtAuthList() {
	return Cookies.remove(SmtAuthListKey)
}

export function list(query: any) {
	return request(smartore_url('/platform/smtapp/auth/ofProj'), query, 'GET')
}

export function checkAuthOfAppMode(app_mode: any) {
	if (process.env.NODE_ENV === 'development') {
		return true
	}
	const auth_list = getSmtAuthList()
	for (let i = 0; i < auth_list.length; i++) {
		if (auth_list[i].app_mode === app_mode) {
			return true
		}
	}
	return false
}

import { request } from '@/common/js/request'

export function get(query: any) {
  return request('/api/type/get', query, 'GET')
}

export function fetchList(query: any) {
  return request('/api/type/list', query, 'GET')
}

export function ofMode(query: any) {
  return request('/api/type/ofMode', query, 'GET')
}

export function ofScene(query: any) {
  return request('/api/type/ofScene', query, 'GET')
}

export function ofModeRoot(query: any) {
  return request('/api/type/ofModeRoot', query, 'GET')
}

export function ofPre(query: any) {
  return request('/api/type/ofPre', query, 'GET')
}

export function create(query: any) {
	return request('/api/type/create', query, 'POST')
}

export function update(query: any) {
	return request('/api/type/update', query, 'POST')
}

export function deleteType(query: any) {
	return request('/api/type/delete', query, 'POST')
}

export function setOrder(query: any) {
	return request('/api/type/setOrder', query, 'POST')
}

import { request } from '@/common/js/request'
import { smt_url } from '@/common/js/url'

export function bindUser(query: any) {
    return request(smt_url('/store/auth/bindUser'), query, 'POST')
}

export function cancelBindUser(query: any) {
    return request(smt_url('/store/auth/cancelBindUser'), query, 'POST')
}

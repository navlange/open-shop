import { request } from '@/common/js/request'
import { smt_url } from '@/common/js/url'

export function fetchList(query: any) {
    return request(smt_url('/store/recommend/get'), query, 'GET')
}

export function set(query: any) {
    return request(smt_url('/store/recommend/set'), query, 'POST')
}

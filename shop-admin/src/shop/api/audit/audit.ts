import { request } from '@/common/js/request'
import { smt_url } from '@/common/js/url'

export function get(query: any) {
	return request(smt_url('/store/audit/get'), query, 'GET')
}

export function set(query: any) {
	return request(smt_url('/store/audit/set'), query, 'POST')
}

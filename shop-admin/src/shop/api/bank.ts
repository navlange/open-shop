import { request } from "@/common/js/request";
import { openapi_url } from "@/common/js/url";

export function get(query: any) {
    return request(openapi_url("/store/bank/get"), query, "GET");
}

import { request } from '@/common/js/request'

export function queryList(query: any) {
	return request('/smt/store/list', query, 'GET')
}

export function fetch(query: any) {
	return request('/smt/store/get', query, 'GET')
}

export function createStore(query: any) {
	return request('/smt/store/create', query, 'POST')
}

export function importStore(query: any) {
	return request('/smt/store/import', query, 'POST')
}

export function update(query: any, new_data: any) {
	return request('/smt/store/update', query, 'POST', new_data)
}

export function create(query: any, new_data: any) {
	return request('/smt/store/create', query, 'POST', new_data)
}

export function deleteStore(query: any) {
	return request('/smt/store/delete', query, 'POST')
}

export function setOrder(query: any) {
	return request('/smt/store/setOrder', query, 'POST')
}

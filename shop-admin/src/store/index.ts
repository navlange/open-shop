import { createStore } from "vuex";
import getters from "./getters";

import global from "@/global";

let modules: Record<string, any> = [];

if (global.compile_env === 'vue-cli-service') {
    const modulesFiles = require.context("./modules", true, /\.js$/);

    // you do not need `import app from './modules/app'`
    // it will auto require all vuex module from modules file
    modules = modulesFiles.keys().reduce((modules: any, modulePath) => {
        // set './app.js' => 'app'
        const moduleName: any = modulePath.replace(/^\.\/(.*)\.\w+$/, "$1");
        const value = modulesFiles(modulePath);
        modules[moduleName] = value.default;
        return modules;
    }, {});
}

if (typeof import.meta.env !== 'undefined' && global.compile_env === 'vite') {
    // const modulesFiles = require.context("./modules", true, /\.js$/);
    const modulesFiles = import.meta.globEager("./modules/*.js");

    for (const path in modulesFiles) {
        const value: any = modulesFiles[path];
        const moduleName = path.replace(/^\.\/modules\/(.*)\.\w+$/, "$1");
        modules[moduleName] = value.default;
    }
}

export default createStore({
    state: {},
    getters: getters,
    // mutations: {},
    // actions: {},
    modules: modules,
});

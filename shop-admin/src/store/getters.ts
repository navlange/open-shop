const getters = {
sidebar: (state: any) => state.app.sidebar,
  language: (state: any) => state.app.language,
  size: (state: any) => state.app.size,
  device: (state: any) => state.app.device,
  visitedViews: (state: any) => state.tagsView.visitedViews,
  cachedViews: (state: any) => state.tagsView.cachedViews,
  token: (state: any) => state.user.token,
  avatar: (state: any) => state.user.avatar,
  name: (state: any) => state.user.name,
  introduction: (state: any) => state.user.introduction,
  roles: (state: any) => state.user.roles,
  projInfo: (state: any) => state.user.proj_info,
  has_delete_permission: (state: any) => state.user.has_delete_permission,
  permission_routes: (state: any) => state.permission.routes,
  errorLogs: (state: any) => state.errorLog.logs,
  platform_info: (state: any) => state.platform.platform_info
}
export default getters

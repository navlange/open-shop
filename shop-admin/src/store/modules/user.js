import {
    login,
    logout,
    getInfo,
    getToken,
    setToken,
    removeToken,
} from "@/rbac/api/auth";
import router, { resetRouter } from "@/router";
import { ofPcid as roleOfPcid } from "@/clerk/api/role/role";
import { ElMessage } from "element-plus";

const state = {
    token: getToken(),
    name: "",
    avatar: "",
    introduction: "",
    roles: [],
    proj_info: {},
    has_delete_permission: false,
};

const mutations = {
    SET_TOKEN: (state, token) => {
        state.token = token;
    },
    SET_INTRODUCTION: (state, introduction) => {
        state.introduction = introduction;
    },
    SET_NAME: (state, name) => {
        state.name = name;
    },
    SET_AVATAR: (state, avatar) => {
        state.avatar = avatar;
    },
    SET_ROLES: (state, roles) => {
        state.roles = roles;
    },
    SET_PROJ_INFO: (state, proj_info) => {
        state.proj_info = proj_info;
    },
    SET_HAS_DELETE_PERMISSION: (state, has_delete_permission) => {
        state.has_delete_permission = has_delete_permission;
    },
};

const actions = {
    // user login
    login({ commit }, userInfo) {
        const { username, password, pid } = userInfo;
        return new Promise((resolve, reject) => {
            var data = { username: username.trim(), password: password };
            if (pid !== null && pid !== "" && pid !== undefined) {
                data = Object.assign(data, {
                    pid: pid,
                });
            }
            login(data)
                .then((response) => {
                    if (response.errorCode === 200) {
                        const { data } = response;
                        commit("SET_TOKEN", data.token);
                        setToken(data.token);
                        resolve();
                    } else {
                        ElMessage({
                            message: response.errorMsg,
                            type: "error",
                            duration: 5 * 1000,
                        });
                        reject(response.errorCode);
                    }
                })
                .catch((error) => {
                    console.log(error);
                    reject(error);
                });
        });
    },

    // get user info
    getInfo({ commit, state }) {
        return new Promise((resolve, reject) => {
            getInfo(
                Object.assign({
                    token: state.token,
                })
            )
                .then((response) => {
                    if (response.errorCode === 200) {
                        const { data } = response;

                        if (!data) {
                            reject("Verification failed, please Login again.");
                        }

                        const { roles, name, avatar, introduction, proj_info } =
                            data;

                        commit("SET_PROJ_INFO", proj_info);

                        if (roles && roles.length > 0) {
                            // roles must be a non-empty array
                            // if (!roles || roles.length <= 0) {
                            //   reject('getInfo: roles must be a non-null array!')
                            // }
                            commit("SET_ROLES", roles);
                            commit("SET_NAME", name);
                            commit("SET_AVATAR", avatar);
                            commit("SET_INTRODUCTION", introduction);
                            // commit("SET_HAS_ALL_STORE_PERMISSION", true);
                            commit("SET_HAS_DELETE_PERMISSION", true);
                            resolve(data);
                        } else {
                            roleOfPcid()
                                .then((res) => {
                                    commit(
                                        "SET_ROLES",
                                        res.data.role.split("|")
                                    );
                                    if (
                                        res.data.roles_admin !== null &&
                                        res.data.roles_admin !== "" &&
                                        res.data.roles_admin !== undefined
                                    ) {
                                        commit(
                                            "SET_ROLES_ADMIN",
                                            res.data.roles_admin.split("|")
                                        );
                                    }
                                    if (
                                        res.data.roles_store !== null &&
                                        res.data.roles_store !== "" &&
                                        res.data.roles_store !== undefined
                                    ) {
                                        commit(
                                            "SET_ROLES_STORE",
                                            res.data.roles_store.split("|")
                                        );
                                    }
                                    commit("SET_NAME", res.data.name);
                                    commit("SET_AVATAR", res.data.avatar);
                                    commit(
                                        "SET_INTRODUCTION",
                                        res.data.introduction
                                    );
                                    if (res.data.role === "admin") {
                                        // commit(
                                        //     "SET_HAS_ALL_STORE_PERMISSION",
                                        //     true
                                        // );
                                        if (
                                            res.data.roles_admin.indexOf(
                                                "admin"
                                            ) >= 0
                                        ) {
                                            commit(
                                                "SET_HAS_DELETE_PERMISSION",
                                                true
                                            );
                                        }
                                    }
                                    if (res.data.role === "store_admin") {
                                        commit(
                                            "SET_ADMIN_STORE_ID",
                                            res.data.store_id
                                        );
                                    }
                                    var resolve_data = Object.assign(data, {
                                        roles: res.data.role.split("|"),
                                    });
                                    if (
                                        res.data.roles_admin !== null &&
                                        res.data.roles_admin !== "" &&
                                        res.data.roles_admin !== undefined
                                    ) {
                                        resolve_data = Object.assign(
                                            resolve_data,
                                            {
                                                roles_admin:
                                                    res.data.roles_admin.split(
                                                        "|"
                                                    ),
                                            }
                                        );
                                    }
                                    if (
                                        res.data.roles_store !== null &&
                                        res.data.roles_store !== "" &&
                                        res.data.roles_store !== undefined
                                    ) {
                                        resolve_data = Object.assign(
                                            resolve_data,
                                            {
                                                roles_store:
                                                    res.data.roles_store.split(
                                                        "|"
                                                    ),
                                            }
                                        );
                                    }
                                    resolve(resolve_data);
                                })
                                .catch((e) => {
                                    reject(e);
                                });
                        }
                    } else {
                        commit("SET_TOKEN", "");
                        commit("SET_ROLES", []);
                        reject(response.errorMsg);
                    }
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },

    // user logout
    logout({ commit, state, dispatch }) {
        return new Promise((resolve, reject) => {
            logout(state.token)
                .then(() => {
                    commit("SET_TOKEN", "");
                    commit("SET_ROLES", []);
                    removeToken();
                    resetRouter();

                    // reset visited views and cached views
                    // to fixed https://github.com/PanJiaChen/vue-element-admin/issues/2485
                    dispatch("tagsView/delAllViews", null, { root: true });

                    resolve();
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },

    // remove token
    resetToken({ commit }) {
        return new Promise((resolve) => {
            commit("SET_TOKEN", "");
            commit("SET_ROLES", []);
            removeToken();
            resolve();
        });
    },

    // dynamically modify permissions
    async changeRoles({ commit, dispatch }, role) {
        const token = role + "-token";

        commit("SET_TOKEN", token);
        setToken(token);

        const { roles } = await dispatch("getInfo");

        resetRouter();

        // generate accessible routes map based on roles
        const accessRoutes = await dispatch(
            "permission/generateRoutes",
            roles,
            { root: true }
        );
        // dynamically add accessible routes
        router.addRoutes(accessRoutes);

        // reset visited views and cached views
        dispatch("tagsView/delAllViews", null, { root: true });
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
};

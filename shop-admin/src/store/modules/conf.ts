import { init as get_platform_init } from '@/platform/api/conf'
import { ElMessage } from 'element-plus'

const state = {
	alias: {}
}

const mutations = {
	SET_ALIAS: (state: any, alias: any) => {
		state.alias = alias
	}
}

const actions = {
	// user login
	get_init(context: any) {
		return new Promise((resolve, reject) => {
			get_platform_init(null).then((response: any) => {
				if (response.errorCode === 200) {
					const { data } = response
					context.commit('SET_ALIAS', data.alias)
					resolve(data)
				} else {
					ElMessage({
						message: response.errorMsg,
						type: 'error',
						duration: 5 * 1000
					})
					reject(response.errorCode)
				}
			}).catch((error: any) => {
				console.log(error)
				reject(error)
			})
		})
	}
}

export default {
	namespaced: true,
	state,
	mutations,
	actions
}

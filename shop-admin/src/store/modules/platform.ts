import { get as get_platform_info } from '@/platform/api/info'
import { ElMessage } from 'element-plus'

const state = {
	platform_info: {}
}

const mutations = {
	SET_PLATFORM_INFO: (state: any, platform_info: any) => {
		state.platform_info = platform_info
	}
}

const actions = {
	get_info (context: any) {
		return new Promise((resolve, reject) => {
			get_platform_info(null).then((response: any) => {
				if (response.errorCode === 200) {
					const { data } = response
					context.commit('SET_PLATFORM_INFO', data)
					resolve(data)
				} else {
					ElMessage({
						message: response.errorMsg,
						type: 'error',
						duration: 5 * 1000
					})
					reject(response.errorCode)
				}
			}).catch((error: any) => {
				console.log(error)
				reject(error)
			})
		})
	}
}

export default {
	namespaced: true,
	state,
	mutations,
	actions
}

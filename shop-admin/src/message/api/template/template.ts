import { request } from '@/common/js/request'
import { openapi_url } from '@/common/js/url'

export function list(query: any) {
    return request(openapi_url('/message/template/list'), query, 'GET')
}

export function get(query: any) {
    return request(openapi_url('/message/template/get'), query, 'GET')
}

export function create(query: any) {
    return request(openapi_url('/message/template/create'), query, 'POST')
}

export function update(query: any) {
    return request(openapi_url('/message/template/update'), query, 'POST')
}

export function deleteTemplate(query: any) {
    return request(openapi_url('/message/template/delete'), query, 'POST')
}

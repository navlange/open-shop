import { request } from '@/common/js/request'
import { openapi_url } from '@/common/js/url'

export function fetch(query: any) {
	return request(openapi_url('/user/get'), query, 'GET')
}

export function fetchByMobile(query: any) {
	return request(openapi_url('/user/getByMobile'), query, 'GET')
}

export function createUser(query: any) {
	return request(openapi_url('/user/create'), query, 'POST')
}

export function updateUser(query: any) {
	return request(openapi_url('/user/update'), query, 'POST')
}

export function fetchList(query: any) {
	return request(openapi_url('/user/list'), query, 'GET')
}

export function deleteUser(query: any) {
	return request(openapi_url('/user/delete'), query, 'POST')
}

export function getIdcard(query: any) {
	return request(openapi_url('/user/idcard/get'), query, 'GET')
}

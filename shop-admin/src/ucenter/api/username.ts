import { request } from "@/common/js/request";
import { openapi_url } from "@/common/js/url";

export function update(query: any) {
    return request(openapi_url("/user/username/update"), query, "POST");
}

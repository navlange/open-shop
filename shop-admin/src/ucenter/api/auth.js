import request from '@/utils/request'
import { url } from '@/utils/url'

export function updatePassword(query) {
  return request({
    url: url('/ucenter/auth/updatePassword'),
    method: 'post',
    params: query
  })
}

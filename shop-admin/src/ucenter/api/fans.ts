import { request } from '@/common/js/request'
import { openapi_url } from '@/common/js/url'

export function list(query: any) {
	return request(openapi_url('/fans/list'), query, 'GET')
}

export function ofUser(query: any) {
	return request(openapi_url('/fans/ofUser'), query, 'GET')
}

export function deleteFans(query: any) {
	return request(openapi_url('/fans/delete'), query, 'POST')
}

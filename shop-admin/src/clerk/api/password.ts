import { request } from '@/common/js/request'
import { openapi_url } from '@/common/js/url'

export function update(query: any) {
	return request(openapi_url('/clerk/password/update'), query, 'POST')
}

export function updateCid(query: any) {
	return request(openapi_url('/clerk/password/updateCid'), query, 'POST')
}

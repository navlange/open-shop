import { request } from "@/common/js/request";
import { smt_url } from "@/common/js/url";

export function fetch(query: any) {
    return request(smt_url("/clerk/marketing/get"), query, "GET");
}

export function update(query: any) {
    return request(smt_url("/clerk/marketing/update"), query, "POST");
}

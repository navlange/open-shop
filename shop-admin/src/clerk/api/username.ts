import { request } from '@/common/js/request'
import { openapi_url } from '@/common/js/url'

export function fetch(query: any) {
	return request(openapi_url('/clerk/username/get'), query, 'GET')
}

export function update(query: any) {
	return request(openapi_url('/clerk/username/update'), query, 'POST')
}

export function updateCid(query: any) {
	return request(openapi_url('/clerk/username/updateCid'), query, 'POST')
}

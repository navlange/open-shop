import { request } from "@/common/js/request";
import { openapi_url } from "@/common/js/url";

export function fetch(query: any) {
    return request(openapi_url("/clerk/role/admin/get"), query, "GET");
}

export function update(query: any) {
    return request(openapi_url("/clerk/role/admin/update"), query, "POST");
}

import { request } from '@/common/js/request'
import { openapi_url } from '@/common/js/url'

export function fetch(query: any) {
	return request(openapi_url('/clerk/role/get'), query, 'GET')
}

export function ofPcid(query: any) {
	return request(openapi_url('/clerk/role/ofPcid'), query, 'GET')
}

export function update(query: any) {
	return request(openapi_url('/clerk/role/update'), query, 'POST')
}

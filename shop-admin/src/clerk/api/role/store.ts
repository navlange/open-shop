import { request } from '@/common/js/request'
import { openapi_url } from '@/common/js/url'

export function fetch(query: any) {
	return request(openapi_url('/clerk/role/store/get'), query, 'GET')
}

export function update(query: any) {
	return request(openapi_url('/clerk/role/store/update'), query, 'POST')
}

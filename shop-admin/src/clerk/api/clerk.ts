import { request } from "@/common/js/request";
import { openapi_url } from "@/common/js/url";

export function ofProj(query: any) {
    return request(openapi_url("/clerk/list"), query, "GET");
}

export function fetch(query: any) {
    return request(openapi_url("/clerk/get"), query, "GET");
}

export function update(query: any) {
    return request(openapi_url("/clerk/update"), query, "POST");
}

export function create(query: any) {
    return request(openapi_url("/clerk/create"), query, "POST");
}

export function deleteClerk(query: any) {
    return request(openapi_url("/clerk/delete"), query, "POST");
}

export function bindUser(query: any) {
    return request(openapi_url("/clerk/bindUser"), query, "POST");
}

export function cancelBindUser(query: any) {
    return request(openapi_url("/clerk/cancelBindUser"), query, "POST");
}

import { request } from '@/common/js/request'

export function login(data) {
    return request('/api/auth/admin/login', data, 'post')
}

export function getInfo(token) {
    // return new Promise(function(resolve, reject) {
    //   resolve({ data: { roles: ['admin'] }})
    // })
    return request(
        '/api/auth/admin/role',
        {
            token: token
        },
        'GET'
    )
}

export function logout(token) {
    return request(
        '/api/auth/logout',
        {
            token: token
        },
        'POST'
    )
}

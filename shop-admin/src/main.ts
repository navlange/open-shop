// import './assets/main.css'

import ElementPlus from 'element-plus' //全局引入
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'


import './styles/element-variables.scss'
import '@/styles/index.scss' // global css

import '@/common/iconfont/iconfont.css' // iconfont css

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
// 引入store
import store from '@/store/index.js'
import '@/permission.js' // permission control

import router from './router'
// import { productRoutes } from '@/product/router'

import 'virtual:svg-icons-register' // svg-icon需要引入
import SvgIcon from '@/components/SvgIcon/index.vue'

import i18n from '@/lang/index.js'

import VueUeditorWrap from 'vue-ueditor-wrap';

const app = createApp(App)

app.use(ElementPlus)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}

app.use(createPinia())
// 使用store //
app.use(store)
app.use(i18n)

app.component('SvgIcon', SvgIcon)

app.use(router)

app.use(VueUeditorWrap)

app.mount('#app')
